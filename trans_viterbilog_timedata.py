import sys

finname = "log.fastcky10" if len(sys.argv) < 2 else sys.argv[1]
foutname = "fastcky10.speed" if len(sys.argv) < 3 else sys.argv[2]

fout = open(foutname, "w")
for line in open(finname):
    tmp = line.strip().split()
    if tmp == []:
        continue
    if len(tmp) > 1 and "Viterbi" in tmp[1]:
        x = int(tmp[0])
    if "Time:" in tmp[0]:
        y = float(tmp[1])
        print >> fout, x, y
fout.close()
