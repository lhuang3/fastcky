#!/usr/bin/env python

"""Generate random nucs sequence of a given length.
Run: randseq.py length
"""

import sys
import numpy as np

"""probs based on the distribution in biginput.noknots"""
__probs = {"A": 0.2559, "C": 0.2258, "G": 0.2762, "U": 0.2366, "N": 0.0055}

def main():
    nucs = __probs.keys()
    n_nucs = len(nucs)
    probs = [__probs[n] for n in nucs]
    length = int(sys.argv[1])
    print "".join(nucs[np.random.choice(np.arange(0, n_nucs), p=probs)] for _ in xrange(length))


if __name__ == "__main__":
    main()
