//
// Created by Kai Zhao on 1/29/17.
//

#ifndef FASTCKY_LINPAR_H
#define FASTCKY_LINPAR_H

#include <string>

enum Manner {
    MANNER_NONE = 0,
};


struct State {
    int i;
    double score;
    Manner manner;
};


#endif //FASTCKY_LINPAR_H
