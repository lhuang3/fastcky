#!/bin/env python2.7

import sys, time

# allowed = set(['AU','UA','CG','GC','GU','UG'])
allowed = set(['AU','UA','CG','GC','GU','UG', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg'])

## seq, n
def search(j, s, stack1, stack2, d1, d2, score):
    if score + d1+d2+(n-j)/2 <= best[0]: # TODO: cache
        return
    if j == n:
        if stack1=="" and stack2=="" and score > best[0]:
            best[0] = score
            best[1] = s
        return
    search(j+1, s+'.', stack1, stack2, d1, d2, score)
    curr = seq[j]
    if d1+1+d2 <= n-j:
        search(j+1, s+'(', stack1+curr, stack2, d1+1, d2, score)
        if stack1 != "": # N.B.
            search(j+1, s+'[', stack1, stack2+curr, d1, d2+1, score)
    if stack1 != "" and (stack1[-1]+curr) in allowed:
        search(j+1, s+')', stack1[:-1], stack2, d1-1, d2, score+1)
    if stack2 != "" and (stack2[-1]+curr) in allowed:
        search(j+1, s+']', stack1, stack2[:-1], d1, d2-1, score+0.9) # prefer noknots

# for seq in sys.stdin:
#     best = [-1, '']
#     seq = seq.strip()
#     n = len(seq)
#     search(0, '', '', '', 0, 0, 0)
#     print n, best

seq = sys.argv[1] if len(sys.argv) >= 2 else "abacbc"
best = [-1, '']
seq = seq.strip()
n = len(seq)
starttime = time.time()
search(0, '', '', '', 0, 0, 0)
print best
print "len: ", n, "time: ", time.time() - starttime
