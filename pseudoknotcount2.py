#!/usr/bin/env python

import sys, os
from string import maketrans

def ifcross(point1, point2):
    a, b = point1
    c, d = point2
    if a<c<b<d or c<a<d<b:
        return True
    else:
        return False

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def solve(myfile):
    links = []
    maxindex = -1
    for line in open(myfile):
        tmp = line.strip().split()
        if filemode == "bpseq":
            x, y = int(tmp[0]), int(tmp[2])
        elif filemode == "ct":
            if len(tmp) < 6:
                continue
            x, y = int(tmp[0]), int(tmp[4])
        if x < y:
            links.append((x, y))
        maxindex = x
    sets = []
    for i, item in enumerate(links):
        found = False
        for j, myset in enumerate(sets):
            cross = False
            for item2 in myset:
                if ifcross(item, item2):
                    cross = True
                    break
            if not cross:
                sets[j].add(item)
                found = True
                break
        if not found:
            sets.append(set([item]))

    inputseq = []
    for line in open(myfile):
        tmp = line.strip().split()
        if len(tmp) == 2:
            continue
        x = tmp[1]
        inputseq.append(x)

    result = ["."]*maxindex
    lparen = ["(", "[", "{", "<", "`"]
    rparen = [")", "]", "}", ">", "'"]
    for j, myset in enumerate(sets):
        for item in myset:
            result[item[0]-1] = lparen[j]
            result[item[1]-1] = rparen[j]
    return sets, "".join(inputseq), "".join(result)

if __name__ == "__main__":
    mypath = sys.argv[1]
    files = [os.path.join(mypath, f) for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) and ".paren" not in f]
    a = []
    filemode = files[0].rsplit(".",1)[1]
    for f in files:
        a.append((file_len(f), f))
    a.sort()
    files = [x[1] for x in a]

    structurefile = "logs.structures" if len(sys.argv) <= 2 else sys.argv[2]
    sout = open(structurefile, "w")
    max = 1
    for f in files:
        sets, inputseq, mystr = solve(f)
        print len(sets)
        if len(sets) > max:
            max = len(sets)
        # fout = open(f+".paren", "w")
        # print >> fout, inputseq
        # print >> fout, mystr
        # fout.close()
        print >> sout, inputseq
        print >> sout, mystr
    # print "max:", max
    sout.close()
