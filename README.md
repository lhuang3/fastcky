RNA Sequence Parser for Structure Prediction
============================================


### Linear-Time Sequence Parser (linpar)

#### To Compile
The linear time parser can be compiled with ```cmake``` with following commands:
```
mkdir build
cd build
cmake ..
make linpar
```

Note that there are two external libraries stated in ```CMakeLists.txt``` file:

1. ```-lprofiler``` is from Google Performance Tools, and is used to profile the parser, which is turned __off__ by default.
2. ```-ltcmalloc``` is also from Google Performance Tools, which replaces the default ```malloc``` from ```glibc``` and can bring ~5% speedup.


#### To Run
The linear-time parser can be run with:
```
linpar sequence_file [beam_size]
```

The default beam_size is infinite.
