#!/usr/bin/env python

'''
A basic version of the Rivas&Eddy parsing algorithm on RNA sequences with pseudoknots
The objective is to get the sequence with the most number of pairs only.

Created by Dezhong, 02/09/2017
'''

from __future__ import division
from collections import defaultdict
import os, sys, time

INF=100000.0
DEBUG = False

lb = defaultdict(lambda: "*")
rb = defaultdict(lambda: "*")

_lb = ["(", "[", "{", "<", "`","6","L"]
_rb = [")", "]", "}", ">", "'","9","J"]

for i in xrange(len(_lb)):
    lb[i+1] = _lb[i]
    rb[i+1] = _rb[i]


nonestate = (-INF, 1, ("none", None))
initstate = (0.0, 1, ("init", None))

sc = defaultdict(lambda : defaultdict(lambda : nonestate)) ## semicircle
br = defaultdict(lambda : defaultdict(lambda :defaultdict(
    lambda : defaultdict(lambda : nonestate)))) ## bridge
# state = (score, backptr)
# backptr = (manner, split points)

allowedpairs = ["AU", "CG", "GU", "UA", "GC", "UG",
                "aa", "bb", "cc", "dd", "ee", "ff", "gg"]

seq = sys.argv[1] if len(sys.argv) >= 2 else "abacbc"
seq_L = len(seq)

def tryaddsc(i,j, score, pn, backptr):
    if score > sc[i][j][0] or (score == sc[i][j][0] and pn < sc[i][j][1]):
        sc[i][j] = (score, pn, backptr)
def tryaddbr(i,k,l,j,score, pn, backptr):
    if score > br[i][k][l][j][0] or (score == br[i][k][l][j][0] and pn < br[i][k][l][j][1]):
        br[i][k][l][j] = (score, pn, backptr)

def ppsc(i,j):
    _, pn, (manner, splitpoints) = sc[i][j]
    if DEBUG:
        print "sc",i,j,manner,"#",sc[i][j]

    if manner == "init":
        result = ""
    elif manner == "Sskipl":
        result = "."+ppsc(i+1,j)
    elif manner == "Sskipr":
        result = ppsc(i,j-1)+"."
    elif manner == "Spair":
        result = lb[1] + ppsc(i+1,j-1) + rb[1] ## sc pair will always allow pn 1
    elif manner == "ScombineS":
        (k,) = splitpoints
        result = ppsc(i,k) + ppsc(k,j)
    elif manner == "ScombineB":
        (k,p,q) = splitpoints
        partip, partkq = ppbr(i,p,k,q,0)
        deltab = br[i][p][k][q][1]
        partpk, partqj = ppbr(p,k,q,j,deltab)
        result = partip + partpk + partkq + partqj
    elif manner == "Sfill":
        (k,q) = splitpoints
        partik, partqj = ppbr(i,k,q,j,0)
        result = partik + ppsc(k,q) + parkqj
    elif manner == "none":
        result = ""
    else:
        ##debug
        print "error in ppsc",i,j,manner
        sys.exit(0)

    if DEBUG:
        print "result: sc",i,j,"#", result

    return result

def ppbr(i,k,l,j,b):
    _, pn, (manner, splitpoints) = br[i][k][l][j]
    if DEBUG:
        print "br",i,k,l,j,b,manner,"#",br[i][k][l][j]

    if manner == "init":
        result = "", ""
    elif manner == "BcombineS":
        result = ppsc(i,k), ppsc(l,j)
    elif manner == "Bpairout":
        partleft, partright = ppbr(i+1,k,l,j-1,b)
        result = lb[b+pn]+partleft, partright+rb[b+pn]
    elif manner == "Bpairin":
        partleft, partright = ppbr(i,k-1,l+1,j,b)
        result = partleft+lb[b+pn], rb[b+pn]+partright
    elif manner == "Bskipi":
        partleft, partright = ppbr(i+1,k,l,j,b)
        result = "."+partleft, partright
    elif manner == "Bskipk":
        partleft, partright = ppbr(i,k-1,l,j,b)
        result = partleft+".", partright
    elif manner == "Bskipl":
        partleft, partright = ppbr(i,k,l+1,j,b)
        result = partleft, "."+partright
    elif manner == "Bskipj":
        partleft, partright = ppbr(i,k,l,j-1,b)
        result = partleft, partright+"."
    elif manner == "Bexpandi":
        (r,) = splitpoints
        partleft, partright = ppbr(r,k,l,j,b)
        result = ppsc(i,r)+partleft, partright
    elif manner == "Bexpandk":
        (r,) = splitpoints
        partleft, partright = ppbr(i,r,l,j,b)
        result = partleft+ppsc(r,k), partright
    elif manner == "Bexpandl":
        (r,) = splitpoints
        partleft, partright = ppbr(i,k,r,j,b)
        result = partleft, ppsc(l,r)+partright
    elif manner == "Bexpandj":
        (r,) = splitpoints
        partleft, partright = ppbr(i,k,l,r,b)
        result = partleft, partright+ppsc(r,j)
    elif manner == "Bcombineleft":
        (r,s) = splitpoints
        partir, partsk = ppbr(i,r,s,k,b)
        deltab = br[i][r][s][k][1]
        partrs, partlj = ppbr(r,s,l,j,b+deltab)
        result = partir+partrs+partsk, partlj
    elif manner == "Bcombineright":
        (r,s) = splitpoints
        partik, partrs = ppbr(i,k,r,s,b)
        deltab = br[i][k][r][s][1]
        partlr, partsj = ppbr(l,r,s,j,b+deltab)
        result = partik, partlr+partrs+partsj
    elif manner == "Bcombinecross":
        (r,s) = splitpoints
        partir, partls = ppbr(i,r,l,s,b)
        deltab = br[i][r][l][s][1]
        partrk, partsj = ppbr(r,k,s,j,b+deltab)
        result = partir+partrk, partls+partsj
    elif manner == "Bcombinemid":
        (r,s) = splitpoints
        partir, partsj = ppbr(i,r,s,j,b)
        partrk, partls = ppbr(r,k,l,s,b)
        result = partir+partrk, partls+partsj
    elif manner == "none":
        result = "", ""
    else:
        ##debug
        print "error in ppbr",i,k,l,j,b,manner
        sys.exit(0)

    if DEBUG:
        print "result: br",i,k,l,j,b,"#",result[0]+("".join(["?"]*(l-k)))+result[1]
    return result

def greedypp(ii,jj):

    def ifcross(point1, point2):
        a, b = point1
        c, d = point2
        if a<c<b<d or c<a<d<b:
            return True
        else:
            return False

    links = getlinksc(ii,jj)
    links.sort()
    sets = []
    for i, item in enumerate(links):
        found = False
        for j, myset in enumerate(sets):
            cross = False
            for item2 in myset:
                if ifcross(item, item2):
                    cross = True
                    break
            if not cross:
                sets[j].add(item)
                found = True
                break
        if not found:
            sets.append(set([item]))

    result = ["."]*(jj-ii)
    for j, myset in enumerate(sets):
        for item in myset:
            result[item[0]] = lb[j+1]
            result[item[1]] = rb[j+1]
    return "".join(result), len(sets)

def getlinksc(i,j):
    _, pn, (manner, splitpoints) = sc[i][j]
    if DEBUG:
        print "sc",i,j,manner,"#",sc[i][j]

    if manner == "init":
        result = []
    elif manner == "Sskipl":
        result = getlinksc(i+1,j)
    elif manner == "Sskipr":
        result = getlinksc(i,j-1)
    elif manner == "Spair":
        result = [(i,j-1)] + getlinksc(i+1,j-1)
    elif manner == "ScombineS":
        (k,) = splitpoints
        result = getlinksc(i,k) + getlinksc(k,j)
    elif manner == "ScombineB":
        (k,p,q) = splitpoints
        result = getlinkbr(i,p,k,q) + getlinkbr(p,k,q,j)
    elif manner == "Sfill":
        (k,q) = splitpoints
        result = getlinksc(k,q) + getlinkbr(i,k,q,j)
    elif manner == "none":
        result = []
    else:
        ##debug
        print "error in ppsc",i,j,manner
        sys.exit(0)

    if DEBUG:
        print "result: sc",i,j,"#", result

    return result

def getlinkbr(i,k,l,j):
    _, pn, (manner, splitpoints) = br[i][k][l][j]
    if DEBUG:
        print "br",i,k,l,j,b,manner,"#",br[i][k][l][j]

    if manner == "init":
        result = []
    elif manner == "BcombineS":
        result = getlinksc(i,k) + getlinksc(l,j)
    elif manner == "Bpairout":
        result = [(i,j-1)] + getlinkbr(i+1,k,l,j-1)
    elif manner == "Bpairin":
        result = [(k-1, l)] + getlinkbr(i,k-1,l+1,j)
    elif manner == "Bskipi":
        result = getlinkbr(i+1,k,l,j)
    elif manner == "Bskipk":
        result = getlinkbr(i,k-1,l,j)
    elif manner == "Bskipl":
        result = getlinkbr(i,k,l+1,j)
    elif manner == "Bskipj":
        result = getlinkbr(i,k,l,j-1)
    elif manner == "Bexpandi":
        (r,) = splitpoints
        result = getlinksc(i,r) + getlinkbr(r,k,l,j)
    elif manner == "Bexpandk":
        (r,) = splitpoints
        result = getlinksc(r,k) + getlinkbr(i,r,l,j)
    elif manner == "Bexpandl":
        (r,) = splitpoints
        result = getlinksc(l,r) + getlinkbr(i,k,r,j)
    elif manner == "Bexpandj":
        (r,) = splitpoints
        result = getlinksc(r,j) + getlinkbr(i,k,l,r)
    elif manner == "Bcombineleft":
        (r,s) = splitpoints
        result = getlinkbr(i,r,s,k) + getlinkbr(r,s,l,j)
    elif manner == "Bcombineright":
        (r,s) = splitpoints
        result = getlinkbr(i,k,r,s) + getlinkbr(l,r,s,j)
    elif manner == "Bcombinecross":
        (r,s) = splitpoints
        result = getlinkbr(i,r,l,s) + getlinkbr(r,k,s,j)
    elif manner == "Bcombinemid":
        (r,s) = splitpoints
        result = getlinkbr(i,r,s,j) + getlinkbr(r,k,l,s)
    elif manner == "none":
        result = []
    else:
        ##debug
        print "error in ppbr",i,k,l,j,b,manner
        sys.exit(0)

    if DEBUG:
        print "result: br",i,k,l,j,b,"#",result[0]+("".join(["?"]*(l-k)))+result[1]
    return result

starttime = time.time()

for j in xrange(seq_L+1):
    sc[j][j] = initstate
    for i in xrange(j-1,-1,-1):

        tryaddsc(i,j, sc[i+1][j][0], sc[i+1][j][1], ("Sskipl", None))
        tryaddsc(i,j, sc[i][j-1][0], sc[i][j-1][1], ("Sskipr", None))

        if seq[i]+seq[j-1] in allowedpairs:
            tryaddsc(i,j, sc[i+1][j-1][0]+1, sc[i+1][j-1][1], ("Spair", None))

        for k in xrange(i+1, j):
            tryaddsc(i,j, sc[i][k][0]+sc[k][j][0], max(sc[i][k][1], sc[k][j][1]), ("ScombineS", (k,)))

        for k in xrange(i+2, j-1):
            for p in xrange(i+1,k):
                for q in xrange(k+1,j):
                    tryaddsc(i,j, br[i][p][k][q][0]+br[p][k][q][j][0],
                             br[i][p][k][q][1]+br[p][k][q][j][1], ("ScombineB", (k,p,q)))

        for k in xrange(i+1, j-1):
            for q in xrange(k+1, j):
                tryaddsc(i,j, br[i][k][q][j][0]+sc[k][q][0],
                         max(br[i][k][q][j][1],sc[k][q][1]), ("Sfill", (k,q)))

        br[i][i][j][j] = initstate

        for k in xrange(i+1,j-1):
            for l in xrange(k+1, j):
                tryaddbr(i,k,l,j,sc[i][k][0]+sc[l][j][0], max(sc[i][k][1],sc[l][j][1]), ("BcombineS", None))

                if seq[i]+seq[j-1] in allowedpairs:
                    tryaddbr(i,k,l,j,br[i+1][k][l][j-1][0]+1, br[i+1][k][l][j-1][1], ("Bpairout", None))

                if seq[k-1]+seq[l] in allowedpairs:
                    tryaddbr(i,k,l,j,br[i][k-1][l+1][j][0]+1, br[i][k-1][l+1][j][1], ("Bpairin", None))

                tryaddbr(i,k,l,j,br[i+1][k][l][j][0], br[i+1][k][l][j][1], ("Bskipi", None))
                tryaddbr(i,k,l,j,br[i][k-1][l][j][0], br[i][k-1][l][j][1], ("Bskipk", None))
                tryaddbr(i,k,l,j,br[i][k][l+1][j][0], br[i][k][l+1][j][1], ("Bskipl", None))
                tryaddbr(i,k,l,j,br[i][k][l][j-1][0], br[i][k][l][j-1][1], ("Bskipj", None))

                for r in xrange(i+1, k):
                    tryaddbr(i,k,l,j,br[r][k][l][j][0]+sc[i][r][0], max(br[r][k][l][j][1],sc[i][r][1]), ("Bexpandi", (r,)))
                for r in xrange(i+1, k):
                    tryaddbr(i,k,l,j,br[i][r][l][j][0]+sc[r][k][0], max(br[i][r][l][j][1],sc[r][k][1]), ("Bexpandk", (r,)))
                for r in xrange(l+1, j):
                    tryaddbr(i,k,l,j,br[i][k][r][j][0]+sc[l][r][0], max(br[i][k][r][j][1],sc[l][r][1]), ("Bexpandl", (r,)))
                for r in xrange(l+1, j):
                    tryaddbr(i,k,l,j,br[i][k][l][r][0]+sc[r][j][0], max(br[i][k][l][r][1],sc[r][j][1]), ("Bexpandj", (r,)))

                for r in xrange(i+1, k-1):
                    for s in xrange(r+1, k):
                        tryaddbr(i,k,l,j,br[i][r][s][k][0]+br[r][s][l][j][0], br[i][r][s][k][1]+br[r][s][l][j][1], ("Bcombineleft", (r,s)))

                for r in xrange(l+1, j-1):
                    for s in xrange(r+1, j):
                        tryaddbr(i,k,l,j,br[l][r][s][j][0]+br[i][k][r][s][0], br[l][r][s][j][1]+br[i][k][r][s][1], ("Bcombineright", (r,s)))

                for r in xrange(i+1, k):
                    for s in xrange(l+1, j):
                        tryaddbr(i,k,l,j,br[i][r][l][s][0]+br[r][k][s][j][0], br[i][r][l][s][1]+br[r][k][s][j][1], ("Bcombinecross", (r,s)))
                        tryaddbr(i,k,l,j,br[i][r][s][j][0]+br[r][k][l][s][0], max(br[i][r][s][j][1],br[r][k][l][s][1]), ("Bcombinemid", (r,s)))

best = sc[0][seq_L]
structure, pagenumber = greedypp(0,seq_L)
print "score:",best[0], "page number:", pagenumber
print "len: ", seq_L, "time: ", time.time() - starttime
print seq
print ">structure"
print structure

# print "score:",best[0], "page number:", best[1]
# print "len: ", seq_L, "time: ", time.time() - starttime
# print seq
# print ">structure"
# print ppsc(0,seq_L)
