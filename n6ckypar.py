#!/usr/bin/env python

'''
A basic version of the Rivas&Eddy parsing algorithm on RNA sequences with pseudoknots
The objective is to get the sequence with the most number of pairs only.

Created by Dezhong, 02/09/2017
'''

from __future__ import division
from collections import defaultdict
import os, sys, time

INF=100000.0
DEBUG = False

lb = defaultdict(lambda: "*")
rb = defaultdict(lambda: "*")

_lb = ["(", "[", "{", "<", "`","6","L"]
_rb = [")", "]", "}", ">", "'","9","J"]

for i in xrange(len(_lb)):
    lb[i] = _lb[i]
    rb[i] = _rb[i]


nonestate = (-INF, ("none", None))
initstate = (0.0, ("init", None))

sc = defaultdict(lambda : defaultdict(lambda : nonestate)) ## semicircle
br = defaultdict(lambda : defaultdict(lambda :defaultdict(
    lambda : defaultdict(lambda : nonestate)))) ## bridge
# state = (score, backptr)
# backptr = (manner, split points)

allowedpairs = ["AU", "CG", "GU", "UA", "GC", "UG",
                "aa", "bb", "cc", "dd", "ee", "ff", "gg"]

seq = sys.argv[1] if len(sys.argv) >= 2 else "abacbc"
seq_L = len(seq)

def tryaddsc(i,j, score, backptr):
    if score > sc[i][j][0]:
        sc[i][j] = (score, backptr)
def tryaddbr(i,k,l,j,score, backptr):
    if score > br[i][k][l][j][0]:
        br[i][k][l][j] = (score, backptr)

def ppsc(i,j,b):
    _, (manner, splitpoints) = sc[i][j]
    if DEBUG:
        print "sc",i,j,b,manner,"#",sc[i][j]

    if manner == "init":
        return "", b
    elif manner == "Sskipl":
        return "."+ppsc(i+1,j,b)[0], b
    elif manner == "Sskipr":
        return ppsc(i,j-1,b)[0]+".", b
    elif manner == "Spair":
        return lb[b] + ppsc(i+1,j-1,b)[0] + rb[b], b
    elif manner == "ScombineS":
        (k,) = splitpoints
        return ppsc(i,k,b)[0] + ppsc(k,j,b)[0], b
    elif manner == "ScombineB":
        (k,p,q) = splitpoints
        partip, partkq, b1 = ppbr(i,p,k,q,b)
        partpk, partqj, _ = ppbr(p,k,q,j,b1)
        return partip + partpk + partkq + partqj, b
    elif manner == "Sfill":
        (k,q) = splitpoints
        partik, partqj, _ = ppbr(i,k,q,j,b)
        return partik + ppsc(k,q,b)[0] + parkqj
    elif manner == "none":
        return "", b
    else:
        ##debug
        print "error in ppsc",i,j,b,manner
        sys.exit(0)

def ppbr(i,k,l,j,b):
    _, (manner, splitpoints) = br[i][k][l][j]
    if DEBUG:
        print "br",i,k,l,j,manner,"#",br[i][k][l][j]

    if manner == "init":
        return "", "", b
    elif manner == "BcombineS":
        return ppsc(i,k,b)[0], ppsc(l,j,b)[0], b
    elif manner == "Bpairout":
        partleft, partright, b1 = ppbr(i+1,k,l,j-1,b)
        return lb[b]+partleft, partright+rb[b], max(b+1,b1)
    elif manner == "Bpairin":
        partleft, partright, b1 = ppbr(i,k-1,l+1,j,b)
        return partleft+lb[b], rb[b]+partright, max(b+1,b1)
    elif manner == "Bskipi":
        partleft, partright, b1 = ppbr(i+1,k,l,j,b)
        return "."+partleft, partright, b1
    elif manner == "Bskipk":
        partleft, partright, b1 = ppbr(i,k-1,l,j,b)
        return partleft+".", partright, b1
    elif manner == "Bskipl":
        partleft, partright, b1 = ppbr(i,k,l+1,j,b)
        return partleft, "."+partright, b1
    elif manner == "Bskipj":
        partleft, partright, b1 = ppbr(i,k,l,j-1,b)
        return partleft, partright+".", b1
    elif manner == "Bexpandi":
        (r,) = splitpoints
        partleft, partright, b1 = ppbr(r,k,l,j,b)
        return ppsc(i,r,b)[0]+partleft, partright, b1
    elif manner == "Bexpandk":
        (r,) = splitpoints
        partleft, partright, b1 = ppbr(i,r,l,j,b)
        return partleft+ppsc(r,k,b)[0], partright, b1
    elif manner == "Bexpandl":
        (r,) = splitpoints
        partleft, partright, b1 = ppbr(i,k,r,j,b)
        return partleft, ppsc(l,r,b)[0]+partright, b1
    elif manner == "Bexpandj":
        (r,) = splitpoints
        partleft, partright, b1 = ppbr(i,k,l,r,b)
        return partleft, partright+ppsc(r,j,b)[0], b1
    elif manner == "Bcombineleft":
        (r,s) = splitpoints
        partir, partsk, b1 = ppbr(i,r,s,k,b)
        partrs, partlj, b2 = ppbr(r,s,l,j,b1)
        return partir+partrs+partsk, partlj, b2
    elif manner == "Bcombineright":
        (r,s) = splitpoints
        partlr, partsj, b1 = ppbr(l,r,s,j,b)
        partik, partrs, b2 = ppbr(i,k,r,s,b1)
        return partik, partlr+partrs+partsj, b2
    elif manner == "Bcombinecross":
        (r,s) = splitpoints
        partir, partls, b1 = ppbr(i,r,l,s,b)
        partrk, partsj, b2 = ppbr(r,k,s,j,b1)
        return partir+partrk, partls+partsj, b2
    elif manner == "Bcombinecross":
        (r,s) = splitpoints
        partir, partsj, b1 = ppbr(i,r,s,j,b)
        partrk, partls, b2 = ppbr(r,k,l,s,b)
        return partir+partrk, partls+partsj, max(b1+b2)
    elif manner == "none":
        return "", "", b
    else:
        ##debug
        print "error in ppbr",i,k,l,j,b,manner
        sys.exit(0)

starttime = time.time()

for j in xrange(seq_L+1):
    sc[j][j] = initstate
    for i in xrange(j-1,-1,-1):

        tryaddsc(i,j, sc[i+1][j][0], ("Sskipl", None))
        tryaddsc(i,j, sc[i][j-1][0], ("Sskipr", None))

        if seq[i]+seq[j-1] in allowedpairs:
            tryaddsc(i,j, sc[i+1][j-1][0]+1, ("Spair", None))

        for k in xrange(i+1, j):
            tryaddsc(i,j, sc[i][k][0]+sc[k][j][0], ("ScombineS", (k,)))

        for k in xrange(i+2, j-1):
            for p in xrange(i+1,k):
                for q in xrange(k+1,j):
                    tryaddsc(i,j, br[i][p][k][q][0]+br[p][k][q][j][0], ("ScombineB", (k,p,q)))

        for k in xrange(i+1, j-1):
            for q in xrange(k+1, j):
                tryaddsc(i,j, br[i][k][q][j][0]+sc[k][q][0], ("Sfill", (k,q)))

        br[i][i][j][j] = initstate

        for k in xrange(i+1,j-1):
            for l in xrange(k+1, j):
                tryaddbr(i,k,l,j,sc[i][k][0]+sc[l][j][0], ("BcombineS", None))

                if seq[i]+seq[j-1] in allowedpairs:
                    tryaddbr(i,k,l,j,br[i+1][k][l][j-1][0]+1, ("Bpairout", None))

                if seq[k-1]+seq[l] in allowedpairs:
                    tryaddbr(i,k,l,j,br[i][k-1][l+1][j][0]+1, ("Bpairin", None))

                tryaddbr(i,k,l,j,br[i+1][k][l][j][0], ("Bskipi", None))
                tryaddbr(i,k,l,j,br[i][k-1][l][j][0], ("Bskipk", None))
                tryaddbr(i,k,l,j,br[i][k][l+1][j][0], ("Bskipl", None))
                tryaddbr(i,k,l,j,br[i][k][l][j-1][0], ("Bskipj", None))

                for r in xrange(i+1, k):
                    tryaddbr(i,k,l,j,br[r][k][l][j][0]+sc[i][r][0], ("Bexpandi", (r,)))
                for r in xrange(i+1, k):
                    tryaddbr(i,k,l,j,br[i][r][l][j][0]+sc[r][k][0], ("Bexpandk", (r,)))
                for r in xrange(l+1, j):
                    tryaddbr(i,k,l,j,br[i][k][r][j][0]+sc[l][r][0], ("Bexpandl", (r,)))
                for r in xrange(l+1, j):
                    tryaddbr(i,k,l,j,br[i][k][l][r][0]+sc[r][j][0], ("Bexpandj", (r,)))

                for r in xrange(i+1, k-1):
                    for s in xrange(r+1, k):
                        tryaddbr(i,k,l,j,br[i][r][s][k][0]+br[r][s][l][j][0], ("Bcombineleft", (r,s)))

                for r in xrange(l+1, j-1):
                    for s in xrange(r+1, j):
                        tryaddbr(i,k,l,j,br[l][r][s][j][0]+br[i][k][r][s][0], ("Bcombineright", (r,s)))

                for r in xrange(i+1, k):
                    for s in xrange(l+1, j):
                        tryaddbr(i,k,l,j,br[i][r][l][s][0]+br[r][k][s][j][0], ("Bcombinecross", (r,s)))
                        tryaddbr(i,k,l,j,br[i][r][s][j][0]+br[r][k][l][s][0], ("Bcombinemid", (r,s)))

best = sc[0][seq_L]
print best[0]
print "len: ", seq_L, "time: ", time.time() - starttime
print seq
print ">structure"
print ppsc(0,seq_L,0)[0]
