#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=brktlen.score.pdf' 

set key top right
set key spacing 2.5 
set key samplen 1.5
set size 0.5
set size ratio 0.8
set xlabel "base pair distance"
set ylabel "average F1" offset 1.5,0
set xtics 200
set xrange[0:4000]
# set yrange [10:60]

#set offset 1,1,1,1

plot "../f1_by_len/log.Mathewsf1bylen_beamckypar_beam100" u 2:13 w lp ps 1 lw 4 t "linear, b=100", \
     "../f1_by_len/log.Mathewsf1bylen_Zuker" u 2:13 w lp ps 0.7 lw 4 t "exact Zuker" , \
     "../f1_by_len/log.Mathewsf1bylen_rnaf1bylen" u 2:13 w lp ps 0.7 lw 4 t "RNAstructure" , \
     "../f1_by_len/log.Mathewsf1bylen_vienna_viterbi" u 2:13 w lp ps 0.7 lw 4 t "vienna" 
     # 70 notitle, \
