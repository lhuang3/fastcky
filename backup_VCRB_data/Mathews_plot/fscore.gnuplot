#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=fscore.pdf' 

#set xrange [0:15]
#set logscale x
#set yrange [-1:6]
set key bottom right
set key spacing 2.5
set key samplen 1.5 # important
set size 0.5
set size ratio 0.8
set xrange [0:0.06]
set xtics 0.06
# set yrange [48:68]

set xlabel "average time (seconds) per sequence"
set ylabel "average F-1 score" offset 1.5,0
#set title "linear-time dynamic programming vs. cubic-time CKY"

plot "<(sed -n '1,5p' ../Mathews_avgtime_list)" u 3:4 w lp ps 0.5 lw 4 t "linear DP w/ beam {/Arial-Italic b}" , \
     "" u 3:4:(sprintf("{/Arial-Italic b}=%d", $2)) w labels offset char 1.5,0.5 font "Symbol,8" notitle, \
     "<(tail -1 ../Mathews_avgtime_list)" u 2:3 w p ps 0.5 lw 2 lc 3 t "exact Zuker", \
     "<(sed -n '14p' ../Mathews_avgtime_list)" u 2:3 w p ps 0.5 lw 2 lc 3 t "vienna", \
     63.14 t "Contrafold MEA", \
     61.44 t "Vienna MEA"
     #"vienna.noknots.log" u 4:6 w p ps 0.5 lw 2 lc 4 t "Vienna", \