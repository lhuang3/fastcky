//
// Created by Kai Zhao on 11/8/16.
//

#include "linpar.h"

#include <algorithm>
#include <unordered_set>
#include <functional>
#include <iostream>
#include <tuple>
#include <sys/time.h>

#include "fastcky_w.h"
#include "utils.h"


using namespace std;

#define BEAMSIZE INF
// #define DO_JUMP
// #define ALLOW_MULTI
// #define O_TO_P
// #define DEBUG


// ------------ from utils.h --------
char global_bases[MAXN];
short global_ints[MAXN];
int global_L;
bool allowedpairposition[MAXN][MAXN];
bool allowedunpairposition[MAXN][MAXN];

double cachesingle[SINGLE_MAX_LEN+1][SINGLE_MAX_LEN+1];


namespace std{
    namespace
    {

        // Code from boost
        // Reciprocal of the golden ratio helps spread entropy
        //     and handles duplicates.
        // See Mike Seymour in magic-numbers-in-boosthash-combine:
        //     http://stackoverflow.com/questions/4948780

        template <class T>
        inline void hash_combine(std::size_t& seed, T const& v)
        {
            seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        // Recursive template code derived from Matthieu M.
        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
            static void apply(size_t& seed, Tuple const& tuple)
            {
                HashValueImpl<Tuple, Index-1>::apply(seed, tuple);
                hash_combine(seed, std::get<Index>(tuple));
            }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple,0>
        {
            static void apply(size_t& seed, Tuple const& tuple)
            {
                hash_combine(seed, std::get<0>(tuple));
            }
        };
    }

    template <typename ... TT>
    struct hash<std::tuple<TT...>>
    {
        size_t
        operator()(std::tuple<TT...> const& tt) const
        {
            size_t seed = 0;
            HashValueImpl<std::tuple<TT...> >::apply(seed, tt);
            return seed;
        }

    };
}


string getShapeStr(Shape shape) {
    switch (shape) {
        case SHAPE_E:
            return "E";
        case SHAPE_M1:
             return "M1";
        case SHAPE_M2:
            return "M2";
        case SHAPE_O:
            return "O";
        case SHAPE_P:
            return "P";
        default:
            assert(false);
            return "";
    }
}


double State::heuristic_at_least[SINGLE_MAX_LEN+1];

auto StateHash = [](const State& s) -> size_t {
    return hash<State::StateSignature>{}(s.signature());
};

auto StateEq = [](const State& s1, const State& s2) -> bool {
    return s1.signature() == s2.signature();
};

auto StateCmp = [](const State& a, const State& b) -> bool {
    return a.score_ + a.heuristic_ > b.score_ + b.heuristic_;
};


State NullState(SHAPE_NONE, -1, -1, -1, -1, -1, -1);


State::State(Shape shape, int i, int j, int l=0, int l1=0, int l2=0, int status=0)
        :shape_(shape), i_(i), j_(j), l_(l), l1_(l1), l2_(l2),
         status_(status), boundary_gap_l_(false), boundary_gap_r_(false),
         score_(0), heuristic_(0), inside_(0), backptr1_(NULL), backptr2_(NULL),
         signature_(make_tuple(SHAPE_NONE, 0, 0, 0, 0, 0, 0, 0, 0)){ }


string State::pp() const {
    if (i_ == -1 && j_ == 0) return "";

    const State *parent1 = backptr1_;
    const State *parent2 = backptr2_;
    if (manner_ == MANNER_PUSH) return "(";
    string s =  parent1->pp();
    string s2, s3;
    switch (manner_) {
        case MANNER_SKIP:
            return s + ".";
        case MANNER_LINK:
            return s + ")";
        case MANNER_REDUCE:
            s2 = parent2->pp();
            return s + s2;
#ifdef O_TO_P
        case MANNER_OTOPLINK:
          s2 = parent2->pp();
          s3 = std::string(j_-parent2->j_-1, '.');
          return s + s2 + s3 + ")";
#endif
        default:
            break;
    }
    assert(false);
    return "";
}


string State::id() const {
    char buf[64];
    sprintf(buf, "%p", this);
    string ret = string(buf);
    return ret.substr(ret.length()-5);
}

string State::pp_backptr() const {
    char buf[512];
    if (manner_ == MANNER_REDUCE)
        sprintf(buf, "(reduce, %s, %s)", backptr1_->id().c_str(), backptr2_->id().c_str());
    else if (manner_ == MANNER_OTOPLINK)
      sprintf(buf, "(OtoPlink, %s, %s)", backptr1_->id().c_str(),backptr2_->id().c_str());
    else if (backptr1_==NULL)
        sprintf(buf, manner_==MANNER_PUSH?"(push)":"(init)");
    else sprintf(buf, "(%s, %s)",
                 manner_==MANNER_SKIP?"skip":"link",
                 backptr1_->id().c_str());
    return string(buf);
}

string State::to_str() const {
    string shape = getShapeStr(shape_);
    char buf[1024];
    sprintf(buf, "%s %s (%d, %d) tot=%.2f sc=%.2f in=%.2f %s", id().c_str(), shape.c_str(), i_, j_,
             score_+heuristic_, score_, inside_, pp_backptr().c_str());
    string ret(buf);
    if (shape_ == SHAPE_P) {
        replace(ret.begin(), ret.end(), '(', '[');
        replace(ret.begin(), ret.end(), ')', ']');
    };
    if (shape_ == SHAPE_O)
        ret += " st=" + to_string(status_);
    if (shape_ == SHAPE_O) {
        sprintf(buf, " bgaps=%s l=%d l1=%d l2=%d",
                ("("+ to_string(boundary_gap_l_) + to_string(boundary_gap_r_)+")").c_str(),
                l_, l1_, l2_);
        ret += buf;
    }
    //if (manner_ == MANNER_PUSH)
        ret += " #lptrs=" + to_string(leftptrs_.size());
    ret += " " + pp();
    return ret;
}

State::StateSignature State::signature() const {
    if (get<0>(signature_) == SHAPE_NONE) {
        switch (shape_) {
            case SHAPE_E:
                signature_ = make_tuple(shape_, 0, j_, 0, 0, 0, 0, 0, 0);
                break;
            case SHAPE_P: case SHAPE_M1: case SHAPE_M2:
                signature_ = make_tuple(shape_, i_, j_, 0, 0, 0, 0, 0, 0);
                break;
            default:
                signature_ = make_tuple(shape_, i_, j_, l_, l1_, l2_,
                                        status_, boundary_gap_l_, boundary_gap_r_);
        }
        /*
        if (shape_ == SHAPE_E) signature_ = to_string(shape_) + "|" + to_string(j_) ;
        else if (shape_ == SHAPE_P || shape_ == SHAPE_M1 || shape_ == SHAPE_M2)
            signature_ = to_string(shape_) + "|" + to_string(i_) + "|" + to_string(j_);
        else // shape O
            signature_ = to_string(shape_) + "|" + to_string(i_) + "|" + to_string(j_)
                    + "|" + to_string(l_) + "|" + to_string(l1_) + "|" + to_string(l2_)
                    + "|" + to_string(status_) + "|"
                         + to_string(boundary_gap_l_) + "|" + to_string(boundary_gap_r_);
         */
    }
    return signature_;
}


// j will be unpaired
State State::skip() const {
    auto state = State(shape_, i_, j_+1, l_+1, l1_, l2_, status_);
    double sc = 0;
    if (shape_ == SHAPE_E) {
        sc = external_unpaired;
    } else if (shape_ == SHAPE_O) {
        if (status_ == 0) {
            state.boundary_gap_l_ = true;
            state.boundary_gap_r_ = boundary_gap_r_;
            ++state.l1_;
        } else {
            // status == 1
            state.boundary_gap_l_ = boundary_gap_l_;
            state.boundary_gap_r_ = true;
            ++state.l2_;
            if (l_ == 0) {
                int j = j_ - 1;
                int i = i_ + l_ + 1;  // (...(
                // right-to-left
                sc += gethelixclosing(j, i);
                sc += getterminalmismatch(j, i);
            }
        }
    } else {
        // shape M1/M2
        sc = multi_unpaired;
    }

    if (shape_ == SHAPE_O)
        // prepay
        state.heuristic_ = heuristic_ + heuristic_at_least[min(SINGLE_MAX_LEN, state.l_)];
    state.score_ = score_ + sc;
    state.inside_ = inside_ + sc;
    state.manner_ = MANNER_SKIP;
    state.backptr1_ = this;
    state.leftptrs_ = leftptrs_;
    return state;
}


// j will be (, forming shape O
State State::push() const {
    if (shape_ == SHAPE_O && status_ == 1) {
        assert(false);
        return NullState;
    }

    // Dezhong comment: shapeO formed here (st = 0)
    auto state = State(SHAPE_O, j_, j_+1, 0, 0, 0, 0);
    state.score_ = score_;
    state.inside_ = 0;
    state.manner_ = MANNER_PUSH;
    state.leftptrs_.push_back(this);
    state.heuristic_ = heuristic_ + heuristic_at_least[0];
    return state;
}


// i and j will be paired, forming shape P, from O_0/O_2 or M_2
State State::link() const {
  if (ALLOWED_PAIRS(i_, j_)) {
        State state = State(SHAPE_P, i_, j_);  // not advancing j
        double sc = getbasepair(i_, j_);
        if (shape_ == SHAPE_O) {
            if (status_ == 0) {  // no link b/w i--j ==> hairpin
                sc += gethairpin(i_, j_) + gethelixclosing(i_, j_) + getterminalmismatch(i_, j_);
            } else {  // 1 link b/w i--j: stacking, bulge, or internal
                if (l_ == 0)  // boundary_gaps == (false, false): stacking
                    sc += gethelixstacking(i_, j_);
                else {  // bulge or internal
                    sc += getterminalmismatch(i_, j_) + gethelixclosing(i_, j_);
                    sc += cachesingle[l1_>SINGLE_MAX_LEN?SINGLE_MAX_LEN:l1_][l2_>SINGLE_MAX_LEN?SINGLE_MAX_LEN:l2_];
                    if (boundary_gap_l_ && boundary_gap_r_) {
                        // internal
                    //     sc += getinternallength(l_);
                        if (l_ == 2) sc += getinternalnuc(i_+1, j_-1);
                    //     if (1<=l1_ && EXPLICIT_MAX_LEN>=l1_ && 1<=l2_ && EXPLICIT_MAX_LEN>=l2_)
                    //         sc += getinternalexplicit(l1_, l2_);
                    //     if (l1_ == l2_)
                    //         sc += getinternalsym(l1_);
                    //     else
                    //         sc += getinternalasym(l1_, l2_);
                    } else {
                        // bulge
                    //     sc += getbulgelength(l_);
                        if (l_ == 1)
                            sc += getbulgenuc( boundary_gap_l_ ? (i_+1) : (j_-1) );
                    }
                }
            }
        } else {  // M2
            sc += gethelixclosing(i_, j_) + multi_paired;
            if (j_ < global_L-1)
                sc += getdangleleft(i_, j_);
            if (i_ > 0)
                sc += getdangleright(i_, j_);
        }
        state.score_ = score_ + sc;
        state.inside_ = inside_ + sc;
        state.manner_ = MANNER_LINK;
        state.backptr1_ = this;
        state.leftptrs_ = leftptrs_;
        return state;
  } else {
        assert(false);
        return NullState;
  }
}


// combines self (P) with left state (E, M, O) to yield many new states
vector<State> State::reduce() const {
  // TODO: when reducing H->I_(i,j+1), push all possible pairs (from I) to future beams
  // currently, reduce() returns a vector of states and would be added to cands for next beam only
    vector<State> ret;
    for (auto& left : leftptrs_) {
        int i = i_, j = j_, k = left->i_;
        Shape shape = left->shape_;
        auto state = State(shape, k, j+1);
        double sc = 0;
        if (shape == SHAPE_E) {
            // right-to-left
            sc += gethelixclosing(j, i) + external_paired;
            if (j < global_L - 1) sc += getdangleleft(j, i);
            if (i > 0) sc += getdangleright(j, i);
        } else if (shape == SHAPE_O) {
            // O2 can not be leftstate, check push() function
            if (i >0 && j <global_L -1)
                if (left->boundary_gap_l_)  // (.(..) left bulge
                    sc += getterminalmismatch(j, i) + gethelixclosing(j, i);
        } else {  // shape == SHAPE_M2
            state.shape_ = SHAPE_M2;
            if (j < global_L-1) sc += getdangleleft(j, i);
            if (i > 0) sc += getdangleright(j, i);
            sc += multi_paired;
            sc += gethelixclosing(j, i);
        }
        state.score_ = left->score_ + inside_ + sc;
        state.inside_ = left->inside_ + inside_ + sc;
        state.manner_ = MANNER_REDUCE;
        state.backptr1_ = left;
        state.backptr2_ = this;
        state.leftptrs_ = left->leftptrs_;
        state.boundary_gap_l_ = left->boundary_gap_l_;  // ....(...)
        state.boundary_gap_r_ = false;
        state.l1_ = left->l1_;
        state.l2_ = 0;
        state.l_ = left->l_;
        state.status_ = 1; // Dezhong comment: status changed to 1 here
        state.heuristic_ = left->heuristic_;
        ret.push_back(state);
    }
    return ret;
}

vector<State> State::OtoPlink() const {
  vector<State> ret;
  int newi = i_;
  for (int newj = j_; l1_+l2_+newj-j_ <= SINGLE_MAX_LEN && newj < global_L; ++newj) {
    if (ALLOWED_PAIRS(newi, newj)) {
      // number of skip: newj-newi; link [newi] and [newj]

      // variables after skip
      int newl = l_+newj-j_, newl1 = l1_, newl2 = l2_+newj-j_;
      bool newboundary_gap_l = boundary_gap_l_;
      bool newboundary_gap_r = (newj-j_>0)?true:boundary_gap_r_;
      // newbackptr1 is 'this'
      // newleftptrs is same as leftptrs_

      auto state = State(SHAPE_P, newi, newj);
      double sc = getbasepair(newi, newj);

      if (l_ == 0 && newj-j_>0) { // from skip; using old variables
        int j = j_-1;
        int i = i_ + l_ + 1;
        // right-to-left
        sc += gethelixclosing(j, i);
        sc += getterminalmismatch(j, i);
      }

      // from link
      if (newl == 0)
        sc += gethelixstacking(newi, newj);
      else { // bulge or internal
        sc += getterminalmismatch(newi, newj) + gethelixclosing(newi, newj);
        sc += cachesingle[newl1>SINGLE_MAX_LEN?SINGLE_MAX_LEN:newl1][newl2>SINGLE_MAX_LEN?SINGLE_MAX_LEN:newl2];
        if (newboundary_gap_l && newboundary_gap_r) {
          // internal
        //   sc += getinternallength(newl);
          if (newl == 2) sc += getinternalnuc(newi+1, newj-1);
        //   if (1<=newl1 && EXPLICIT_MAX_LEN>=newl1 && 1<=newl2 && EXPLICIT_MAX_LEN>=newl2)
        //     sc += getinternalexplicit(newl1, newl2);
        //   if (newl1 == newl2)
        //     sc += getinternalsym(newl1);
        //   else
        //     sc += getinternalasym(newl1, newl2);
        } else {
          // bulge
        //   sc += getbulgelength(newl);
          if (newl == 1)
            sc += getbulgenuc( newboundary_gap_l ? (newi+1) : (newj-1) );
        }
      }

      state.score_ = score_ + sc;
      state.inside_ = inside_ + sc;
      state.manner_ = MANNER_OTOPLINK;
      state.backptr1_ = backptr1_;
      state.backptr2_ = backptr2_;
      state.leftptrs_ = leftptrs_;
      ret.push_back(state);
    }
  }
  return ret;
}

// (....(...)  collaps l, remove heuristics
State State::O1toM1() const {
    auto state = State(SHAPE_M1, i_, j_);
    double sc = multi_unpaired * l_ + multi_base + multi_paired;
    const State *leftstate = backptr1_, *rightstate = backptr2_;

    int i = rightstate->i_, j = rightstate->j_;
    if (j < global_L -1) sc += getdangleleft(j, i);
    if (i > 0) sc += getdangleright(j, i);
    // right to left N.B.
    sc += gethelixclosing(j, i);

    state.score_ = leftstate->score_ + rightstate->inside_ + sc;
    state.inside_ = leftstate->inside_ + rightstate->inside_ + sc;
    state.manner_ = manner_;
    state.backptr1_ = backptr1_;
    state.backptr2_ = backptr2_;
    state.leftptrs_ = leftptrs_;
    state.heuristic_ = leftptrs_[0]->heuristic_;  // TODO: is this correct?
    return state;
};


State State::get_init_state(){
    State state = State(SHAPE_E, -1, 0);
    state.score_ = 0;
    state.inside_ = 0;
    state.manner_ = MANNER_INIT;
    state.backptr1_ = NULL;
    state.backptr2_ = NULL;
    return state;
}


void State::init_heuristic_scores() {
    for(int i=0; i<SINGLE_MAX_LEN+1; ++i) {
        heuristic_at_least[i] = min(gethairpinatleast(i), min(getbuldgelengthatleast(i),
                                                              getinternallengthatleast(i)));
#ifdef DEBUG
        printf("%d %f %f %f\t0 %f\n", i, gethairpin(0, i), getbulgelength(i), getinternallength(i),
               heuristic_at_least[i]);
#endif  // DEBUG
    }
}


struct DecoderResult {
    char structure[MAXN];
    double score;
    int numofstates;
    double time;
};


DecoderResult solve(char* seq, unsigned beamsize) {
    typedef unordered_set<State, decltype(StateHash), decltype(StateEq)> BeamStep;

    int num_states[SHAPE_NONE+1] = {0};

    struct timeval starttime, endtime;
    gettimeofday(&starttime, NULL);

    strcpy(global_bases, seq);
    global_L = int(strlen(seq));

    for (int i = 0; i < global_L; ++i)
      global_ints[i] = GET_ACGU_NUM(global_bases[i]);
    // TODO

    State initstate = State::get_init_state();
    vector<BeamStep> beam(global_L+1, BeamStep({}, StateHash, StateEq));
    vector<BeamStep> already_beam(global_L, BeamStep({}, StateHash, StateEq));
    beam[0].insert(initstate);

#ifdef O_TO_P
    vector<vector<State>> realpairs;
    for (int i=0; i < global_L; ++i) {
      vector<State> temp;
      realpairs.push_back(temp);
    }
    vector<State> O_archives;
#endif

#ifdef DEBUG
    printf("-1\n%s\n", initstate.to_str().c_str());
#endif  // DEBUG

    for (int i=0; i<global_L; ++i) {
        vector<State> cands;
        // all states in pairs are temporary: they will be copied and kept in already_beam
        // when necessary
#ifdef O_TO_P
        vector<State>& pairs = realpairs[i];
#else
        vector<State> pairs;
#endif

        BeamStep& laststep = beam[i];

        for (auto& state : laststep) {
            auto newstate = state.skip();
            cands.push_back(newstate);
            ++ num_states[newstate.shape_];
            if (state.shape_ != SHAPE_O || state.status_ != 1) {
                auto newstate = state.push();
                cands.push_back(newstate);
                ++ num_states[newstate.shape_];
            }
            if (state.shape_ == SHAPE_O || state.shape_ == SHAPE_M2) {
                // special pool for Ps
                if (ALLOWED_PAIRS(state.i_, state.j_)) {
                    auto newstate = state.link();
                    pairs.push_back(newstate);
                    ++ num_states[newstate.shape_];
                }
            }
        }

        // -------- pairs ---------
        sort(pairs.begin(), pairs.end(), StateCmp);

        //unordered_set<State, decltype(StateHash), decltype(StateEq)> already({}, StateHash, StateEq);
        auto& already = already_beam[i];
        for(auto& state : pairs) {
            if(already.find(state) == already.end()) {  // throw away equivalent but worse items
                already.insert(state);
                auto& already_state = *already.find(state);
                // TODO: change H->I_(i,j+1) rule and specially added the I states to future beams
                // currently, all return states of reduce() is pushed to current candidates
                for (auto& newstate: already_state.reduce()) {
                    // dezhong: OtoPlink trick
#ifndef O_TO_P
                    ++ num_states[newstate.shape_];
                    cands.push_back(newstate);
#endif
                    if (newstate.shape_ == SHAPE_O) {
                        // O1(....(....) => M1
                        // newstate is from reduce, if newstate.shape_ is O, it must be O1
#ifdef O_TO_P
                      ++ num_states[newstate.shape_];
                      O_archives.push_back(newstate);
                      // OtoPlink: O1 => P
                      for (auto& newstate3: newstate.OtoPlink()) {
                        ++ num_states[newstate3.shape_];
                        int newj = newstate3.j_;
                        realpairs[newj].push_back(newstate3);
                      }
#endif
#ifdef ALLOW_MULTI
                        auto newstate2 = newstate.O1toM1();
                        cands.push_back(newstate2);
                        ++ num_states[newstate2.shape_];
#endif
                    }
#ifdef O_TO_P
                    else {
                      ++ num_states[newstate.shape_];
                      cands.push_back(newstate);
                    }
#endif
                }
            }
        }

        // --------- cands ---------
        sort(cands.begin(), cands.end(), StateCmp);

        if(cands.size() > beamsize) cands.resize(beamsize, NullState);

        //beam.push_back(BeamStep({}, StateHash, StateEq));
        BeamStep& newstep = beam[i+1];
        for (auto& state : cands) {
            auto dup = newstep.find(state);
            if (dup == newstep.end())
                newstep.insert(state);
            else if (state.manner_ == MANNER_PUSH) {
                // shifted (pushed) state
                // the one in beam is better; mergewith it
                for(auto& left : state.leftptrs_)
                    dup->leftptrs_.push_back(left);
            }
        }

#ifdef DEBUG
        printf("%d -----  ****   ==================== ****-------\n", i);
        vector<const State*> tosort;
        for (auto& state : newstep) tosort.push_back(&state);
        sort(tosort.begin(), tosort.end(),
        [](const State* s1, const State* s2) -> bool {
            return StateCmp(*s1, *s2);
        });
        int c = 0;
        for (auto& s : tosort) {
            printf("%d %d : %s\n", i, c++, s->to_str().c_str());
        }
        printf("---\n");
        tosort.clear();
        for (auto& state : already) tosort.push_back(&state);
        sort(tosort.begin(), tosort.end(),
             [](const State* s1, const State* s2) -> bool {
                 return StateCmp(*s1, *s2);
             });
        for (auto& s : tosort)
            printf("%s leftptrs %lu\n", s->to_str().c_str(), s->leftptrs_.size());

#endif  // DEBUG
    }

    BeamStep& laststep = beam.back();

    State best = NullState;
    double best_score = -INF;
    for(auto& state : laststep) {
        if (state.shape_ == SHAPE_E)
            if (state.score_ > best_score) {
                best_score = state.score_;
                best = state;
            }
    }

    gettimeofday(&endtime, NULL);
    double elapsed_time = endtime.tv_sec - starttime.tv_sec
                          + (endtime.tv_usec-starttime.tv_usec)/1000000.0;

    DecoderResult result;
    memset(result.structure, 0, MAXN);
    result.score = best_score;
    result.time = elapsed_time;
    result.numofstates = 0;


    if (best_score != -INF) {
        string structure = best.pp();
        strcpy(result.structure, structure.c_str());

        printf("Viterbi score: %8.5f ", best.score_);

        for(int i=0; i<SHAPE_NONE; ++i){
          result.numofstates += num_states[i];
        }

        printf("Time: %.5f seconds  len: %d  all: %d (", result.time, global_L, result.numofstates);
        for(int i=0; i<SHAPE_NONE; ++i){
            printf("%s: %d%s", getShapeStr(Shape(i)).c_str(), num_states[i], (i==SHAPE_NONE-1)?"":" ");
        }
        printf(")\n");
        //printf("(%s)\n", result.structure);
        //printf("%s\n", global_bases);
        printf(">structure\n%s\n\n", result.structure);
    } else {
        printf("FAILD\n");
        printf("Viterbi score: -10000\n");
    }

    return result;
}

int main(int argc, char** argv){
    assert(argc >= 2);

    initialize();
    initializecachesingle();
#ifdef DO_JUMP
    initialize_4n();
#endif

    State::init_heuristic_scores();

    char* seq_file_name = argv[1];
    FILE* f_seq = fopen(seq_file_name, "r");

    int beamsize = BEAMSIZE;
    if (argc>=3) beamsize = stoi(argv[2]);
    printf("Beam Size %d\n", beamsize);

    // variables for decoding
    char seq[MAXN];
    int num=0, total_len = 0, total_states = 0;
    double total_score = .0;
    double total_time = .0;

    // go through the seq file to decode each seq
    while (fscanf(f_seq, "%s", seq) != EOF) {
        printf("seq:\n%s\n", seq); // passed
        DecoderResult result = solve(seq, beamsize);

        ++num;
        total_len += strlen(seq);
        total_score += result.score;
        total_states += result.numofstates;
        total_time += result.time;
    }

    fclose(f_seq);

    double dnum = (double)num;
    printf("num_seq: %d; avg_len: %.1f ", num, total_len/dnum);
    printf("beam: %d; avg_score: %.4f; avg_time: %.3f; tot_time: %f; avg_states: %.1f\n",
           BEAMSIZE, total_score/dnum, total_time/dnum, total_time, total_states/dnum);
    return 0;
}
