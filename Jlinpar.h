// Linpar with Jump version of transition system.
// The first version does not support multi-loops.
// Created by Dezhong on Jan 2017.

#ifndef FASTCKY_JLINPAR_H
#define FASTCKY_JLINPAR_H

#include <string>
#include <vector>
#include <tuple>

enum Manner {
  MANNER_PUSH=0,         // E(-1,j) => O(j, j_next)
  MANNER_SKIP_E,         // E(-1,j) => E(-1,j+1)
  MANNER_SKIP_M,         // M(i, j) => M(i, j+1)
  MANNER_JUMP,           // O(i, j) => O(i, j_next)
  MANNER_PAIR,           // O(i, j) => P(i, j+1)
  MANNER_PTOM,           // P(i, j) => M(i, j)
  MANNER_EXPAND_FROM_P,  // P(i, j) => P(k, l)
  MANNER_EXPAND_FROM_M2, // M2(i,j) => P(k, l)
  MANNER_REDUCE_E,       // E(-1,i) + P(i, j) => E(-1,j)
  MANNER_REDUCE_M,       // M(k, i) + P(i, j) => M(k, j)
  MANNER_REDUCE_M2,      // M(k, i) + P(i, j) => M2(k,j)
  MANNER_INIT            // => E(-1,0)
}; // now one manner is exactly one rule
enum Shape {
  SHAPE_E = 0, // -1 _ _ _ j
  SHAPE_P, // i( _ _ _ )j
  SHAPE_O, // i( _ _ _ j
  SHAPE_M,
  SHAPE_M2,
  SHAPE_NONE }; // now shape only used in numofstates

std::string getShapeStr(Shape shape);

class State {
 public:
  int i_;
  double score_;
  enum Manner manner_;

  union TraceInfo {
    int split;
    struct {
      char l1;
      char l2;
    } paddings;
  };

  TraceInfo trace;

  State(int i);
  State(int i, double score, Manner manner);
  State(int i, double score, Manner manner, int split);
  State(int i, double score, Manner manner, char l1, char l2);

  std::string to_str(int j, double prescore) const;
  std::string pp_backptr(int j) const;
  std::string id() const;

  int signature() const;

  std::string to_str(int j) const;

  // ignored: heuristic stuff
 private:

};

#endif // FASTCKY_JLINPAR_H
