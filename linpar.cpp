//
// Created by Kai Zhao on 1/29/17.
//

#include "linpar.h"

#include <cassert>
#include <fstream>
#include <sys/time.h>

#include "utility.h"

using namespace std;

struct DecoderResult {
    string structure;
    double score;
    int numofstates;
    double time;
};

struct DecoderResult solve(string& seq) {
    struct timeval starttime, endtime;

    unsigned seq_length = static_cast<int>(seq.length());

    gettimeofday(&starttime, NULL);



    gettimeofday(&endtime, NULL);
    double elapsed_time = endtime.tv_sec - starttime.tv_sec + (endtime.tv_usec-starttime.tv_usec)/1000000.0;

    return {"", 0, 0, elapsed_time};
}


int main(int argc, char** argv){
    assert(argc >= 2);

    initialize();
    initialize_cachesingle();

    char* seq_file_name = argv[1];

    ifstream f_seq(seq_file_name);

    // variables for decoding
    int num=0, total_len = 0, total_states = 0;
    double total_score = .0;
    double total_time = .0;

    // go through the seq file to decode each seq
    for (string seq; getline(f_seq, seq);) {
        printf("seq:\n%s\n", seq.c_str()); // passed

        DecoderResult result = solve(seq);

        ++num;
        total_len += seq.length();
        total_score += result.score;
        total_states += result.numofstates;
        total_time += result.time;
    }

    f_seq.close();

    double dnum = (double)num;
    printf("num_seq: %d; avg_len: %.1f ", num, total_len/dnum);
    printf("avg_score: %.4f; avg_time: %.3f; tot_time: %f; avg_states: %.1f\n",
           total_score/dnum, total_time/dnum, total_time, total_states/dnum);
    return 0;
}