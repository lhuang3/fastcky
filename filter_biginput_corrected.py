# import sys
# for line in open("biginput.corrected"):
#     print line.upper(),


# d = set()
# for line in open("S_Processed.original.Capitalized"):
#     line = line.strip()
#     d.add(line)
# for line in open("biginput.corrected.Capitalized"):
#     line = line.strip()
#     if line not in d:
#         print line

# d = set()
# for line in open("biginput.corrected.notinSProcessed"):
#     line = line.strip()
#     d.add(line)

# d2 = set()
# seqfile = open("biginput.corrected.notinSProcessed.noknots.noT.removeIllegal.seq","w")
# reffile = open("biginput.corrected.notinSProcessed.noknots.noT.removeIllegal.ref","w")
# lines = open("logs.structures").readlines()
# for k in xrange(len(lines) / 3):
#     _seq = lines[3*k].strip().upper()
#     seq = ""
#     for item in _seq:
#         if item in "ACGU":
#             seq += item
#         else:
#             seq += "N"
#     d2.add(seq)
#     ref = lines[3*k+1].strip()
#     pn = int(lines[3*k+2].strip())
#     if seq in d and pn <= 1:
#         print >> seqfile, seq
#         print >> reffile, ref

# seqfile.close()
# reffile.close()

d = set()
for line in open("S_Processed.original.Capitalized"):
    line = line.strip()
    d.add(line)

d2 = set()
seqfile = open("Mathewsdata.notinSProcessed.withpseudo.seq","w")
reffile = open("Mathewsdata.notinSProcessed.withpseudo.ref","w")
generalfile = open("Mathewsdata.notinSProcessed.withpseudo","w")
lines = open("logs.structures").readlines()
for k in xrange(len(lines) / 3):
    _seq = lines[3*k].strip().upper()
    seq = ""
    for item in _seq:
        if item in "ACGU":
            seq += item
        else:
            seq += "N"
    d2.add(seq)
    ref = lines[3*k+1].strip()
    pn = int(lines[3*k+2].strip())
    if seq not in d:
        print >> seqfile, seq
        print >> reffile, ref
        print >> generalfile, pn, len(seq), "A"
        print >> generalfile, seq
        print >> generalfile, ref

seqfile.close()
reffile.close()
generalfile.close()
