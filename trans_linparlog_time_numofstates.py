import sys

finname = sys.argv[1]
foutname = sys.argv[2]

fout = open(foutname, "w")
for line in open(finname):
    if "Viterbi" in line:
        tmp = line.strip().split()
        print >> fout, tmp[7], tmp[4], tmp[9], tmp[11], tmp[13], tmp[15], tmp[17], tmp[19][:-1]
fout.close()
