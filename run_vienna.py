import os, sys, time, subprocess

finname = sys.argv[1]

cnt = -1
tot_time = 0
for line in open(finname):
    cnt += 1
    line = line.strip()
    time0 = time.time()
    output = subprocess.check_output("sed -n %s %s | RNAfold" % (str(cnt+1)+'p', finname), shell=True)
    time1 = time.time()
    tot_time += time1-time0
    print output,
    print "#len:", len(line), "time:", time1-time0
    print
print "avg time", tot_time/cnt
