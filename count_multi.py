''' count multiloop limits 
echo `ls -v ../contrafold-code/full-bpseq-paren/*`| python2.7 count_multi.py| sort -nk1
max 14 (among 133 seqs w/o pseudoknots)

echo `ls -v ../contrafold-code/data-comparna-paren/*`| python2.7 count_multi.py| sort -nk1
max 104 (1% over len 30 among 1070 seqs w/o pseudoknots)
'''

import sys

g_m0 = g_s = -1
for fname in sys.stdin.readline().split():
    line = open(fname).readline().strip()
    if line.find("[") > -1:
        pass #continue # pseudoknots
    stack = [[-1,0]] # (i, count); count is the number of subhelices
    maxh = mleft = mright = m1 = sl = sr = 0
    for i, char in enumerate(line):
        if char == "(":
            stack.append([i, 0])
        elif char == ")":
            lastleft, numhelices = stack[-1]
            if numhelices > 1: # multiloop
                j = 1
                while line[j+lastleft] == ".": # (...(
                    j += 1
                k = 1
                while line[i-k] == ".": # )...)
                    k += 1
                #print "multi", j-1, k-1
                mleft = max(mleft, j-1)
                mright = max(mright, k-1)
            elif numhelices == 1: # single loop
                j = 1
                while line[j+lastleft] == ".": # (...(
                    j += 1
                k = 1
                while line[i-k] == ".": # )...)
                    k += 1
                #if not (j-1 == k-1 == 0):
                #    print "single", j-1, k-1 
                sl = max(sl, j-1)
                sr = max(sr, k-1)                
            stack.pop()
            stack[-1][1] += 1 # number of subhelices inside last level
    print "len", len(line), mleft, mright, sl, sr, fname, line



            
