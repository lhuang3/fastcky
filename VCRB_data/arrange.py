import os, sys, time, subprocess

def cntcheck(mycnt):
    if keyword == "Mathewsdata":
        assert (mycnt == 2900)
    else:
        assert (mycnt == 1241) 

keyword = "Comparnadata" 
files = [f for f in os.listdir("original") if os.path.isfile(os.path.join("original", f)) and keyword in f]

print "files to be processed:"
for f in files:
    print f
print "end list of files."

## output to structure
## structure to f1
## f1 to avgf1
## output+f1 to basepairdistance - aveF1
for f in files:
    print "producing structures/f1 files: %s" % f
    targetf = f.replace("data", "structure")
    if "beamckypar" in f or "Zuker" in f or "contrafold" in f:
        os.system("python ../plot_CV/getcontrafoldstructure.py original/%s > structures/%s" % (f, targetf))
    elif "rnastructure" in f:
        os.system("python ../plot_CV/getrnastructure.py original/%s > structures/%s" %(f, targetf) )
    elif "vienna" in f:
        if "viterbi" in f:
            os.system("python ../plot_CV/getviennastructure.py original/%s Viterbi > structures/%s" %( f, targetf) )
        else:
            os.system("python ../plot_CV/getviennastructure.py original/%s > structures/%s" %( f, targetf) )
    else:
        print "failed to process from output to structure:", f
    os.system("python ../rna.py ../Comparnadata.notinSProcessed.withpseudo.seq ../Comparnadata.notinSProcessed.withpseudo.ref structures/%s >f1/%s" % (targetf, targetf.replace("structure", "f1")) )
    os.system("python ../f1_by_len.py ../Comparnadata.notinSProcessed.withpseudo.seq ../Comparnadata.notinSProcessed.withpseudo.ref structures/%s >f1_by_len/%s" % (targetf, targetf.replace("structure", "f1bylen")) )

## output to time_len plus avg time
if keyword == "Mathewsdata":
    avgtimeout = open("Mathews_avgtime_list", "w")
else:
    avgtimeout = open("Comparna_avgtime_list", "w")

for f in files:
    targetf = f.replace("data", "time")
    fout = open ("time_len/%s" % targetf, "w")
    mytimesum = 0.0
    mycnt = 0
    numofstates = 0
    if "beamckypar" in f or "Zuker" in f or "contrafold_viterbi" in f:
        for line in open("original/"+f):
            if "Time" in line:
                tmp = line.strip().split()
                mytime = float(tmp[1])
                mylen = int(tmp[3])
                mytimesum += mytime
                mycnt += 1
                if "beamckypar" in f or "Zuker" in f:
                    numofstates = float(line.strip().split()[7])
                    print >> fout, mylen, mytime, numofstates
                else:
                    print >> fout, mylen, mytime
    else:
        for line in open("original/"+f):
            if "#len" in line:
                tmp = line.strip().split()
                mytime = float(tmp[3])
                mylen = int(tmp[1])
                mytimesum += mytime
                mycnt += 1
                print >> fout, mylen, mytime
    cntcheck(mycnt)
    avgfscore = subprocess.check_output("tail -1 f1/%s" % f.replace("data", "f1").replace("structure", "f1"), shell=True)
    avgfscore = avgfscore.strip()[:-1].rsplit(" ")[-2]
    if "beamckypar" in f:
        print >> avgtimeout, f, f.rsplit("m",1)[1], mytimesum / mycnt, avgfscore
    else:
        print >> avgtimeout, f, mytimesum / mycnt, avgfscore

avgtimeout.close()

## output to avg model score ## only Zuker and beamckypar
if keyword == "Mathewsdata":
    avgmodelscoreout = open("Mathews_avgmodelscore_list", "w")
else:
    avgmodelscoreout = open("Comparna_avgmodelscore_list", "w")

for f in files:
    if "beamckypar" in f or "Zuker" in f:
        modelscore = 0
        numofstates = 0
        mycnt = 0
        for line in open("original/"+f):
            if "Time" in line:
                modelscore += float(line.strip().split()[5])
                numofstates += float(line.strip().split()[7])
                mycnt += 1
        cntcheck(mycnt)
        if "Zuker" in f:
            print >> avgmodelscoreout, f, numofstates/mycnt, modelscore/mycnt
        else:
            print >> avgmodelscoreout, f, f.rsplit("m",1)[1], numofstates/mycnt, modelscore/mycnt

avgmodelscoreout.close()

## output to avg num of states ## only Zuker and beamckypar
if keyword == "Mathewsdata":
    avgnumofstatesout = open("Mathews_avgnumofstates_list", "w")
else:
    avgnumofstatesout = open("Comparna_avgnumofstates_list", "w")

for f in files:
    if "beamckypar" in f or "Zuker" in f:
        numofstates = 0
        mycnt = 0
        for line in open("original/"+f):
            if "Time" in line:
                numofstates += float(line.strip().split()[7])
                mycnt += 1
        cntcheck(mycnt)
        print >> avgnumofstatesout, f, numofstates/mycnt

avgnumofstatesout.close()

