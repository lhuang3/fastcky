#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=seqlen.score.pdf' 

set key bottom right
set key spacing 2.5 
set key samplen 1.5
set size 0.5
set size ratio 0.8
set xlabel "sequence length"
set ylabel "average F1" offset 1.5,0
set xtics 1000

#set offset 1,1,1,1

plot "b100.seqlen.score.log" u 2:11 w lp ps 0.5 lw 4 t "beam 100", \
     "cky.seqlen.score.log" u 2:11 w lp ps 0.5 lw 4 t "exact Zuker" 
