import sys, os
files = [f for f in os.listdir(".") if os.path.isfile(os.path.join(".", f)) and ".gnuplot" in f]

for f in files:
    os.system("gnuplot %s" % f)
