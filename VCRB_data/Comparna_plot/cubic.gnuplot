#!/usr/bin/env gnuplot

#                   3              5                   8          10
#Viterbi score:  9.07475 Time: 13.99677 seconds  len: 323  all: 950948 (E: 15664, O: 746103, P: 189181)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=cubic.pdf' 

set xrange [0:1000]
#set logscale xy
#set yrange [-2e7:1e9]
#set key 285, 19.5e6
set key spacing 2.8 top left font ",8"
#set key samplen 0 # important
set size 0.52
#set xtics 1000
set size ratio 1.3
set xlabel "sequence length ({/Arial-Italic n})"
set ylabel "number of states generated" offset 29,0
#set title "linear-time dynamic programming vs. cubic-time CKY"
# set label "{/Arial-Italic b}=200" at 2000, 2e8 rotate by 20
# set label "{/Arial-Italic b}=100" at 3000, 0.4e8 rotate by 2
# set label "exact Zuker, \\~{/Arial-Italic n}^{2.5}" at 1300, 3.2e8 rotate by 75
#set label "exact CKY, \\~{/Arial-Italic n}^{3.1}" at 140, 4.4e6 rotate by 78
#     "ckypar.noknots.b20.time.log"   u 4:8 w p ps .8 notitle,\
#     "ckypar.noknots.b50.time.log"   u 4:8 w p ps .8 notitle,\
#     "ckypar.noknots.b200.time.log"   u 4:8 w p ps .8 notitle,\

f1(x) = x*26117 -2.e6 # x**1.165 * exp(8.878)   #b100
f2(x) = x*58152 -5.71e6 # x**1.355 * exp(8.213)   #b200
f3(x) = x**2.473 * exp(1.288)                     #zuker

set xrange [0:1000]

plot \
     "../time_len/log.Comparnatime_beamckypar_beam100"   u 1:3 w p ps .8 t "beam100", \
     "../time_len/log.Comparnatime_Zuker"   u 1:3 w p ps .8 t "Zuker", \
     "../time_len/log.Comparnatime_beamckypar_beam200"        u 1:3 w p ps .8 t "beam200"
     # "../time_len/log.Comparnatime_beamckypar_beam500"        u 1:3 w p ps .8 t "beam500"

     # f1(x) notitle, \
     # f2(x) notitle, \
     # -1e100 notitle, \
     # f3(x) notitle
     #"ckypar.b100.quickselect.time.log"   u 4:14 w p ps .8 notitle
     #"ckypar.time.log" u 4:6 w p ps 0.8 notitle, \
     #"ckypar.b100.quickselect.time.log"   u 4:8 w p ps .8 notitle#, \
     #"ckypar.b500.quickselect.time.log"   u 4:6 w p ps .8 notitle, \
     #"ckypar.b100.quickselect.time.log"   u 4:6 w p ps .8 notitle#, \
     #x*3400-150000 ls 5 notitle, \
     #x**2.69*exp(0.63)+100000 notitle, \
     #x**3.07*exp(-0.72) ls 5 notitle
     
#: {/Arial-Italic n}^{2.7}", \
#: {/Arial-Italic n}^{3.1}" ,\
