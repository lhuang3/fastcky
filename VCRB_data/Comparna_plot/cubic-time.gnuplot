#!/usr/bin/env gnuplot

#                   3              5                   8          10
#Viterbi score:  9.07475 Time: 13.99677 seconds  len: 323  all: 950948 (E: 15664, O: 746103, P: 189181)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=cubic-time.pdf' 

set xrange [100:]
#set logscale xy
#set yrange [-1:1e9]
#set key 285, 19.5e6
# set key spacing 2.6
set key samplen 0 # important
set key top left font ",8"
set size 0.52
set xtics 1000
set size ratio 1.3
set xlabel "sequence length ({/Arial-Italic n})"
set ylabel "average time per sentence" offset 31,0
#set title "linear-time dynamic programming vs. cubic-time CKY"
#set label "linear {/Arial-Italic b}=2000" at 170, 1e6 rotate by 10
#set label "CKY {/Arial-Italic k}=50, \\~{/Arial-Italic n}^{2.7}" at 250, 3.e6 rotate by 68
#set label "exact CKY, \\~{/Arial-Italic n}^{3.1}" at 140, 4.4e6 rotate by 78

plot \
     "../time_len/log.Comparnatime_beamckypar_beam020"   u 1:2 w p ps .8 t "b20",\
     "../time_len/log.Comparnatime_beamckypar_beam050"   u 1:2 w p ps .8 t "b50",\
     "../time_len/log.Comparnatime_beamckypar_beam100"   u 1:2 w p ps .8 t "b100",\
     "../time_len/log.Comparnatime_beamckypar_beam200"   u 1:2 w p ps .8 t "b200",\
     "../time_len/log.Comparnatime_Zuker"        u 1:2 w p ps .8 t "Zuker", \
     "../time_len/log.Comparnatime_contrafold_viterbi"        u 1:2 w p ps .8 t "contrafold", \
     "../time_len/log.Comparnatime_vienna_viterbi"        u 1:2 w p ps .8 t "vienna"

     # "../time_len/log.Comparnatime_rnastructure"        u 1:2 w p ps .8 t "RNAstructure", \
     # "../time_len/log.Comparnatime_vienna_mea"        u 1:2 w p ps .8 t "vienna MEA", \
     # "../time_len/log.Comparnatime_contrafold_mea_6"        u 1:2 w p ps .8 t "contrafold MEA", \
     # "../time_len/log.Comparnatime_beamckypar_beam500"   u 1:2 w p ps .8 t "b500",\     


set output '| epstopdf --filter --outfile=cubic-time-loglog.pdf' 
set logscale xy
unset xrange
unset xtics
replot
     
#: {/Arial-Italic n}^{2.7}", \
#: {/Arial-Italic n}^{3.1}" ,\
