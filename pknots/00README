-------------------------------------------------------------
pknots: An extension of the Zuker MFOLD algorithm to optimal
        minimum energy prediction of pseudoknotted RNA structures. 
        It also includes coaxial energies.

Elena Rivas & Sean Eddy
Janelia Farm HHMI, Ashburn VA, USA
--------------------------------------------------------------
Current release: 1.08 (SEP 2012)

o Getting pknots_v1.08
   WWW home:       http://selab.janelia.org/software.html
   Distribution:   ftp://selab.janelia.org/pub/software/pknots/

o Installing pknots-1.08
   See the file INSTALL for brief instructions.

   You should also read the following files:
   COPYING   -- copyright notice, and information on the free software license
   LICENSE   -- Full text of the GNU Public License, version 2 (see COPYING)

o Running pknots-1.08
  Synopsis:

 Usage: ./pknots [-options] <infile> <outfile>
 where <infile> is a fasta or Stockholm file with sequences to fold
 results in Stockholm format are saved to <outfile>.

  where options are:
  -c          : add L^5 coaxials (V6)
  -g          : save as ct-format files
  -h          : show help and usage
  -k          : allow pseudoknots
  -s          : shuffle sequences
  -t          : print traceback
  -v          : be verbose?
  --infmt <s> : specify format

  Input file:
	to see an example of the type of input files allowed check "Demos/".

   

o Reporting bugs
  If you have any questions, complaints, or suggestions, please e-mail me:

   Elena Rivas
   rivase@janelia.hhmi.org

   Janelia Farm, HHMI
   19700 Helix Drive   
   Ashburn, VA 20147
   USA


References

"A dynamic programming algorithm for RNA structure prediction including pseudoknots." 
     E. Rivas and S.R. Eddy. J. Mol Biol., (285) 2053-2068, 5 February 1999.