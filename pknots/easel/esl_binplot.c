/* Collecting and displaying binplots.
 * 
 *  1. Creating/destroying binplots and collecting data.
 *  2. Output and display of binned data.
 *  3. Test driver.
 *  4. Examples.
 *  
 * ER, Fri Apr  9 13:42:11 EDT 2010 [janelia]
 * SVN $Id:  $
 */
#include "esl_config.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <limits.h>

#include "easel.h"
#include "esl_stats.h"
#include "esl_binplot.h"
#include "esl_vectorops.h"

/*****************************************************************
 * 1. Creating/destroying binplots and collecting data.
 *****************************************************************/

/* Function:  esl_binplot_Create()
 * Synopsis:  Create a new <ESL_BINPLOT>.
 * Incept:    SRE, Fri Jul  1 13:40:26 2005 [St. Louis]
 *
 * Purpose:   Creates and returns a new binplot object, initially
 *            allocated to count x values  $>$ <xmin> and $<=$ <xmax> into
 *            bins of width <w>. Thus, a total of <xmax>-<xmin>/<w> bins
 *            are initially created. 
 *            
 *            The lower bound <xmin> and the width <w> permanently
 *            determine the offset and width of the binning, but not
 *            the range.  For example, <esl_binplot_Create(-100,
 *            100, 0.5)> would init the object to collect scores into
 *            400 bins $[-100< x \leq -99.5],[-99.5 < x \leq
 *            -99.0]...[99.5 <x \leq 100.0]$.  Aside from this, the
 *            range specified by the bounds <xmin> and <xmax> only
 *            needs to be an initial guess. The binplot object will
 *            reallocate itself dynamically as needed to accommodate
 *            scores that exceed current bounds.
 *
 *            You can be sloppy about <xmax>; it does not have to
 *            exactly match a bin upper bound. The initial allocation
 *            is for all full-width bins with upper bounds $\leq
 *            xmax$.
 *
 *            <esl_binplot_Create()> creates a simplified binplot
 *            object that collates only the "display" binplot. For
 *            a more complex object that also keeps the raw data samples,
 *            better suited for fitting distributions and goodness-of-fit
 *            testing, use <esl_binplot_CreateFull()>.
 *  
 * Args:      xmin - caller guesses that minimum score will be > xmin
 *            xmax - caller guesses that max score will be <= xmax
 *            w    - size of bins (1.0, for example)
 *            
 * Returns:   ptr to new <ESL_BINPLOT> object, which caller is responsible
 *            for free'ing with <esl_binplot_Destroy()>.
 *
 * Throws:    <NULL> on allocation failure.
 */
ESL_BINPLOT *
esl_binplot_Create(double xmin, double xmax, double w)
{
  ESL_BINPLOT *p = NULL;
  int status;
  int i;

  ESL_ALLOC(p, sizeof(ESL_BINPLOT));

  p->xmin      =  DBL_MAX;	/* xmin/xmax are the observed min/max */
  p->xmax      = -DBL_MAX;
  p->ymin      =  DBL_MAX;	/* ymin/ymax are the observed min/max */
  p->ymax      = -DBL_MAX;
  p->n         = 0;
  p->obs       = NULL;		/* will get allocated below... */
  p->ymean     = NULL;		/* will get allocated below... */
  p->ystdv     = NULL;		/* will get allocated below... */
  p->bmin      = xmin;		/* bmin/bmax are the allocated bounds */
  p->bmax      = xmax;
  p->nb        = (int)((xmax-xmin)/w);
  p->imin      = p->nb;
  p->imax      = -1;
  p->w         = w;

  p->x         = NULL;
  p->y         = NULL;
  p->nalloc    = 0;
 
  p->is_full     = FALSE;
  p->is_done     = FALSE;
  p->dataset_is  = COMPLETE;

  ESL_ALLOC(p->obs,   sizeof(uint64_t) * p->nb);
  ESL_ALLOC(p->ymean, sizeof(double)   * p->nb);
  ESL_ALLOC(p->ystdv, sizeof(double)   * p->nb);
  for (i = 0; i < p->nb; i++) p->obs[i]   = 0;
  for (i = 0; i < p->nb; i++) p->ymean[i] = 0.0;
  for (i = 0; i < p->nb; i++) p->ystdv[i] = 0.0;
  return p;

 ERROR:
  esl_binplot_Destroy(p);
  return NULL;
}

/* Function:  esl_binplot_CreateFull()
 * Synopsis:  A <ESL_BINPLOT> to keep all data samples.
 * Incept:    ER, Tue Jul 26 13:19:27 2005 [St. Louis]
 *
 * Purpose:   Alternative form of <esl_binplot_Create()> that 
 *            creates a more complex binplot that will contain not just the 
 *            display binplot, but also keeps track of all
 *            the raw sample values. Having a complete vector of raw
 *            samples improves distribution-fitting and goodness-of-fit 
 *            tests, but will consume more memory. 
 */
ESL_BINPLOT *
esl_binplot_CreateFull(double xmin, double xmax, double w)
{
  int status;
  ESL_BINPLOT *p = esl_binplot_Create(xmin, xmax, w);
  if (p == NULL) return NULL;

  p->n      = 0;		/* make sure */
  p->nalloc = 128;		/* arbitrary initial allocation size */
  ESL_ALLOC(p->x, sizeof(double) * p->nalloc);
  ESL_ALLOC(p->y, sizeof(double) * p->nalloc);
  p->is_full = TRUE;
  return p;

 ERROR:
  esl_binplot_Destroy(p);
  return NULL;
}


/* Function:  esl_binplot_Destroy()
 * Synopsis:  Frees a <ESL_BINPLOT>.
 * Incept:    ER, Fri Apr  9 13:33:28 EDT 2010 [janelia]
 *
 * Purpose:   Frees an <ESL_BINPLOT> object <b>.
 */
void
esl_binplot_Destroy(ESL_BINPLOT *p)
{
  if (p ==  NULL) return;
  if (p->x      != NULL) free(p->x);
  if (p->y      != NULL) free(p->y);
  if (p->obs    != NULL) free(p->obs); 
  if (p->ymean  != NULL) free(p->ymean); 
  if (p->ystdv  != NULL) free(p->ystdv); 
  free(p);
  return;
}

/* Function:  esl_binplot_Score2Bin()
 * Synopsis:  Given a real-valued <x>; calculate integer bin <b>
 * Incept:    ER, Fri Apr  9 14:17:53 EDT 2010 [janelia]
 *
 * Purpose:   For a real-valued <x>, figure out what bin it would
 *            go into in the binplot <p>; return this value in
 *            <*ret_b>.
 *
 * Returns:   <eslOK> on success.
 *
 * Throws:    <eslERANGE> if bin <b> would exceed the range of
 *            an integer; for instance, if <x> isn't finite.
 *
 * Xref:      J5/122. Replaces earlier macro implementation;
 *            we needed to range check <x> better.
 */
int
esl_binplot_Score2Bin(ESL_BINPLOT *p, double x, int *ret_b)
{
  int status;

  if (! isfinite(x)) ESL_XEXCEPTION(eslERANGE, "value added to binplot is not finite");

  x = ceil( ((x - p->bmin) / p->w) - 1.); 
  
  /* x is now the bin number as a double, which we will convert to
   * int. Because x is a double (64-bit), we know all ints are exactly
   * represented.  Check for under/overflow before conversion.
   */
  if (x < (double) INT_MIN || x > (double) INT_MAX) 
    ESL_XEXCEPTION(eslERANGE, "value %f isn't going to fit in binplot", x);

  *ret_b = (int) x;
  return eslOK;

 ERROR:
  *ret_b = 0;
  return status;
}

/* Function:  esl_binplot_Add()
 * Synopsis:  Add a sample to the binplot.
 * Incept:    ER, Fri Apr  9 13:36:13 EDT 2010 [janelia]
 *
 * Purpose:   Adds pair of values <x> <y> to a binplot <b>.
 *           
 *            The binplot will be automatically reallocated as
 *            needed if the score is smaller or larger than the
 *            current allocated bounds.  
 *
 * Returns:   <eslOK> on success.
 *
 * Throws:    <eslEMEM> on reallocation failure.
 *
 *            <eslERANGE> if <x> is beyond the reasonable range for
 *            the binplot to store -- either because it isn't finite,
 *            or because the binplot would need to allocate a number
 *            of bins that exceeds <INT_MAX>.
 *
 *            Throws <eslEINVAL> for cases where something has been done
 *            to the binplot that requires it to be 'finished', and
 *            adding more data is prohibited; for example, 
 *            if tail or censoring information has already been set.
 *            On either failure, initial state of <h> is preserved.
 */
int
esl_binplot_Add(ESL_BINPLOT *p, double x, double y)
{
  int   status;
  void *tmp;
  int b;			/* what bin we're in                       */
  int nnew;			/* # of new bins created by a reallocation */
  int bi;

  /* Censoring info must only be set on a finished binplot;
   * don't allow caller to add data after configuration has been declared
   */
  if (p->is_done)
    ESL_EXCEPTION(eslEINVAL, "can't add more data to this binplot");

  /* If we're a full binplot, check whether we need to reallocate
   * the full data vector.
   */
  if (p->is_full && p->nalloc == p->n) 
    {
      ESL_RALLOC(p->x, tmp, sizeof(double) * p->nalloc * 2);
      ESL_RALLOC(p->x, tmp, sizeof(double) * p->nalloc * 2);
      ESL_RALLOC(p->y, tmp, sizeof(double) * p->nalloc * 2);
      p->nalloc *= 2;
    }

  /* Which bin will we want to put x into?
   */
  if ((status = esl_binplot_Score2Bin(p,x, &b)) != eslOK) return status;

  /* Make sure we have that bin. Realloc as needed.
   * If that reallocation succeeds, we can no longer fail;
   * so we can change the state of h.
   */
  if (b < 0)    /* Reallocate below? */
    {				
      nnew = -b*2;	/* overallocate by 2x */
      if (nnew > INT_MAX - p->nb)
	ESL_EXCEPTION(eslERANGE, "value %f requires unreasonable binplot bin number", x);
      ESL_RALLOC(p->obs,   tmp, sizeof(uint64_t) * (nnew+ p->nb));
      ESL_RALLOC(p->ymean, tmp, sizeof(double)   * (nnew+ p->nb));
      ESL_RALLOC(p->ystdv, tmp, sizeof(double)   * (nnew+ p->nb));
      
      memmove(p->obs+nnew,   p->obs,   sizeof(uint64_t) * p->nb);
      memmove(p->ymean+nnew, p->ymean, sizeof(double)   * p->nb);
      memmove(p->ystdv+nnew, p->ystdv, sizeof(double)   * p->nb);
      p->nb    += nnew;
      b        += nnew;
      p->bmin  -= nnew*p->w;
      p->imin  += nnew;
      if (p->imax > -1) p->imax += nnew;
      for (bi = 0; bi < nnew; bi++) p->obs[bi]   = 0;
      for (bi = 0; bi < nnew; bi++) p->ymean[bi] = 0.0;
      for (bi = 0; bi < nnew; bi++) p->ystdv[bi] = 0.0;
    }
  else if (b >= p->nb)  /* Reallocate above? */
    {
      nnew = (b-p->nb+1) * 2; /* 2x overalloc */
      if (nnew > INT_MAX - p->nb) 
	ESL_EXCEPTION(eslERANGE, "value %f requires unreasonable binplot bin number", x);
      ESL_RALLOC(p->obs,   tmp, sizeof(uint64_t) * (nnew+ p->nb));
      ESL_RALLOC(p->ymean, tmp, sizeof(double) * (nnew+ p->nb));
      ESL_RALLOC(p->ystdv, tmp, sizeof(double) * (nnew+ p->nb));
      
      for (bi = p->nb; bi < p->nb+nnew; bi++) p->obs[bi]   = 0;
      for (bi = p->nb; bi < p->nb+nnew; bi++) p->ymean[bi] = 0.0;
      for (bi = p->nb; bi < p->nb+nnew; bi++) p->ystdv[bi] = 0.0;
      if (p->imin == p->nb) { /* boundary condition of no data yet*/
	p->imin+=nnew; 
     }
      p->bmax  += nnew*p->w;
      p->nb    += nnew;
    }

  /* If we're a full binplot, then we keep the raw x value,
   * reallocating as needed.
   */
  if (p->is_full)  p->x[p->n] = x;

  /* add the y value */
  p->ymean[b] += y;
  p->ystdv[b] += y*y;

  /* Bump the bin counter, and all the data sample counters.
   */
  p->obs[b]++;
  p->n++;

  if (b > p->imax) p->imax = b;
  if (b < p->imin) p->imin = b;
  if (x > p->xmax) p->xmax = x;
  if (x < p->xmin) p->xmin = x;
  if (y > p->ymax) p->ymax = y;
  if (y < p->ymin) p->ymin = y;
   return eslOK;

 ERROR:
  return status;
}
  

/*****************************************************************
 * 2. Output and display of binned data.
 *****************************************************************/ 

/* Function:  esl_binplot_Print() 
 * Synopsis:  Print a "pretty" ASCII binplot.
 * Incept:    ER, Fri Apr  9 15:50:14 EDT 2010 [janelia]
 *
 * Purpose:   Print a "prettified" display binplot <h> to a file 
 *            pointer <fp>.
 *            Deliberately a look-and-feel clone of Bill Pearson's 
 *            excellent FASTA output.
 *            
 *            Display will only work well if the bin width (w) is 0.1 or more,
 *            because the score labels are only shown to one decimal point.
 * 
 * Args:      fp     - open file to print to (stdout works)
 *            p      - binplot to print
 *
 * Returns:   <eslOK> on success.
 */
int
esl_binplot_Print(FILE *fp, ESL_BINPLOT *p)
{
  int      i;
  double   x;
  uint64_t maxbar;
  int      imode;
  uint64_t units;
  int      num;
  char     buffer[81];		  /* output line buffer */
  int      pos;			  /* position in output line buffer */
  uint64_t lowcount, highcount;	  
  int      ilowbound, ihighbound; 
  int      emptybins = 3;

  /* Find out how we'll scale the binplot.  We have 58 characters to
   * play with on a standard 80-column terminal display: leading "%6.1f
   * %6d %6d|" occupies 21 chars.  Save the peak position, we'll use
   * it later.
   */
  maxbar = 0;
  imode  = 0;
  for (i = 0; i < p->nb; i++)
    if (p->obs[i] > maxbar) 
      {
	maxbar  = p->obs[i];     /* max height    */
	imode   = i;
      }

  /* Truncate binplot display on both sides, ad hoc fashion.
   * Start from the peak; then move out until we see <emptybins> empty bins,
   * and stop.
   */
  for (num = 0, ihighbound = imode; ihighbound < p->imax; ihighbound++)
    {
      if (p->obs[ihighbound] > 0) { num = 0; continue; } /* reset */
      if (++num == emptybins)     { break;             } /* stop  */
    }
  for (num = 0, ilowbound = imode; ilowbound > p->imin; ilowbound--)
    {
      if (p->obs[ilowbound] > 0)  { num = 0; continue; } /* reset */
      if (++num == emptybins)     { break;             } /* stop  */
    }

		/* collect counts outside of bounds */
  for (lowcount = 0, i = p->imin; i < ilowbound; i++)
    lowcount += p->obs[i];
  for (highcount = 0, i = p->imax; i > ihighbound; i--)
    highcount += p->obs[i];

		/* maxbar might need to be raised now; then set our units  */
  if (lowcount  > maxbar) maxbar = lowcount;
  if (highcount > maxbar) maxbar = highcount;
  units = ((maxbar-1)/ 58) + 1;

  /* Print the binplot
   */
  fprintf(fp, "%6s %6s %6s  (one = represents %llu sequences)\n", 
	  "score", "obs", "exp", (unsigned long long) units);
  fprintf(fp, "%6s %6s %6s\n", "-----", "---", "---");
  buffer[80] = '\0';
  buffer[79] = '\n';
  for (i = p->imin; i <= p->imax; i++)
    {
      memset(buffer, ' ', 79 * sizeof(char));
      x = i*p->w + p->bmin;

      /* Deal with special cases at edges
       */
      if      (i < ilowbound)  continue;
      else if (i > ihighbound) continue;
      else if (i == ilowbound && i != p->imin) 
	{
	  sprintf(buffer, "<%5.1f %6llu %6s|", x+p->w, (unsigned long long) lowcount, "-");
	  if (lowcount > 0) {
	    num = 1+(lowcount-1) / units;
	    for (pos = 21; num > 0; num--)  buffer[pos++] = '=';
	  }
	  fputs(buffer, fp);
	  continue;
	}
      else if (i == ihighbound && i != p->imax)
	{
	  sprintf(buffer, ">%5.1f %6llu %6s|", x, (unsigned long long) highcount, "-");
	  if (highcount > 0) {
	    num = 1+(highcount-1) / units;
	    for (pos = 21; num > 0; num--)  buffer[pos++] = '=';
	  }
	  fputs(buffer, fp);
	  continue;
	}

      /* Deal with most cases
       */
      if (p->obs[i] < 1000000)	/* displayable in 6 figures or less? */
	{
	  sprintf(buffer, "%6.1f %6llu %6s|", x, (unsigned long long) p->obs[i], "-");
	}
      else
	{
	    sprintf(buffer, "%6.1f %6.2e %6s|",   x, (double) p->obs[i], "-");

	}
      buffer[21] = ' ';		/* sprintf writes a null char; replace it */


      /* Mark the binplot bar for observed hits
       */ 
      if (p->obs[i] > 0) {
	num = 1 + (p->obs[i]-1) / units;
	for (pos = 21; num > 0; num--)  buffer[pos++] = '=';
      }
	  
      /* Print the line
       */
      fputs(buffer, fp);
    }

  return eslOK;
}
  
/* Function:  esl_binplot_Plot()
 * Synopsis:  Output a binplot in xmgrace XY format.
 * Incept:    ER, Fri Apr  9 15:54:39 EDT 2010 [janelia]
 *
 * Purpose:   Print observed (and expected, if set) binned counts
 *            in a binplot <h> to open file pointer <fp>
 *            in xmgrace XY input file format.
 *
 * Returns:   <eslOK> on success.
 */
int
esl_binplot_Plot(FILE *fp, ESL_BINPLOT *p)
{
  int    i;
  double x;

  /* First data set is the observed binplot
   */
  for (i = p->imin; i <= p->imax; i++)
    if (p->obs[i] > 0)
      {
	x = esl_binplot_Bin2LBound(p,i);
	fprintf(fp, "%f %6llu %f %f\n", x, (unsigned long long) p->obs[i], p->ymean[i], p->ystdv[i]);
      }
  fprintf(fp, "&\n");

  return eslOK;
}

/*****************************************************************
 * 3. Test driver.
 *****************************************************************/
#ifdef eslBINPLOT_TESTDRIVE
/* compile: 
 *   gcc -g -Wall -I. -L. -o test -DeslBINPLOT_TESTDRIVE esl_binplot.c -leasel -lm
 * run:     
 *   ./test -t1; ./test -t2; ./test -t3; ./test -t4; ./test -t5
 *   
 *   -t1    - complete data, fit to complete Gumbel\n\
 *   -t2    - complete data, high scores fit as censored Gumbel\n\
 *   -t3    - complete data, high scores fit to exponential tail\n\
 *   -t4    - censored data, fit as censored Gumbel\n\
 *   -t5    - complete data, binned, high scores fit to exponential tail\n\
 *
 * Some suggestions for manual testing:
 *   ./test -t1 -j1 -v --surv test.xy; xmgrace test.xy          
 *        examine survivor plot fit, for -t1 
 *        do -t2 thru -t5 too
 *
 *   ./test -t1 --j1 -v -qq test.xy; xmgrace test.xy          
 *        examine QQ plot fit, for -t1 
 *        do -t2 thru -t5 too
 *        
 *   ./test -t1 -v > foo
 *   grep "^Estimated" foo | awk '{print $9}' | sort -g > test.xy
 *        Look for straight line fit to G-test p values.
 *        sub $9->$13 for chi-squared
 *        sub Estimated -> Parametric for the parametric fits
 */
#include <stdio.h>
#include <stdlib.h>

#include "easel.h"
#include "esl_random.h"
#include "esl_getopts.h"

#endif /*eslBINPLOT_TESTDRIVE*/
