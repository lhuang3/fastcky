/* Collection and display of binned xy plots.
 * 
 * ER, Fri Apr  9 13:22:26 EDT 2010 [janelia]
 * SVN $Id: $
 */
#ifndef ESL_BINPLOT_INCLUDED
#define ESL_BINPLOT_INCLUDED

#include <math.h>   /* floor() is in one of the macros */

/* Structure: ESL_BINPLOT
 * 
 * Keeps a xy plot, in which x values are counted into bins of
 * size (width) w. 
 *   binplot starts at bmin <  floor(xmin/w) * w
 *   binplot ends at   bmax >= ceil(xmax/w)*w
 *   nb = (bmax-bmin)/w
 *   each x value is counted into bin b = nb - (int) (bmax-x)/w
 *   each bin b contains x values bw+bmin < x <= (b+1)w + bmin
 * 
 * Anything having to do with the values themselves (n, etc)
 * is a uint64_t, with range 0..2^64-1  (up to 2e19).
 */  
typedef struct {
  /* The binplot is kept as counts in fixed-width bins.
   */
  uint64_t *obs;	/* observed counts in bin b, 0..nb-1 (dynamic)      */
  double   *ymean;	/* observed ymean value in bin b, 0..nb-1 (dynamic) */
  double   *ystdv;	/* observed ystdv value in bin b, 0..nb-1 (dynamic) */

  int       nb;         /* number of bins                                   */
  double    w;		/* fixed width of each bin                          */
  double    bmin, bmax;	/* binplot bounds: all x satisfy bmin < x <= bmax   */
  int       imin, imax;	/* smallest, largest bin that contain obs[i] > 0    */

  /* Optionally, in a "full" b, we can also keep all the raw values in x and y.
   */
  double    xmin, xmax;	/* smallest, largest sample value x observed        */
  double    ymin, ymax;	/* smallest, largest sample value y observed        */
  uint64_t  n;          /* total number of raw data samples                  */
  double   *x;		/* optional: raw sample values x[0..n-1]            */
  double   *y;		/* optional: raw sample values y[0..n-1]            */
  uint64_t  nalloc;	/* current allocated size of samples                */

  /* Some status flags
   */
  int is_full;		/* TRUE when we're keeping raw data in x           */
  int is_done;		/* TRUE if we prevent more Add()'s                 */
  enum { COMPLETE } dataset_is; 

} ESL_BINPLOT;

#define esl_binplot_Bin2LBound(p,b)  ((p)->w*(b) + (p)->bmin)
#define esl_binplot_Bin2UBound(p,b)  ((p)->w*((b)+1) + (p)->bmin)

/* Creating/destroying binplot and collecting data:
 */
extern ESL_BINPLOT *esl_binplot_Create    (double bmin, double bmax, double w);
extern ESL_BINPLOT *esl_binplot_CreateFull(double bmin, double bmax, double w);
extern void         esl_binplot_Destroy(ESL_BINPLOT *p);
extern int          esl_binplot_Score2Bin(ESL_BINPLOT *p, double x, int *ret_b);
extern int          esl_binplot_Add(ESL_BINPLOT *p, double x, double y);


/* Output/display of binned data:
 */
extern int esl_binplot_Print(FILE *fp, ESL_BINPLOT *p);
extern int esl_binplot_Plot(FILE *fp, ESL_BINPLOT *p);

#endif /*!ESL_BINPLOT_INCLUDED*/
/*****************************************************************
 * @LICENSE@
 *****************************************************************/
