#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>
#include <inttypes.h>

#include "fastcky_w.h"

// lhuang max seq length
#define MAXN 4500
//#define MAXM 222222  // ????
#define INF 1000000007

#define NOTON 5 // NUM_OF_TYPE_OF_NUCS
#define NOTOND 25
#define NOTONT 125

#define EXPLICIT_MAX_LEN 4
#define SINGLE_MIN_LEN 0
#define SINGLE_MAX_LEN 30

#define HAIRPIN_MIN_LEN 0
#define BULGE_MIN_LEN 1
#define INTERNAL_MIN_LEN 2
#define SYMMETRIC_MIN_LEN 1
#define ASYMMETRY_MIN_LEN 1

#define HAIRPIN_MAX_LEN 30
#define BULGE_MAX_LEN SINGLE_MAX_LEN
#define INTERNAL_MAX_LEN SINGLE_MAX_LEN
#define SYMMETRIC_MAX_LEN 15
#define ASYMMETRY_MAX_LEN 28


#define FFOR(x) for(int enumerating_index = 0; enumerating_index < x; enumerating_index ++)

#define GET_ACGU_NUM(x) ((x=='A'? 0 : (x=='C'? 1 : (x=='G'? 2 : (x=='U'?3: 4))))) //lhuang: N

/* #define GET_ACGU_NUM(x) ((x=='A'? 0 : (x=='C'? 1 : (x=='G'? 2 : 3)))) //lhuang: N */

//#define ALLOWED_PAIRS(x, y) (_allowed_pairs[GET_ACGU_NUM(x)*NOTON + GET_ACGU_NUM(y)])
#define ALLOWED_PAIRS(i, j) (_allowed_pairs[global_ints[i]][global_ints[j]])

#define HELIX_STACKING(i, j, k, l) (_helix_stacking[global_ints[i]][global_ints[j]][global_ints[k]][global_ints[l]])
#define HELIX_STACKING_OLD(x, y, z, w) (_helix_stacking[GET_ACGU_NUM(x)][GET_ACGU_NUM(y)][GET_ACGU_NUM(z)][GET_ACGU_NUM(w)])


#define SWAP(x,y) {int _tmp=x;x=y;y=_tmp;}

bool _allowed_pairs[NOTON][NOTON];
bool _helix_stacking[NOTON][NOTON][NOTON][NOTON];

// bool allowed_pairs_next[NOTON][MAXN];
int get_next_pair[NOTON][MAXN];

extern char global_bases[MAXN];
extern short global_ints[MAXN];
#ifdef REFS
extern char global_ref[MAXN];
#endif
extern int global_L;

extern bool allowedpairposition[MAXN][MAXN];
extern bool allowedunpairposition[MAXN][MAXN];

extern double cachesingle[SINGLE_MAX_LEN+1][SINGLE_MAX_LEN+1];

double getbasepair(int i, int j)
{
#ifdef DEBUG
  printf("entering getbasepair\n");
#endif
  if (ALLOWED_PAIRS(i,j)) // global_bases[i], global_bases[j]))
  {
    int ii = global_ints[i];
    int jj = global_ints[j];
    if (ii > jj) SWAP(ii,jj);
    return base_pair[ii*NOTON+jj];
  }
  else
    assert(false);
}

/* 5 constant features */
/* multi_base */
/* multi_unpaired */
/* multi_paired */
/* external_unpaired */
/* external_paired */

double gethelixstacking(int i, int j)
{
#ifdef DEBUG
  printf("entering gethelixstacking\n");
#endif
  short pair0, pair1, pair2, pair3;
  pair0 = global_ints[i];
  pair1 = global_ints[j];
  pair2 = global_ints[i+1];
  pair3 = global_ints[j-1];
  if (HELIX_STACKING(i, j, i+1, j-1)) //global_bases[i], global_bases[j], global_bases[i+1], global_bases[j-1]))
    ;
  else
    {
      SWAP(pair0, pair3);
      SWAP(pair1, pair2);
    }
  return helix_stacking[pair0*NOTONT + pair1*NOTOND + pair2*NOTON + pair3];
}

inline double gethelixclosing(int i, int j)
{
#ifdef DEBUG
  printf("entering gethelixclosing\n");
#endif
  int ii = global_ints[i];
  int jj = global_ints[j];
  return helix_closing[ii*NOTON+jj];
}

inline double getterminalmismatch(int i, int j)
{
#ifdef DEBUG
  printf("entering getterminalmismatch\n");
#endif
  return terminal_mismatch[global_ints[i]*NOTONT + global_ints[j]*NOTOND + global_ints[i+1]*NOTON + global_ints[j-1]];
}

/* 5 at least features */
/* (count directly) */
/* hairpin_length */
/* bulge_length */
/* internal_length */
/* internal_symmetric_length */
/* internal_asymmetry */

inline double getbulgenuc(int i)
{
#ifdef DEBUG
  printf("entering getbulgenuc\n");
#endif
  int tmp = global_ints[i];
  return bulge_0x1_nucleotides[tmp];
}

inline double getinternalnuc(int i, int j)
{
#ifdef DEBUG
  printf("entering getinternalnuc\n");
#endif
  int ii = global_ints[i];
  int jj = global_ints[j];
  if (ii > jj) SWAP(ii,jj);
  return internal_1x1_nucleotides[ii*NOTON+jj];
}

inline double getdangleleft(int i, int j)
{
#ifdef DEBUG
  printf("entering getdangleleft\n");
#endif
  return dangle_left[global_ints[i]*NOTOND + global_ints[j]*NOTON + global_ints[i+1]];
}

inline double getdangleright(int i, int j)
{
#ifdef DEBUG
  printf("entering getdangleright\n");
#endif
  return dangle_right[global_ints[i]*NOTOND + global_ints[j]*NOTON + global_ints[j-1]];
}


void initialize()
{
  _allowed_pairs[GET_ACGU_NUM('A')][GET_ACGU_NUM('U')] = true;
  _allowed_pairs[GET_ACGU_NUM('U')][GET_ACGU_NUM('A')] = true;
  _allowed_pairs[GET_ACGU_NUM('C')][GET_ACGU_NUM('G')] = true;
  _allowed_pairs[GET_ACGU_NUM('G')][GET_ACGU_NUM('C')] = true;
  _allowed_pairs[GET_ACGU_NUM('G')][GET_ACGU_NUM('U')] = true;
  _allowed_pairs[GET_ACGU_NUM('U')][GET_ACGU_NUM('G')] = true;

  HELIX_STACKING_OLD('A', 'U', 'A', 'U') = true;
  HELIX_STACKING_OLD('A', 'U', 'C', 'G') = true;
  HELIX_STACKING_OLD('A', 'U', 'G', 'C') = true;
  HELIX_STACKING_OLD('A', 'U', 'G', 'U') = true;
  HELIX_STACKING_OLD('A', 'U', 'U', 'A') = true;
  HELIX_STACKING_OLD('A', 'U', 'U', 'G') = true;
  HELIX_STACKING_OLD('C', 'G', 'A', 'U') = true;
  HELIX_STACKING_OLD('C', 'G', 'C', 'G') = true;
  HELIX_STACKING_OLD('C', 'G', 'G', 'C') = true;
  HELIX_STACKING_OLD('C', 'G', 'G', 'U') = true;
  HELIX_STACKING_OLD('C', 'G', 'U', 'G') = true;
  HELIX_STACKING_OLD('G', 'C', 'A', 'U') = true;
  HELIX_STACKING_OLD('G', 'C', 'C', 'G') = true;
  HELIX_STACKING_OLD('G', 'C', 'G', 'U') = true;
  HELIX_STACKING_OLD('G', 'C', 'U', 'G') = true;
  HELIX_STACKING_OLD('G', 'U', 'A', 'U') = true;
  HELIX_STACKING_OLD('G', 'U', 'G', 'U') = true;
  HELIX_STACKING_OLD('G', 'U', 'U', 'G') = true;
  HELIX_STACKING_OLD('U', 'A', 'A', 'U') = true;
  HELIX_STACKING_OLD('U', 'A', 'G', 'U') = true;
  HELIX_STACKING_OLD('U', 'G', 'G', 'U') = true;
}


void initializecachesingle()
{
  memset(cachesingle, 0, sizeof(cachesingle));
  for (int l1 = SINGLE_MIN_LEN; l1 <= SINGLE_MAX_LEN; l1 ++)
    for (int l2 = SINGLE_MIN_LEN; l2 <= SINGLE_MAX_LEN; l2 ++)
      {
        if (l1 == 0 && l2 == 0)
          continue;

        // bulge
        else if (l1 == 0)
            cachesingle[l1][l2] += bulge_length[l2];
        else if (l2 == 0)
            cachesingle[l1][l2] += bulge_length[l1];
        else
          {

            // internal
            cachesingle[l1][l2] += internal_length[l1+l2<=INTERNAL_MAX_LEN?l1+l2:INTERNAL_MAX_LEN];

            // internal explicit
            if (l1 <= EXPLICIT_MAX_LEN && l2 <= EXPLICIT_MAX_LEN)
              cachesingle[l1][l2] += internal_explicit[l1<=l2 ? l1*EXPLICIT_MAX_LEN+l2 : l2*EXPLICIT_MAX_LEN+l1];

            // internal symmetry
            if (l1 == l2)
                cachesingle[l1][l2] += internal_symmetric_length[l1<=SYMMETRIC_MAX_LEN?l1:SYMMETRIC_MAX_LEN];

            // internal asymmetry
            else
              {
                int diff = l1 - l2; if (diff < 0) diff = -diff;
                cachesingle[l1][l2] += internal_asymmetry[diff<=ASYMMETRY_MAX_LEN?diff:ASYMMETRY_MAX_LEN];
              }
          }
      }
  return;
}

// get_next_pair[i=0,1,2,3][j=position] = next position that letter(i) can pair
void initialize_next()
{
  // memset(allowed_pairs_next, 0, sizeof(allowed_pairs_next));
  memset(get_next_pair, 0, sizeof(get_next_pair));
  for (int i = 0; i < NOTON; ++i) {
    int tmp = -1;
    for (int j = global_L-1; j >= 0; --j) {
      get_next_pair[i][j] = tmp;
      if (_allowed_pairs[i][global_ints[j]]) {
        // allowed_pairs_next[i][j] = true;
        tmp = j;
      }
    }
  }
}

int getnextpair(int i, int j) // i, j are all positions
{
  return get_next_pair[global_ints[i]][j]; //return -1 if no allowed pairing on the right
}

double gethairpin(int i, int j)
{
    return hairpin_length[j-i-1<=HAIRPIN_MAX_LEN?j-i-1:HAIRPIN_MAX_LEN];
}

double getinternallength(int l)
{
    return internal_length[l<=INTERNAL_MAX_LEN?l:INTERNAL_MAX_LEN];
}


double getinternalexplicit(int l1, int l2){
    int l1_ = l1 > EXPLICIT_MAX_LEN ? EXPLICIT_MAX_LEN :l1;
    int l2_ = l2 > EXPLICIT_MAX_LEN ? EXPLICIT_MAX_LEN : l2;
    return internal_explicit[l1_<=l2_ ? l1_*EXPLICIT_MAX_LEN+l2_ : l2_*EXPLICIT_MAX_LEN+l1_];
}


double getinternalsym(int l)
{
    return internal_symmetric_length[l<=SYMMETRIC_MAX_LEN?l:SYMMETRIC_MAX_LEN];
}

double getinternalasym(int l1, int l2)
{
    int diff = l1 - l2; if (diff < 0) diff = -diff;
    return internal_asymmetry[diff<=ASYMMETRY_MAX_LEN?diff:ASYMMETRY_MAX_LEN];
}


double getbulgelength(int l){
    return bulge_length[l>BULGE_MAX_LEN?BULGE_MAX_LEN:l];
}

double gethairpinatleast(int l)
{
    return hairpin_length_at_least[l<=HAIRPIN_MAX_LEN?l:HAIRPIN_MAX_LEN];
}

double getbuldgelengthatleast(int l) {
    return bulge_length_at_least[l>BULGE_MAX_LEN?BULGE_MAX_LEN:l];
}

double getinternallengthatleast(int l)
{
    return internal_length_at_least[l<=INTERNAL_MAX_LEN?l:INTERNAL_MAX_LEN];
}

//--------------------------------


double scorejunctionA(int i, int j)
{
#ifdef DEBUG
    printf("entering scorejunctionA\n");
#endif
    double result = 0.0;
    result += gethelixclosing(i,j);
    if (i < global_L - 1)
        result += getdangleleft(i,j);
    if (j>0)
        result += getdangleright(i,j);
    return result;
}

double scorejunctionB(int i, int j)
{
#ifdef DEBUG
    printf("entering scorejunctionB\n");
#endif
    return gethelixclosing(i,j) + getterminalmismatch(i,j);
}

double scorehairpin(int i, int j)
{
#ifdef DEBUG
    printf("entering scorehairpin\n");
#endif
    return hairpin_length[j-i-1<=HAIRPIN_MAX_LEN?j-i-1:HAIRPIN_MAX_LEN]+scorejunctionB(i,j);
}


double scorehelix(int i, int j)
{
#ifdef DEBUG
  printf("entering scorehelix\n");
#endif
  return gethelixstacking(i,j)+getbasepair(i+1,j-1);
}

double scoresinglenuc(int i, int j, int p, int q)
{
#ifdef DEBUG
  printf("entering scoresinglenuc\n");
#endif
  int l1=p-i-1, l2=j-q-1;
  double result = .0;
  if (l1 == 0 && l2 == 1)
    result += getbulgenuc(q+1);
  else if (l1 == 1 && l2 == 0)
    result += getbulgenuc(p-1);
  else if (l1 == 1 && l2 == 1)
    result += getinternalnuc(p-1, q+1);
  return result;
}

double scoresingle(int i, int j, int p, int q)
{
#ifdef DEBUG
  printf("entering scoresingle\n");
#endif
  int l1=p-i-1, l2=j-q-1;
  double scorecache = cachesingle[l1][l2];
  return getbasepair(p, q) + scorecache + scorejunctionB(i,j)  \
    + scorejunctionB(q,p) + scoresinglenuc(i,j,p,q);
}

double scoremulti(int i, int j)
{
#ifdef DEBUG
  printf("entering scoremulti\n");
#endif
  return scorejunctionA(i,j) + multi_paired + multi_base;
}

double scoremultiunpaired(int i, int j)
{
#ifdef DEBUG
  printf("entering scoremultiunpaired\n");
#endif
  return (j-i+1) * multi_unpaired;
}

double scoreM1(int i, int j, int k)
{
#ifdef DEBUG
  printf("entering scoreM1\n");
#endif
  return scorejunctionA(k, i) + scoremultiunpaired(k+1, j) \
    + getbasepair(i, k) + multi_paired;
}

double scoreexternalpaired(int i, int j)
{
#ifdef DEBUG
  printf("entering scoreexternalpaired\n");
#endif
  return scorejunctionA(j, i) + external_paired + getbasepair(i, j);
}

double scoreexternalunpaired(int i, int j)
{
#ifdef DEBUG
  printf("entering scoreexternalunpaired\n");
#endif
  return (j-i+1) * external_unpaired;
}

#endif  // UTILS_H
