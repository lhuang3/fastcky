#!/usr/bin/env python

"""
Usage: f1_by_len.py seqs.txt ref.txt pred.txt
"""

import sys, itertools
from rna import RNA, FScore
from collections import defaultdict

def evaluate_by_bracket_length(golds, preds):
    f1s = defaultdict(FScore)
    for gold, pred in zip(golds, preds):
        assert len(gold.seq) == len(pred.seq)
        if gold.seq != pred.seq:
            newseq = gold.seq
            for i, x in enumerate(newseq):
                if x not in 'AUCGaucg':
                    newseq[i] = 'n'
            assert newseq == pred.seq, "\n%s\n%s\n%s" % (gold, "".join(newseq), pred) # same seq
            print >> logs, "invalid char in gold"
        for i, (g, p) in enumerate(zip(gold.pairs, pred.pairs)):
            if g != -1:
                length = i - g
                if length > 0:
                    f1s[length].goldcount += 1
            if p != -1:
                length = i - p
                if length > 0:
                    f1s[length].predcount += 1
                    if p == g:
                        f1s[length].correct += 1
    return f1s


def evaluate_by_sequence_length(golds, preds):
    f1s = defaultdict(FScore)
    for gold, pred in zip(golds, preds):
        assert len(gold.seq) == len(pred.seq)
        if gold.seq != pred.seq:
            newseq = gold.seq
            for i, x in enumerate(newseq):
                if x not in 'AUCGaucg':
                    newseq[i] = 'n'
            assert newseq == pred.seq, "\n%s\n%s\n%s" % (gold, "".join(newseq), pred) # same seq
            print >> logs, "invalid char in gold"
        length = len(gold.pairs)
        for i, (g, p) in enumerate(zip(gold.pairs, pred.pairs)):
            if g != -1:
                if i - g > 0:
                    f1s[length].goldcount += 1
            if p != -1:
                if i - p > 0:
                    f1s[length].predcount += 1
                    if p == g:
                        f1s[length].correct += 1
    return f1s



def main():
    GOLD = []
    TEST = []
    for seq, bracket1, bracket2 in itertools.izip(*map(open, sys.argv[1:])):
        rna1 = RNA.loadpp(seq, bracket1)
        rna2 = RNA.loadpp(seq, bracket2)
        GOLD.append(rna1)
        TEST.append(rna2)
    f1s = evaluate_by_bracket_length(GOLD, TEST)

    points = set([100, 200, 400, 800, 1600, 4000])
    #points = set([50, 100, 200, 300, 500, 2000, 4000])
    fscore = FScore()
    for i in xrange(max(points)+1):
        if i in f1s:
            fscore += f1s[i]
        if i in points:
            print "len %d num %d fscore %s" % (i, fscore.goldcount, fscore)
            fscore = FScore()


if __name__ == "__main__":
    main()



