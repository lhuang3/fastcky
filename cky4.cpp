//////////////////////////////////////////////////////////////////////
// cky4.cpp
//////////////////////////////////////////////////////////////////////

#define PRECISION 0.000001
#define MAXINT 100000000

#define EXPLICIT_MAX_LEN 4
#define SINGLE_MIN_LEN 0
#define SINGLE_MAX_LEN 30

#define HAIRPIN_MIN_LEN 0
#define BULGE_MIN_LEN 1
#define INTERNAL_MIN_LEN 2
#define SYMMETRIC_MIN_LEN 1
#define ASYMMETRY_MIN_LEN 1

#define HAIRPIN_MAX_LEN 30
#define BULGE_MAX_LEN 30 // = single_max_len
#define INTERNAL_MAX_LEN 30
#define SYMMETRIC_MAX_LEN 15
#define ASYMMETRY_MAX_LEN 28

