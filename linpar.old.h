//
// Linear-time RNA structure prediction
//
// Note that in C++11 all the functions returning non-POD objects will be optimized to avoid unnecessary
// object copy.
//
// Created by Kai Zhao on 11/8/16.
//

#ifndef FASTCKY_LINPAR_H
#define FASTCKY_LINPAR_H

#include <string>
#include <vector>
#include <tuple>

#include "utils.h"

enum Manner { MANNER_PUSH, MANNER_SKIP, MANNER_LINK, MANNER_REDUCE, MANNER_OTOPLINK, MANNER_INIT };
enum Shape {
    SHAPE_E = 0,  // -1 _ _ _ j
    SHAPE_P,  // i( _ _ _ )j
    SHAPE_M1,  // i ... ( _ _ _ ) ... j
    SHAPE_M2,  // i ... ( _ _ _ ) [ ... ( _ _ _ ) ...]+ ...j
    SHAPE_O,   // st=0: i ( ... j
               // st=1: i ...p(_ _ _)q ... j
    SHAPE_NONE };

std::string getShapeStr(Shape shape);

class State {
public:
    int i_, j_;  // current span
    int l_, l1_, l2_;
    double score_, heuristic_, inside_;
    enum Manner manner_;
    enum Shape shape_;
    int status_;  // for shape_O only
    const State *backptr1_;
    const State *backptr2_;
    mutable std::vector<const State*> leftptrs_;
    bool boundary_gap_l_, boundary_gap_r_;

    // A tuple of 9 integers
    typedef std::tuple<int, int, int, int, int, int, int, int, int> StateSignature;

    State(Shape shape, int i, int j, int l, int l1, int l2, int status);

    std::string pp() const;
    std::string pp_backptr() const;
    std::string id() const;

    StateSignature signature() const;

    std::string to_str() const;

    // actions
    State skip() const;
    State push() const;
    State link() const;
    std::vector<State> reduce() const;
    std::vector<State> OtoPlink() const;
    State O1toM1() const;

    // heuristic scores shared by all objects
    static double heuristic_at_least[SINGLE_MAX_LEN+1];
    static void init_heuristic_scores();

    static State get_init_state();
private:
    mutable StateSignature signature_;
};

#endif //FASTCKY_LINPAR_H
