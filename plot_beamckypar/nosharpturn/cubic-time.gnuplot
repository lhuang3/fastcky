#!/usr/bin/env gnuplot

#                   3              5                   8          10
#Viterbi score:  9.07475 Time: 13.99677 seconds  len: 323  all: 950948 (E: 15664, O: 746103, P: 189181)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=cubic-time.pdf' 

set xrange [100:]
#set logscale xy
#set yrange [-1:1e9]
#set key 285, 19.5e6
#set key spacing 2.6
#set key samplen 0 # important
set size 0.52
set xtics 1000
set size ratio 1.3
set xlabel "sequence length ({/Arial-Italic n})"
set ylabel "average time per sentence" offset 31,0
#set title "linear-time dynamic programming vs. cubic-time CKY"
#set label "linear {/Arial-Italic b}=2000" at 170, 1e6 rotate by 10
#set label "CKY {/Arial-Italic k}=50, \\~{/Arial-Italic n}^{2.7}" at 250, 3.e6 rotate by 68
#set label "exact CKY, \\~{/Arial-Italic n}^{3.1}" at 140, 4.4e6 rotate by 78

plot \
     "ckypar.noknots.b20.time.log"   u 4:2 w p ps .8 notitle,\
     "ckypar.noknots.b50.time.log"   u 4:2 w p ps .8 notitle,\
     "ckypar.noknots.b100.time.log"   u 4:2 w p ps .8 notitle,\
     "ckypar.noknots.b200.time.log"   u 4:2 w p ps .8 notitle,\
     "ckypar.noknots.b500.time.log"   u 4:2 w p ps .8 notitle,\
     "ckypar.noknots.time.log"        u 4:2 w p ps .8 notitle, \
     "vienna.noknots.time.log"        u 2:4 w p ps .8 notitle
     #"ckypar.b100.quickselect.time.log"   u 4:14 w p ps .8 notitle
     #"ckypar.time.log" u 4:6 w p ps 0.8 notitle, \
     #"ckypar.b100.quickselect.time.log"   u 4:8 w p ps .8 notitle#, \
     #"ckypar.b500.quickselect.time.log"   u 4:6 w p ps .8 notitle, \
     #"ckypar.b100.quickselect.time.log"   u 4:6 w p ps .8 notitle#, \
     #x*3400-150000 ls 5 notitle, \
     #x**2.69*exp(0.63)+100000 notitle, \
     #x**3.07*exp(-0.72) ls 5 notitle


set output '| epstopdf --filter --outfile=cubic-time-loglog.pdf' 
set logscale xy
unset xrange
unset xtics
replot
     
#: {/Arial-Italic n}^{2.7}", \
#: {/Arial-Italic n}^{3.1}" ,\
