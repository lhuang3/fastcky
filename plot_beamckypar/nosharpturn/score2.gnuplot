#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=score2.pdf' 

#set xrange [-10:2560000]
#set xrange [-10: 30000000]
#set logscale x
#set yrange [:38.05]
set key bottom right
set key spacing 2.5 
set key samplen 1.5
set size 0.5
set size ratio 0.8
set xtics 1e7
set xlabel "average \# of states generated per sequence"
set ylabel "average model score" offset 1.5,0
#set title "linear-time dynamic programming vs. cubic-time CKY"

#set offset 1,1,1,1

plot "beamcky.noknots.log" u 14:8 w lp ps 0.5 lw 4 t "linear DP w/ beam {/Arial-Italic b}", \
     "" u 14:8:(sprintf("{/Arial-Italic b}=%d", $6)) w labels offset char 2.3,-0.5 notitle , \
     "cky.noknots.log" u 12:6 w p ps 0.5 lw 2 lc 3 t "exact Zuker"
