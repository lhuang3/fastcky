#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=fscore.pdf' 

#set xrange [0:15]
#set logscale x
#set yrange [-1:6]
set key bottom right
set key spacing 2.5
set key samplen 1.5 # important
set size 0.5
set size ratio 0.8
set xtics 0.1

set xlabel "average time (seconds) per sequence"
set ylabel "average F-1 score" offset 1.5,0
#set title "linear-time dynamic programming vs. cubic-time CKY"

plot "beamcky.noknots.log" u 10:20 w lp ps 0.5 lw 4 t "linear DP w/ beam {/Arial-Italic b}", \
     "" u 10:20:(sprintf("{/Arial-Italic b}=%d", $6)) w labels offset char 2.5,0.5 notitle, \
     "cky.noknots.log" u 8:18 w p ps 0.5 lw 2 lc 3 t "exact Zuker", \
     55.77 t "Contrafold MEA", \
     52.42 t "Vienna"
     #"vienna.noknots.log" u 4:6 w p ps 0.5 lw 2 lc 4 t "Vienna", \