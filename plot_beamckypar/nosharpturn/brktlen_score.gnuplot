#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=brktlen.score.pdf' 

set key top right
set key spacing 2.5 
set key samplen 1.5
set size 0.5
set size ratio 0.8
set xlabel "base pair distance"
set ylabel "average F1" offset 1.5,0
set xtics 200
set yrange [10:60]

#set offset 1,1,1,1

plot "b100.brktlen.score.log" u 2:11 w lp ps 1 lw 4 t "linear, {/Arial-Italic b}=100", \
     70 notitle, \
     "cky.brktlen.score.log" u 2:11 w lp ps 0.7 lw 4 t "exact Zuker" 
