#!/usr/bin/env gnuplot

#          2              4         6               8                 10                  12           14        16        18
#num_seq: 140, avg_len: 115.4 beam: 0, avg_score: 5.9499, avg_time: 14.526, avg_states: 2503485.2  (P= 66.45, R= 58.41, F= 62.17)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=f1vstime.pdf' 

set key top right
set key spacing 2.5 
set key samplen 1.5
set size 0.5
set size ratio 0.8
set xlabel "average time per sequence"
set ylabel "accuracy (F1 score)" offset 1.5,0
set logscale x
set yrange [50.5:]
set xrange [0.005:2000]

set grid
show grid
#set offset 1,1,1,1

plot "f1vstime.log" u 1:($2-0.3):3  with labels  notitle, \
     "" u 1:2 w p ps 1 pt 4 lw 3 notitle 