#!/usr/bin/env gnuplot

#                   3              5                   8          10
#Viterbi score:  9.07475 Time: 13.99677 seconds  len: 323  all: 950948 (E: 15664, O: 746103, P: 189181)

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=cubic-time-big.pdf' 

set xrange [900:12000]
set yrange [0.6:3000]
#set logscale xy
#set yrange [-1:1e9]
#set key 285, 19.5e6
set key spacing 2.6
#set key samplen 0 # important
set key bottom right
set size 0.52
set xtics 10 #(1000,3000,10000)
set size ratio 1.3
set xlabel "random sequence length" # ({/Arial-Italic n})"
set ylabel "average time (secs) per sequence" offset 28,0
#set title "linear-time dynamic programming vs. cubic-time CKY"
set label "linear {/Arial-Italic b}=100, \\~{/Arial-Italic n}^{1.1}" at 1500, 0.8 rotate by 21
#set label "contrafold Viterbi: \\~{/Arial-Italic n}^{2.6}" at 3600, 6 rotate by 53
set label "Vienna: \\~{/Arial-Italic n}^{2.6}" at 4000, 10 rotate by 45
set label "contrafold MEA: \\~{/Arial-Italic n}^{2.9}" at 1500, 10 rotate by 48
set label "RNAStruct.: \\~{/Arial-Italic n}^{2.9}" at 1000, 45 rotate by 48
#set label "CKY {/Arial-Italic k}=50, \\~{/Arial-Italic n}^{2.7}" at 250, 3.e6 rotate by 68
#set label "exact CKY, \\~{/Arial-Italic n}^{3.1}" at 140, 4.4e6 rotate by 78

set logscale xy

set grid

show grid

# beam=100
#f1(x) =  x ** 1.159 / exp(8.25533)
f1(x) = x ** 1.09721 / exp(8.1189)
# contrafold viterbi
f2(x) = x ** 2.63455 / exp(19.0829)
# contrafold mea
f3(x) = x ** 2.91565 / exp(18.3815)
# Vienna
f4(x) = x ** 2.58617 / exp(18.4883)
# RNAStructure
f5(x) = x ** 2.89132 / exp(17.0299)

plot \
     "beam100.cubepruning.external.time.log"   u 6:8 w p ps .8 lc rgb "#CD853F" t "",\
     "contrafold.mea.external.time.log"   u 6:8 w p ps .8 lc rgb "#4169E1" t "",\
     "rnafold.external.time.log"   u 6:8 w p ps .8 lc rgb "#9400D3" t "",\
     "rnastructure.external.time.log"   u 6:8 w p ps .8 lc rgb "#FFD700" t "", \
     f1(x) lc rgb "#CD853F" notitle, \
     f3(x) lc rgb "#4169E1" notitle, \
     f4(x) lc rgb "#9400D3" notitle, \
     f5(x) lc rgb "#FFD700" notitle
     #"contrafold.viterbi.external.time.log"   u 6:8 w p ps .8 lc rgb "#2E8B57" t "",\
     #f2(x) lc rgb "#2E8B57" notitle, \
     #"ckypar.noknots.b500.time.log"   u 4:2 w p ps .8 notitle,\
     #"ckypar.noknots.time.log"        u 4:2 w p ps .8 notitle, \
     #"vienna.noknots.time.log"        u 2:4 w p ps .8 notitle
     #"ckypar.b100.quickselect.time.log"   u 4:14 w p ps .8 notitle
     #"ckypar.time.log" u 4:6 w p ps 0.8 notitle, \
     #"ckypar.b100.quickselect.time.log"   u 4:8 w p ps .8 notitle#, \
     #"ckypar.b500.quickselect.time.log"   u 4:6 w p ps .8 notitle, \
     #"ckypar.b100.quickselect.time.log"   u 4:6 w p ps .8 notitle#, \
     #x*3400-150000 ls 5 notitle, \
     #x**2.69*exp(0.63)+100000 notitle, \
     #x**3.07*exp(-0.72) ls 5 notitle


#set output '| epstopdf --filter --outfile=cubic-time-loglog.pdf' 
#set logscale xy
#unset xrange
#unset xtics
#replot
     
#: {/Arial-Italic n}^{2.7}", \
#: {/Arial-Italic n}^{3.1}" ,\
