fout = open("fastcky.h", "w")

print >> fout, "#ifndef FASTCKY"
print >> fout, "#define FASTCKY"

d = {}
for line in open("contrafold.params.complementary"):
    tmp = line.strip().split()
    d[tmp[0]] = float(tmp[1])

feature_constant = [
    "multi_base",
    "multi_unpaired",
    "multi_paired",
    "external_unpaired",
    "external_paired",
]

for feature in feature_constant:
    string = "#define "
    string += "%s %f" % (feature, d[feature])
    print >> fout, string

feature_unary = [
    "base_pair",
    "terminal_mismatch",
    "bulge_0x1_nucleotides",
    "internal_1x1_nucleotides",
    "helix_stacking",
    "helix_closing",
    "dangle_left",
    "dangle_right",
]

# for feature in feature_unary:
#     string = "#define %s(x) switch(x){" % feature
#     for item in d:
#         if item.startswith(feature):
#             argu = item.rsplit("_")[-1]
#             if not argu.isdigit():
#                 argu = "\""+argu+"\""
#             string += "case %s: %f;break;" % (argu, d[item])
#     string += "}"
#     print >> fout, string

# feature = "internal_explicit"
# string = "#define %s(x,y) switch(x){"
# for i in range(1,5):
#     string2 = "case %d: switch(y){" % i
#     for j in range(1,5):
#         string2+="case %d: "
#     string2 += "}"
#     string += string2+";break;"
# string += "}"

feature_binary = [
    "internal_explicit" ## bounds: 1-4: special treat
]

EXPLICIT_MAX_LEN = 4
SINGLE_MIN_LEN = 0
SINGLE_MAX_LEN = 30

HAIRPIN_MIN_LEN = 0
BULGE_MIN_LEN = 1
INTERNAL_MIN_LEN = 2
SYMMETRIC_MIN_LEN = 1
ASYMMETRY_MIN_LEN = 1

HAIRPIN_MAX_LEN = 30
BULGE_MAX_LEN = SINGLE_MAX_LEN
INTERNAL_MAX_LEN = SINGLE_MAX_LEN
SYMMETRIC_MAX_LEN = 15
ASYMMETRY_MAX_LEN = 28

features_atleast = [
    "hairpin_length",
    "bulge_length",
    "internal_length",
    "internal_symmetric_length",
    "internal_asymmetry"
]

bounds = [
    (HAIRPIN_MIN_LEN, HAIRPIN_MAX_LEN),
    (BULGE_MIN_LEN, BULGE_MAX_LEN),
    (INTERNAL_MIN_LEN, INTERNAL_MAX_LEN),
    (SYMMETRIC_MIN_LEN, SYMMETRIC_MAX_LEN),
    (ASYMMETRY_MIN_LEN, ASYMMETRY_MAX_LEN),
]

print >> fout, "#endif"
fout.close()
