// Created by Dezhong on Jan 2017.

#include "Jlinpar.h"

#include <algorithm>
#include <unordered_set>
#include <functional>
#include <iostream>
#include <sys/time.h>
#include <stack>

#include "fastcky_w.h"
#include "utility.h"

#define MAXN 4500

using namespace std;

#define BEAMSIZE INF
//#define DEBUG
//#define DEBUG_D

string getShapeStr(Shape shape) {
  switch (shape) {
  case SHAPE_E:
    return "E";
  case SHAPE_O:
    return "O";
  case SHAPE_P:
    return "P";
  case SHAPE_M:
    return "M";
  case SHAPE_M2:
    return "M2";
  default:
    assert(false);
    return "";
  }
}

string getMannerStr(Manner manner_) {
  switch (manner_) {
  case MANNER_PUSH:
    return "manner_push";
  case MANNER_JUMP:
    return "manner_jump";
  case MANNER_SKIP_E:
    return "manner_skip_E";
  case MANNER_REDUCE_E:
    return "manner_reduce_E";
  case MANNER_INIT:
    return "manner_init";
  case MANNER_SKIP_M:
    return "manner_skip_M";
  case MANNER_PTOM:
    return "manner_PtoM";
  case MANNER_REDUCE_M:
    return "manner_reduce_M";
  case MANNER_PAIR:
    return "manner_pair";
  case MANNER_EXPAND_FROM_M2:
    return "manner_expand_from_M2";
  case MANNER_EXPAND_FROM_P:
    return "manner_expand_from_P";
  case MANNER_REDUCE_M2:
    return "manner_reduce_M2";
  default:
    assert(false);
    return "";
  }
}


Shape getShapeFromManner(Manner manner_) {
  switch (manner_) {
  case MANNER_PUSH:
  case MANNER_JUMP:
    return SHAPE_O;
  case MANNER_SKIP_E:
  case MANNER_REDUCE_E:
  case MANNER_INIT:
    return SHAPE_E;
  case MANNER_SKIP_M:
  case MANNER_PTOM:
  case MANNER_REDUCE_M:
    return SHAPE_M;
  case MANNER_PAIR:
  case MANNER_EXPAND_FROM_M2:
  case MANNER_EXPAND_FROM_P:
    return SHAPE_P;
  case MANNER_REDUCE_M2:
    return SHAPE_M2;
  default:
    assert(false);
    return SHAPE_NONE;
  }
}

auto StateHash = [](const State& s) -> size_t {
  return std::hash<int>{}(s.i_);
};

auto StateEq = [](const State& s1, const State& s2) -> bool {
  return s1.i_ == s2.i_;
};


auto StateCmp = [](const State& a, const State& b) -> bool {
  return a.score_ > b.score_;
};

typedef std::unordered_set<State, decltype(StateHash), decltype(StateEq)> BeamStep;

State NullState(-2);

State::State(int i)
  :i_(i), score_(0.0) { }

State::State(int i, double score, Manner manner)
  :i_(i), score_(score), manner_(manner) { }

State::State(int i, double score, Manner manner, int split)
  :i_(i), score_(score), manner_(manner) {trace.split = split;}

State::State(int i, double score, Manner manner, char l1, char l2)
  :i_(i), score_(score), manner_(manner) {trace.paddings.l1 = l1; trace.paddings.l2 = l2;}

string State::id() const{
  char buf[64];
  sprintf(buf, "%p", this);
  string ret = string(buf);
  return ret.substr(ret.length()-5);
}

string State::pp_backptr(int j) const {
  string ret = "todo:pp_backptr";
  return ret;
  // TODO
}

string State::to_str(int j, double prescore) const {
  Shape shape = getShapeFromManner(manner_);
  string shapestr = getShapeStr(shape);
  char buf[1024];

  sprintf(buf, "%s %s(%d, %d), %s, sc=%.2f, in=%.2f", id().c_str(), shapestr.c_str(), i_, j, getMannerStr(manner_).c_str(), score_, score_-prescore);
  if (manner_ == MANNER_REDUCE_E || manner_ == MANNER_REDUCE_M || manner_ == MANNER_REDUCE_M2)
    sprintf(buf, "%s split=%d", buf, trace.split);
  else if (manner_ == MANNER_EXPAND_FROM_P || manner_ == MANNER_EXPAND_FROM_M2)
    sprintf(buf, "%s l1=%d l2=%d", buf, static_cast<int>(trace.paddings.l1), static_cast<int>(trace.paddings.l2));

  string ret(buf);
  if (shape == SHAPE_P) {
    replace(ret.begin(), ret.end(), '(', '[');
    replace(ret.begin(), ret.end(), ')', ']');
  };
  return ret;
}

int State::signature() const {
  return i_;
}

string pp(const State& inputstate,
          int inputj,
          vector<State>& beamE,
          vector<BeamStep>& beamO,
          vector<BeamStep>& beamM,
          vector<BeamStep>& beamM2,
          vector<BeamStep>& already_beam) {
  if (inputstate.i_ == -1 && inputj == 0) return "";
  int st = inputstate.i_; // string start point
  string result;
  
  Shape shape = getShapeFromManner(inputstate.manner_);
  if (shape == SHAPE_P)
    result = string(inputj-st+1, '.');
  else if (shape == SHAPE_E) {
    ++st;
    result = string(inputj-st, '.');
  }
  else
    result = string(inputj-st, '.');

  stack<tuple<State, int> > stk;
  stk.push(make_tuple(inputstate, inputj));

  while ( !stk.empty()) {
    tuple<State, int> top = stk.top();
    State& state = get<0>(top);
    int j = get<1>(top);
    stk.pop();

#ifdef DEBUG_D
    printf("Inpp: %s \n", state.to_str(j, state.i_<0?0.0:beamE[state.i_].score_).c_str());
#endif

    int i = state.i_;

    switch (state.manner_) {
    case MANNER_PUSH:
    case MANNER_JUMP:
      result[i-st] = '(';
      break;

    case MANNER_SKIP_E:
      stk.push(make_tuple(beamE[j-1], j-1));
      break;

    case MANNER_SKIP_M:
      {
#ifdef DEBUG
        assert(beamM[j-1].find(State(i)) != beamM[j-1].end());
#endif
        const State& newstate = *(beamM[j-1].find(State(i)));
        stk.push(make_tuple(newstate, j-1));
        break;
      }
    case MANNER_PAIR:
      result[i-st] = '(';
      result[j-st] = ')';
      break;

    case MANNER_PTOM:
      {
#ifdef DEBUG
        assert(already_beam[j-1].find(State(i)) != already_beam[j-1].end());
#endif
        const State& newstate = *(already_beam[j-1].find(State(i)));
        stk.push(make_tuple(newstate, j-1));
        break;
      }

    case MANNER_EXPAND_FROM_P:
      {
        int intl1 = static_cast<int>(state.trace.paddings.l1);
        int intl2 = static_cast<int>(state.trace.paddings.l2);
        int p = i+intl1+1, q = j-intl2-1;
#ifdef DEBUG
        // if (already_beam[q].find(State(p)) == already_beam[q].end()) {
        //   printf("my manner: %s\n", getMannerStr(state.manner_).c_str());
        //   printf("original: %d %d; l1=%d l2=%d\n", i, j, intl1, intl2);
        //   printf("failling: %d %d\n", p, q);
        // }
        assert(already_beam[q].find(State(p)) != already_beam[q].end());
#endif
        const State& newstate = *(already_beam[q].find(State(p)));
        stk.push(make_tuple(newstate, q));
        result[j-st] = ')';
        result[i-st] = '(';
        break;
      }

    case MANNER_EXPAND_FROM_M2:
      {
        int intl1 = static_cast<int>(state.trace.paddings.l1);
        int intl2 = static_cast<int>(state.trace.paddings.l2);
        int p = i+intl1+1, q = j-intl2; // one less
#ifdef DEBUG
        assert(beamM2[q].find(State(p)) != beamM2[q].end());
#endif
        const State& newstate = *(beamM2[q].find(State(p)));
        stk.push(make_tuple(newstate, q));
        result[j-st] = ')';
        result[i-st] = '(';
        break;
      }

    case MANNER_REDUCE_E:
      {
        int k = state.trace.split;
        State& prestate = beamE[k];
#ifdef DEBUG
        assert(already_beam[j-1].find(State(k)) != already_beam[j-1].end());
#endif
        const State& newstate = *(already_beam[j-1].find(State(k)));

        stk.push(make_tuple(prestate, k));
        stk.push(make_tuple(newstate, j-1));
        break;
      }

    case MANNER_REDUCE_M:
    case MANNER_REDUCE_M2:
      {
        int k = state.trace.split;
#ifdef DEBUG
        assert(beamM[k].find(State(i)) != beamM[k].end());
#endif
        const State& prestate = *(beamM[k].find(State(i)));

#ifdef DEBUG
        assert(already_beam[j-1].find(State(k)) != already_beam[j-1].end());
#endif
        const State& newstate = *(already_beam[j-1].find(State(k)));

        stk.push(make_tuple(prestate, k));
        stk.push(make_tuple(newstate, j-1));
        break;
      }

    case MANNER_INIT:
      break;
    default:
      break;

    }
  }

  return result;
}

struct DecoderResult {
  char structure[MAXN];
  double score;
  int numofstates;
  double time;
};

// parsing starts here
DecoderResult solve(char*seq, unsigned beamsize) {

  int numofstates[SHAPE_NONE+1] = {0};

  struct timeval starttime, endtime;
  gettimeofday(&starttime, NULL);

  const int seq_L = int(strlen(seq));

  char global_bases[seq_L];
  strcpy(global_bases, seq);

  short nucs[seq_L];
  for (int i = 0; i < seq_L; ++i)
    nucs[i] = GET_ACGU_NUM(global_bases[i]);

  int get_next_pair[NOTON][seq_L]; // usage: get_next_pair[nucs[i]][j]
  for (int nuci = 0; nuci < NOTON; ++nuci) {
    int tmp = -1;
    for (int j = seq_L-1; j >= 0; --j) {
      get_next_pair[nuci][j] = tmp;
      if (_allowed_pairs[nuci][nucs[j]])
        tmp = j;
    }
  }

  vector<State> beamE(seq_L+1, NullState);
  vector<BeamStep> beamO(seq_L+1, BeamStep({}, StateHash, StateEq));
  vector<BeamStep> beamM(seq_L+1, BeamStep({}, StateHash, StateEq));
  vector<BeamStep> beamM2(seq_L+1, BeamStep({}, StateHash, StateEq));
  vector<BeamStep> already_beam(seq_L, BeamStep({}, StateHash, StateEq)); // beamP
  beamE[0] = State(-1, 0, MANNER_INIT);;

  vector< vector<State *> > M_pointer(seq_L+1, vector<State *>());

#ifdef DEBUG
  printf("-1\n%s\n", beamE[0].to_str(0, 0.0).c_str());
#endif  // DEBUG

  vector< vector<State> > realcands(seq_L+1, vector<State>());
  vector< vector<State> > realpairs(seq_L, vector<State>());

  for (int i=0; i<seq_L; ++i) {
    State& candE = beamE[i+1];
    vector<State>& cands = realcands[i+1];
    // all states in pairs are temporary: they will be copied and kept in already_beam
    // when necessary
    vector<State>& pairs = realpairs[i];

    int newj = i;
    // search starts here

    // shapeE: push and skip
    {
      auto& state = beamE[i];
      int j_next = get_next_pair[nucs[newj]][newj];
      if (j_next != -1) {
        double sc = score_hairpin_length(j_next-newj-1);
        auto newstate = State(newj, state.score_ + sc, MANNER_PUSH);
        ++ numofstates[SHAPE_O];
        realcands[j_next].push_back(newstate);
      }

      auto newstate = State(state.i_, state.score_+external_unpaired, MANNER_SKIP_E);
      ++ numofstates[SHAPE_E];
      if (candE.i_ == -2 || candE.score_ < newstate.score_) {
        candE = newstate;
      }
    }

    // shapeO: jump and pair
    for (auto& state : beamO[i]) {
      int newi = state.i_;
      int j_next = get_next_pair[nucs[newi]][newj];
      if (j_next != -1) {
        int hairpin_len = j_next-newi-1;
        double sc = score_hairpin_length(hairpin_len) - score_hairpin_length(newj-newi-1);
        auto newstate = State(newi, state.score_ + sc, MANNER_JUMP);
        ++ numofstates[SHAPE_O];
        realcands[j_next].push_back(newstate);
      }

      if (_allowed_pairs[nucs[newi]][nucs[newj]]) {
        // hairpin_length already calculated
        double sc = score_junction_B(newi, newj, nucs[newi], nucs[newi+1], nucs[newj-1], nucs[newj]);
        auto newstate = State(newi, state.score_ + sc, MANNER_PAIR);
        ++ numofstates[SHAPE_P];
        pairs.push_back(newstate);
      }
    }

    // shapeM: skip
    for (auto& state : beamM[i]) {
      int newi = state.i_;
      auto newstate = State(newi, state.score_ + multi_unpaired, MANNER_SKIP_M);
      ++ numofstates[SHAPE_M];
      cands.push_back(newstate);
    }

    // shapeM2: expand
    for (auto& state : beamM2[i]) {
      int newi = state.i_;
      for (int k = ((newi-31>=0)?newi-31:0); k<newi; ++k) {
        int l = newj-1, l1 = newi-k-1;
        while (true) {
          l = get_next_pair[nucs[k]][l];
          if (l == -1 || l1+l-newj > 30) break;
          int l2 = l-newj; // one more
          double sc = score_multi(k, l, nucs[k], nucs[k+1], nucs[l-1], nucs[l], seq_L) \
            + (l1 + l2) * multi_unpaired;

          // newscore = E(-1,k).score + state.inside + sc,
          // which is state.score - E(-1, newi).score + E(-1, k).score + sc
          auto newstate = State(k, state.score_ - beamE[newi].score_ + beamE[k].score_ + sc, MANNER_EXPAND_FROM_M2, \
                                static_cast<char>(l1), static_cast<char>(l2));
          ++ numofstates[SHAPE_P];
          realpairs[l].push_back(newstate);
        }
      }
    }

    // sort and resize P
    sort(pairs.begin(), pairs.end(), StateCmp);

    auto& already = already_beam[i];
    for (auto& state: pairs) {
      if (already.find(state) == already.end()) { // throw away equivalent but worse items
        already.insert(state);
        auto& already_state = *already.find(state);
        // shapeP: PtoM, reduce, expand

        int newi = already_state.i_;
        auto prestate_E = beamE[newi];

        {
          double sc = score_M1(newi, newj, newj, nucs[newi-1], nucs[newi], nucs[newj], nucs[newj+1], seq_L);
          auto newstate = State(newi, already_state.score_ + sc, MANNER_PTOM);
          ++ numofstates[SHAPE_M];
          cands.push_back(newstate);
        }

        {
          double sc = score_external_paired(newi, newj, nucs[newi-1], nucs[newi], nucs[newj], nucs[newj+1], seq_L);
          // newscore = preE.score + P.inside + sc, which is P.score + sc
          auto newstate = State(-1, already_state.score_ + sc, MANNER_REDUCE_E, newi);
          ++ numofstates[SHAPE_E];
          if (candE.i_ == -2 || candE.score_ < newstate.score_) {
            candE = newstate;
          }
        }

        for (auto& prestate_M : beamM[newi]) {
          double sc = score_M1(newi, newj, newj, nucs[newi-1], nucs[newi], nucs[newj], nucs[newj+1], seq_L);
          // newscore = preM.score + P.inside + sc, which is preM.score + P.score - preE.score + sc
          double newscore = prestate_M.score_ + already_state.score_ - prestate_E.score_ + sc;
          auto newstate = State(prestate_M.i_, newscore, MANNER_REDUCE_M, newi);
          auto newstate_M2 = State(prestate_M.i_, newscore, MANNER_REDUCE_M2, newi);
          ++ numofstates[SHAPE_M];
          ++ numofstates[SHAPE_M2];
          cands.push_back(newstate);
          cands.push_back(newstate_M2);
        }


        if (newi != 0 && newj != seq_L-1) {
          double precalculate_junctionB = score_junction_B(newj, newi, nucs[newj], nucs[newj+1], nucs[newi-1], nucs[newi]);
          for (int k = ((newi-31>=0)?newi-31:0); k<newi; ++k) {
            int l = newj, l1 = newi-k-1;
            while (true) {
              l = get_next_pair[nucs[k]][l];
              if (l == -1 || l1+l-newj-1 > 30) break;
              int l2 = l-newj-1;

              double sc;
              if (l1 == 0 && l2 == 0)
                sc = score_helix(nucs[k], nucs[k+1], nucs[l-1], nucs[l]);
              else
                sc = precalculate_junctionB +
                  score_single_without_junctionB(k, l, newi, newj, nucs[newi-1], nucs[newi], nucs[newj], nucs[newj+1]) +
                  score_junction_B(k, l, nucs[k], nucs[k+1], nucs[l-1], nucs[l]);

              auto newstate = State(k, state.score_ - beamE[newi].score_ + beamE[k].score_ + sc, MANNER_EXPAND_FROM_P, static_cast<char>(l1), static_cast<char>(l2));
              ++ numofstates[SHAPE_P];
              realpairs[l].push_back(newstate);
            }
          }
        }

      }

    }

    sort(cands.begin(), cands.end(), StateCmp);

    if(cands.size() > beamsize) cands.resize(beamsize, NullState);

    for (auto& state : cands) {
      BeamStep& newstep = (state.manner_ == MANNER_PUSH ||
                           state.manner_ == MANNER_JUMP)
        ?beamO[i+1]: ((state.manner_ == MANNER_REDUCE_M2)
                      ?beamM2[i+1]:beamM[i+1]);
      auto dup = newstep.find(state);
      if (dup == newstep.end()) {
        newstep.insert(state);
      }
    }

#ifdef DEBUG
    printf("%d -----  ****   ==================== ****-------\n", i);
    vector<const State*> tosort;
    tosort.push_back(&beamE[i+1]);
    for (auto& state : beamO[i+1]) tosort.push_back(&state);
    for (auto& state : beamM[i+1]) tosort.push_back(&state);
    for (auto& state : beamM2[i+1]) tosort.push_back(&state);
    sort(tosort.begin(), tosort.end(),
         [](const State* s1, const State* s2) -> bool {
           return StateCmp(*s1, *s2);
         });
    int c = 0;
    for (auto& s : tosort) {
      printf("%d %d : %s ", i, c++, s->to_str(i+1, s->i_<0?0.0:beamE[s->i_].score_).c_str());
      const State& newstate = *s;
      printf("%s", pp(newstate, i+1, beamE,beamO,beamM,beamM2,already_beam).c_str());
      printf("\n");
    }
    printf("---\n");
    tosort.clear();
    for (auto& state : already) tosort.push_back(&state);
    sort(tosort.begin(), tosort.end(),
         [](const State* s1, const State* s2) -> bool {
           return StateCmp(*s1, *s2);
         });
    for (auto& s : tosort) {
      printf("%s ", s->to_str(i, s->i_<0?0.0:beamE[s->i_].score_).c_str());
      const State& newstate = *s;
      printf("%s", pp(newstate, i, beamE,beamO,beamM,beamM2,already_beam).c_str());
      printf("\n");
    }
#endif  // DEBUG

  }

  State best = beamE[seq_L];
  double best_score = best.score_;

  gettimeofday(&endtime, NULL);
  double elapsed_time = endtime.tv_sec - starttime.tv_sec
    + (endtime.tv_usec-starttime.tv_usec)/1000000.0;

  DecoderResult result;
  memset(result.structure, 0, MAXN);
  result.score = best_score;
  result.time = elapsed_time;
  result.numofstates = 0;

  if (best_score != -INF) {
    //TODO print structure
    //string structure = best.pp();
    string structure = pp(best, seq_L, beamE,beamO,beamM,beamM2,already_beam);
    strcpy(result.structure, structure.c_str());

    printf("Viterbi score: %8.5f ", best.score_);

    for(int i=0; i<SHAPE_NONE; ++i){
      result.numofstates += numofstates[i];
    }

    printf("Time: %.5f seconds  len: %d  all: %d (", result.time, seq_L, result.numofstates);
    for(int i=0; i<SHAPE_NONE; ++i){
      printf("%s: %d%s", getShapeStr(Shape(i)).c_str(), numofstates[i], (i==SHAPE_NONE-1)?"":" ");
    }
    printf(")\n");
    //printf("(%s)\n", result.structure);
    //printf("%s\n", global_bases);
    printf(">structure\n%s\n\n", result.structure);
  } else {
    printf("FAILED\n");
    printf("Viterbi score: -10000\n");
  }

  return result;
}

int main(int argc, char** argv){
  assert(argc >= 2);

  initialize();
  initialize_cachesingle();

  char* seq_file_name = argv[1];
  FILE* f_seq = fopen(seq_file_name, "r");

  int beamsize = BEAMSIZE;
  if (argc>=3) beamsize = stoi(argv[2]);
  printf("Beam Size %d\n", beamsize);

  // variables for decoding
  char seq[MAXN];
  int num=0, total_len = 0, total_states = 0;
  double total_score = .0;
  double total_time = .0;

  // go through the seq file to decode each seq
  while (fscanf(f_seq, "%s", seq) != EOF) {
    printf("seq:\n%s\n", seq); // passed
    DecoderResult result = solve(seq, beamsize);

    ++num;
    total_len += strlen(seq);
    total_score += result.score;
    total_states += result.numofstates;
    total_time += result.time;
  }

  fclose(f_seq);

  double dnum = (double)num;
  printf("num_seq: %d; avg_len: %.1f ", num, total_len/dnum);
  printf("beam: %d; avg_score: %.4f; avg_time: %.3f; tot_time: %f; avg_states: %.1f\n",
         BEAMSIZE, total_score/dnum, total_time/dnum, total_time, total_states/dnum);
  return 0;
}

