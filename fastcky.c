#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <assert.h>
#include <stdbool.h>
#include <inttypes.h>

////////////////////////////////////////
// switches                           //
////////////////////////////////////////

//#define DEBUG
#define IF_FEATS
//#define IF_VALUES
//#define IF_SEQUENCE
//#define IF_UNIQ
#define BEAMSIZE INF
#define MULTI

/* #define REFS */
/* #define FORCE_DECODE */

////////////////////////////////////////
////////////////////////////////////////

#include "fastcky_w.h"
#include "utils.h"

////////////////////////////////////////
// structs and global variables       //
////////////////////////////////////////

struct State {
  bool ifexist;
  int i,j;
  double score;
  char* manner;
  struct State* parent1;
  struct State* parent2;
  // omit fv recording
  int l1,l2; // add pair of index recording
};

#define DETAILEDCOUNT(x) {if(x == MANNER_HAIRPIN) ++dnum[0];else if (x == MANNER_SINGLE) ++dnum[1];else if (x == MANNER_HELIX) ++dnum[2];else if (x == MANNER_MULTI) ++dnum[3];else if (x == MANNER_M1$P || x == MANNER_M1$M1_U) ++dnum[4];else if (x == MANNER_C$C_U) ++dnum[5];else if (x == MANNER_M$M1_M1) ++dnum[6];else if (x == MANNER_M$M_M1) ++dnum[7];else if (x == MANNER_C$C_P) ++dnum[8];else if (x == MANNER_M$U_M) ++dnum[9];else if (x == MANNER_C$U) ++dnum[10];else if (x == MANNER_C$P) ++dnum[11];}
#define DETAILEDPRINT {printf("Updates bestP:%d bestM1:%d bestM:%d bestC:%d\n", dnum[0]+dnum[1]+dnum[2]+dnum[3], dnum[4], dnum[6]+dnum[7]+dnum[9], dnum[5]+dnum[8]+dnum[10]+dnum[11]);}
#define SETSTATE(s, _ifexist, _i, _j, _score, _manner, _parent1, _parent2, _l1, _l2) {s.ifexist=_ifexist;s.i=_i;s.j=_j;s.score=_score;s.manner=_manner;s.parent1=_parent1;s.parent2=_parent2;s.l1=_l1;s.l2=_l2;++numofstates;DETAILEDCOUNT(_manner)}

static char MANNER_HAIRPIN[] = "hairpin";
static char MANNER_SINGLE[] = "single";
static char MANNER_HELIX[] = "helix";
static char MANNER_MULTI[] = "multi";
static char MANNER_M1$P[] = "M1=P";
static char MANNER_M1$M1_U[] = "M1=M1+U";
static char MANNER_C$C_U[] = "C=C+U";
static char MANNER_M$M1_M1[] = "M=M1+M1";
static char MANNER_M$M_M1[] = "M=M+M1";
static char MANNER_C$C_P[] = "C=C+P";
static char MANNER_M$U_M[] = "M=U+M";
static char MANNER_C$U[] = "C=U";
static char MANNER_C$P[] = "C=P";

short global_ints[MAXN];
char global_bases[MAXN];
#ifdef REFS
char global_ref[MAXN];
#endif
int global_L;

bool allowedpairposition[MAXN][MAXN];
bool allowedunpairposition[MAXN][MAXN];
struct State bestP[MAXN][MAXN], bestM1[MAXN][MAXN], bestM[MAXN][MAXN], bestC[MAXN][MAXN];

struct EvalResult {
  int intersect, predicted, gold;
};

struct DecoderResult {
  char structure[MAXN];
  double score;
  int numofstates;
  double time;
};


////////////////////////////////////////
////////////////////////////////////////

void state_pp(struct State* state_ptr, char* result) // write to global result, never reallocate!
{
  
  if (!state_ptr)
    {
      return;
    }  
  int i = state_ptr->i, j = state_ptr->j;

  //printf("state %d, %d, %s\n", i, j, state_ptr->manner);
  if (i > j)
    {
      return;
    }
  if (i == j)
    {
      result[i] = '.';
      return;
    }

  char* manner = state_ptr->manner;

  // printf("%d %d %s\n", i, j, manner);
  
  if (manner == MANNER_HAIRPIN)
    {
      //result[0] = '(';
      result[i] = '(';
      int l = 1;
      FFOR(j-i-1)
        result[i + l++] = '.';
      result[i + l++] = ')';
      //result[i + l] = 0;
    }

  else if (manner == MANNER_SINGLE)
    {
      //char s1[MAXN];
      result[i+0] = '(';
      int l = 1;
      FFOR(state_ptr->l1)
        result[i + l++] = '.';
      //result[l] = 0;
      state_pp(state_ptr->parent1, result);
      //strcat(result, result);
      l = state_ptr->parent1->j+1; //strlen(result);
      FFOR(state_ptr->l2)
        result[l++] = '.';
      result[l++] = ')';
      //result[l] = 0;
    }

  else if (manner == MANNER_HELIX || manner == MANNER_MULTI)
    {
      //char s1[MAXN];
      result[i+0] = '('; //result[i+1] = 0;
      state_pp(state_ptr->parent1, result);
      //strcat(result, result);
      int l = state_ptr->parent1->j+1; //strlen(result);
      result[l++] = ')';
      //result[l] = 0;
    }

  else if (manner == MANNER_C$C_U)
    {
      //char s1[MAXN];
      state_pp(state_ptr->parent1, result);
      //strcpy(result, result);
      int l = state_ptr->parent1->j+1; //strlen(result);
      FFOR(state_ptr->l2 - state_ptr->l1 + 1)
        result[l++] = '.';
      //result[l] = 0;
    }

  else if (manner == MANNER_M1$M1_U)
    {
      //char s1[MAXN];
      state_pp(state_ptr->parent1, result);
      //strcpy(result, result);
      int l = state_ptr->parent1->j+1; //strlen(result);
      result[l++] = '.';
      //result[l] = 0;
    }

  else if (manner == MANNER_M$M1_M1 || manner == MANNER_M$M_M1 || manner == MANNER_C$C_P)
    {
      //char s1[MAXN], result[MAXN];
      state_pp(state_ptr->parent1, result);
      //strcpy(result, result);
      state_pp(state_ptr->parent2, result);
      //strcat(result, result);
    }

  else if (manner == MANNER_M$U_M)
    {
      int l = i;
      FFOR(state_ptr->l2 - state_ptr->l1 + 1)
        result[l++] = '.';
      //result[l] = 0;
      //char s2[MAXN];
      state_pp(state_ptr->parent2, result);
      //strcat(result, result);
    }

  else if (manner == MANNER_C$U)
    {
      int l = i;
      FFOR(j-i+1)
        result[l++] = '.';
      //result[l] = 0;
    }

  else if (manner == MANNER_C$P || manner == MANNER_M1$P)
    state_pp(state_ptr->parent1, result);

  //printf("state %d, %d, %s, %s\n", i, j, state_ptr->manner, result);

  return;
}


// global variable here
double cachesingle[SINGLE_MAX_LEN+1][SINGLE_MAX_LEN+1];

/* // NOTE: eval function ignored. eval result */
/* struct EvalResult eval(char* structure, char* ref) */
/* { */
/* } */

struct DecoderResult solve(char* seq, char* ref)
{
  struct timeval starttime, endtime;
  strcpy(global_bases, seq);
#ifdef REFS
  strcpy(global_ref, ref);
#endif
  global_L = strlen(seq);
  memset(allowedpairposition, false, MAXN*MAXN*sizeof(bool));
  memset(allowedunpairposition, true, MAXN*MAXN*sizeof(bool));
  memset(bestP, 0, MAXN*MAXN*sizeof(struct State));
  memset(bestM1, 0, MAXN*MAXN*sizeof(struct State));
  memset(bestM, 0, MAXN*MAXN*sizeof(struct State));
  memset(bestC, 0, MAXN*MAXN*sizeof(struct State));
    unsigned long truenumofstates = 0;
  int numofstates = 0;
  int dnum[12];
  memset(dnum, 0, sizeof(dnum));

  gettimeofday(&starttime, NULL); // lhuang: after memset

  for (int i = 0; i < global_L; i++)
    global_ints[i] = GET_ACGU_NUM(global_bases[i]);



#ifdef FORCE_DECODE
  int stack[MAXN];
  int tail=0;
  for (int i = 0; i < global_L; i ++)
    {
      char c = ref[i];
      if (c == '(')
        {
          stack[tail] = i;
          tail ++;
        }
      else if (c == ')')
        {
          assert(tail>0);
          int left_index = stack[tail-1];
          tail --;
          allowedpairposition[left_index][i] = true;
        }
    }

  for (int i = 0; i < global_L; i ++)
    for (int j = i; j < global_L; j ++)
      {
        bool flag = true;
        for (int k = i; k <= j; k ++)
          if (!(ref[k] == '.'))
            {
              flag = false;
              break;
            }
        if (flag)
          allowedunpairposition[i][j] = true;
        else
          allowedunpairposition[i][j] = false;
      }
#endif

#ifndef FORCE_DECODE
  for (int i = 0; i < global_L; i ++)
    for (int j = i+1; j < global_L; j ++)
      {
        if (ALLOWED_PAIRS(i, j)) //seq[i], seq[j]))
          allowedpairposition[i][j] = true;
      }
#endif

  for (int length = 2; length <= global_L; length ++)
    for (int i = 0; i <= global_L-length; i ++)
      {
        int j = i+length-1;

#ifdef DEBUG
  printf("i, j: %d, %d, %s\n", i, j, allowedpairposition[i][j]?"true":"false");
#endif
        // update bestP
#ifdef DEBUG
  printf("updating bestP\n");
#endif
        if (allowedpairposition[i][j])
          {
            // hairpin
            if (j-i == 1 || allowedunpairposition[i+1][j-1])
              {
                double newscore = scorehairpin(i, j);
                  ++truenumofstates;
                if (!bestP[i][j].ifexist || bestP[i][j].score < newscore)
                  SETSTATE(bestP[i][j], true, i, j, newscore, MANNER_HAIRPIN, NULL, NULL, 0, 0)
              }

            // helix and single-branched loops
            // constraints: single length <= SINGLE_MAX_LEN
            int tmp1 = j; if (tmp1 > i+1+SINGLE_MAX_LEN+1) tmp1 = i+1+SINGLE_MAX_LEN+1;
            for (int p = i+1; p < tmp1; p ++)
              {
                int tmp2 = p+1; if (tmp2 < j-1-SINGLE_MAX_LEN+(p-i-1)) tmp2 = j-1-SINGLE_MAX_LEN+(p-i-1);
                for (int q = tmp2; q < j; q ++)
                  if (allowedpairposition[p][q])
                    {
                      if (p == i+1 && q == j-1)
                        {
                          if (bestP[p][q].ifexist)
                            {
                              struct State* ostate_ptr = & bestP[p][q];
                              double newscore = scorehelix(i,j) + ostate_ptr->score;
                                ++truenumofstates;
                              if (!bestP[i][j].ifexist || bestP[i][j].score < newscore)
                                SETSTATE(bestP[i][j], true, i, j, newscore, MANNER_HELIX, ostate_ptr, NULL, 0, 0)
                                  }
                        }
                      else if (bestP[p][q].ifexist && allowedunpairposition[i+1][p-1] && allowedunpairposition[q+1][j-1])
                        {
                          struct State* ostate_ptr = & bestP[p][q];
                          double newscore = scoresingle(i,j,p,q) + ostate_ptr->score;
                            ++truenumofstates;
                          if (!(bestP[i][j].ifexist) || bestP[i][j].score < newscore)
                            SETSTATE(bestP[i][j], true, i, j, newscore, MANNER_SINGLE, ostate_ptr, NULL, p-i-1, j-q-1)
                              }
                    }
              }

            // multi
            if (j-i-1>= 4 && bestM[i+1][j-1].ifexist)
              {
                struct State* ostate_ptr = & bestM[i+1][j-1];
                double newscore = scoremulti(i,j) + ostate_ptr->score;
                  ++truenumofstates;
                if (!bestP[i][j].ifexist || bestP[i][j].score < newscore)
                  SETSTATE(bestP[i][j], true, i, j, newscore, MANNER_MULTI, ostate_ptr, NULL, 0, 0)
              }
          }

        // update bestM1
#ifdef DEBUG
        printf("updating bestM1\n");
#endif
        if (i != 0 && j != global_L-1 && j-i+1 >= 2)
          {
            // M1=P
            if (allowedpairposition[i][j] && bestP[i][j].ifexist)
              {
                struct State* ostate_ptr = & bestP[i][j];
                double newscore = scoreM1(i, j, j) + ostate_ptr->score;
                  ++truenumofstates;
                if (!bestM1[i][j].ifexist || bestM1[i][j].score < newscore)
                  SETSTATE(bestM1[i][j], true, i, j, newscore, MANNER_M1$P, ostate_ptr, NULL, 0, 0)
              }
            // M1=M1+U
            if (allowedunpairposition[j][j] && bestM1[i][j-1].ifexist)
              {
                struct State* ostate_ptr = & bestM1[i][j-1];
                double newscore = scoremultiunpaired(j,j) + ostate_ptr->score;
                  ++truenumofstates;
                if (!bestM1[i][j].ifexist || bestM1[i][j].score < newscore)
                  SETSTATE(bestM1[i][j], true, i, j, newscore, MANNER_M1$M1_U, ostate_ptr, NULL, 0, 0)
              }
          }

        // update bestM
#ifdef DEBUG
  printf("updating bestM\n");
#endif
        if (i != 0 && j != global_L-1 && j-i+1 >= 4)
          {
            for (int k = i+1; k < j-1; k ++)
              {
                // M = M1 M1
                if (bestM1[i][k].ifexist && bestM1[k+1][j].ifexist)
                  {
                    struct State* ostate1_ptr = & bestM1[i][k];
                    struct State* ostate2_ptr = & bestM1[k+1][j];
                    double newscore = ostate1_ptr->score + ostate2_ptr->score;
                      ++truenumofstates;
                    if (!bestM[i][j].ifexist || bestM[i][j].score < newscore)
                      SETSTATE(bestM[i][j], true, i, j, newscore, MANNER_M$M1_M1, ostate1_ptr, ostate2_ptr, 0, 0)
                  }

                // M = M M1
                if (bestM[i][k].ifexist && bestM1[k+1][j].ifexist)
                  {
                    struct State* ostate1_ptr = & bestM[i][k];
                    struct State* ostate2_ptr = & bestM1[k+1][j];
                    double newscore = ostate1_ptr->score + ostate2_ptr->score;
                      ++truenumofstates;
                    if (ostate1_ptr->manner != &MANNER_M$U_M[0] && (!bestM[i][j].ifexist || bestM[i][j].score < newscore))
                      SETSTATE(bestM[i][j], true, i, j, ostate1_ptr->score + ostate2_ptr->score, MANNER_M$M_M1, ostate1_ptr, ostate2_ptr, 0, 0)
                  }

                // M = unpaired + M
                if (allowedpairposition[i-1][j+1])
                  {
                    int k = i+1;
                    if (bestM[k][j].ifexist && allowedunpairposition[i][k-1])
                      {
                        struct State* ostate_ptr = & bestM[k][j];
                        double newscore = scoremultiunpaired(i, k-1) + ostate_ptr->score;
                          ++truenumofstates;
                        if (ostate_ptr->manner != &MANNER_M$U_M[0] && (!bestM[i][j].ifexist || bestM[i][j].score < newscore))
                          SETSTATE(bestM[i][j], true, i, j, newscore, MANNER_M$U_M, NULL, ostate_ptr, i, k-1)
                      }

                  }

              }
          }
      }

  // update bestC
#ifdef DEBUG
  printf("updating bestC\n");
#endif
  for (int j = 0; j < global_L; j ++)
    {
      // C=U
      if (allowedunpairposition[0][j])
        {
          double newscore = scoreexternalunpaired(0,j);
            ++truenumofstates;
          if (!bestC[0][j].ifexist || bestC[0][j].score < newscore)
            SETSTATE(bestC[0][j], true, 0, j, newscore, MANNER_C$U, NULL, NULL, 0, 0)
        }

      // C=P
      if (bestP[0][j].ifexist)
        {
          struct State* ostate_ptr = & bestP[0][j];
          double newscore = scoreexternalpaired(0,j) + ostate_ptr->score;
            ++truenumofstates;
          if (!bestC[0][j].ifexist || bestC[0][j].score < newscore)
            SETSTATE(bestC[0][j], true, 0, j, newscore, MANNER_C$P, ostate_ptr, NULL, 0, 0)
        }

      // C=C+U
      int k = j-1;
      if (bestC[0][k].ifexist && allowedunpairposition[k+1][j])
        {
          struct State* ostate_ptr = & bestC[0][k];
          double newscore = scoreexternalunpaired(k+1,j) + ostate_ptr->score;
            ++truenumofstates;
          if (!bestC[0][j].ifexist || bestC[0][j].score < newscore)
            SETSTATE(bestC[0][j], true, 0, j, newscore, MANNER_C$C_U, ostate_ptr, NULL, k+1, j)
              }

      for (int k = 0; k < j; k ++)
        {
          // C=C+P
          if (bestC[0][k].ifexist && allowedpairposition[k+1][j] && bestP[k+1][j].ifexist)
            {
              struct State* ostate1_ptr = & bestC[0][k];
              struct State* ostate2_ptr = & bestP[k+1][j];
              double newscore = scoreexternalpaired(k+1,j)+ostate1_ptr->score+ostate2_ptr->score;
                ++truenumofstates;
              if (!bestC[0][j].ifexist || bestC[0][j].score < newscore)
                SETSTATE(bestC[0][j], true, 0, j, newscore, MANNER_C$C_P, ostate1_ptr, ostate2_ptr, 0, 0)
            }
        }
    }

  /* // /\* print all state *\/ */
  /* for (int ii = 0; ii < global_L; ii ++) */
  /*   for (int jj = 0; jj < global_L; jj ++) */
  /*     { */
  /*       if (bestP[ii][jj].ifexist) */
  /*         printf("bestP %d %d %f %s\n", ii, jj, bestP[ii][jj].score, bestP[ii][jj].manner); */
  /*     } */
  /* for (int ii = 0; ii < global_L; ii ++) */
  /*   for (int jj = 0; jj < global_L; jj ++) */
  /*     { */
  /*       if (bestM1[ii][jj].ifexist) */
  /*         printf("bestM1 %d %d %f %s\n", ii, jj, bestM1[ii][jj].score, bestM1[ii][jj].manner); */
  /*     } */
  /* for (int ii = 0; ii < global_L; ii ++) */
  /*   for (int jj = 0; jj < global_L; jj ++) */
  /*     { */
  /*       if (bestM[ii][jj].ifexist) */
  /*         printf("bestM %d %d %f %s\n", ii, jj, bestM[ii][jj].score, bestM[ii][jj].manner); */
  /*     } */
  /* for (int ii = 0; ii < global_L; ii ++) */
  /*   for (int jj = 0; jj < global_L; jj ++) */
  /*     { */
  /*       if (bestC[ii][jj].ifexist) */
  /*         printf("bestC %d %d %f %s\n", ii, jj, bestC[ii][jj].score, bestC[ii][jj].manner); */
  /*     } */

  struct State best_state = bestC[0][global_L-1];
  printf("Viterbi score: %f\n", best_state.score);
  gettimeofday(&endtime, NULL);
  double elapsed_time = endtime.tv_sec - starttime.tv_sec + (endtime.tv_usec-starttime.tv_usec)/1000000.0;
  printf("Time: %f len: %d numofstates: %d  truenumofstates %lu\n", elapsed_time, global_L,
         numofstates, truenumofstates);
  DETAILEDPRINT
  printf(">structure\n");
  char structure[MAXN];

  memset(structure, 0, sizeof(structure));
  state_pp(& best_state, structure);
  structure[best_state.j - best_state.i + 1] = 0;
  printf("%s\n\n", structure);

  fflush(stdout); // lhuang

  struct DecoderResult decoder_result = {"", best_state.score, numofstates, elapsed_time};
  strcpy(decoder_result.structure, structure);
  return decoder_result;

}

int main(int argc, char **argv)
{
  // there must be an input file
  assert(argc >= 2);

  // initialize seq/ref file
  char* seq_file_name = argv[1];
  FILE* f_seq = fopen(seq_file_name, "r");
#ifdef REFS
  char* ref_file_name = argv[2];
  FILE* f_ref = fopen(ref_file_name, "r");
#endif

  // variables for decoding
  char seq[MAXN];
  char ref[MAXN];
  struct DecoderResult decoder_result;
  int num=0, total_len = 0, total_states = 0;
  double total_score = .0;
  double total_time = .0;
  int seqlen = 0;
#ifdef REFS
  // NOTE: eval function ignored.
  /* EvalResult eval_result; */
  /* int total_intersect = 0, total_predicted = 0, total_gold = 0; */
#endif

  // initialize cache single
  initialize();
  initializecachesingle();

  // go through the seq file to decode each seq
  while (fscanf(f_seq, "%s", seq) != EOF)
    {
#ifdef REFS
      fscanf(f_ref, "%s", ref);
#endif
      num ++;
      seqlen = strlen(seq);
      printf("seq %d len %d:\n%s\n", num, seqlen, seq); // passed
      fflush(stdout);
#ifdef FORCE_DECODE
      printf("ref:\n%s\n", ref);
#endif

      // decode and collect results
      decoder_result = solve(seq, ref); // ref for FORCE_DECODE only
      total_len += seqlen;
      total_score += decoder_result.score;
      total_states += decoder_result.numofstates;
      total_time += decoder_result.time;
/* #ifdef REFS */
/*       // NOTE: eval function ignored. */
/*       // eval on ref and collect results */
/*       eval_result = eval(decoder_result.structure, ref); */
/*       total_intersect += eval_result.intersect; */
/*       total_predicted += eval_result.predicted; */
/*       total_gold += eval_result.gold; */
/* #endif */
    }
  fclose(f_seq);
#ifdef REFS
  fclose(f_ref);

  // calculate performance
  // NOTE: eval function ignored. */
  /* double precision = 0.0, recall = 0.0, fscore = 0.0; */
  /* if (total_intersect) */
  /*   { */
  /*     precision = total_intersect / total_predicted; */
  /*     recall = total_intersect / total_gold; */
  /*     fscore = precision * recall / (2*(precision + recall)); */
  /*   } */
  /* printf("precision: %f; recall: %f; fscore: %f\n", precision, recall, fscore); */
#endif

  // print decoding statistics
  double dnum = (double)num;
  printf("num_seq: %d; avg_len: %f\n", num, total_len/dnum);
  printf("beam: %d; avg_score: %f; avg_time: %f; tot_time: %f; avg_states_used: %f\n", BEAMSIZE, total_score/dnum, total_time/dnum, total_time, total_states/dnum);
  return 0;
}
