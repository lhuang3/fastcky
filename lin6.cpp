//////////////////////////////////////////////////////////////////////
// lin6.cpp
//////////////////////////////////////////////////////////////////////


#if defined(HAMMING_LOSS)
#define ScoreUnpairedPosition(i) (loss_unpaired_position[i])
#else
#define ScoreUnpairedPosition(i) (RealT(0))
#endif
#define CountUnpairedPosition(i,v)

// score for leaving s[i+1...j] unpaired

#if defined(HAMMING_LOSS)
#define ScoreUnpaired(i,j) (loss_unpaired[offset[i]+j])
#else
#define ScoreUnpaired(i,j) (RealT(0))
#endif
#define CountUnpaired(i,j,v)

// score for a base pair which is not part of any helix

#if PARAMS_ISOLATED_BASE_PAIR
#define ScoreIsolated() score_isolated_base_pair.first
#define CountIsolated(v) { score_isolated_base_pair.second += (v); }
#else
#define ScoreIsolated() RealT(0)
#define CountIsolated(v)
#endif

// base score for a multi-branch loop

#if PARAMS_MULTI_LENGTH
#define ScoreMultiBase() score_multi_base.first
#define CountMultiBase(v) { score_multi_base.second += (v); }
#else
#define ScoreMultiBase() RealT(0)
#define CountMultiBase(v)
#endif

// score for a base-pair adjacent to a multi-branch loop

#if PARAMS_MULTI_LENGTH
#define ScoreMultiPaired() score_multi_paired.first
#define CountMultiPaired(v) { score_multi_paired.second += (v); }
#else
#define ScoreMultiPaired() RealT(0)
#define CountMultiPaired(v)
#endif

// score for each unpaired position in a multi-branch loop

#if PARAMS_MULTI_LENGTH
#define ScoreMultiUnpaired(i) (score_multi_unpaired.first + ScoreUnpairedPosition(i))
#define CountMultiUnpaired(i,v) { score_multi_unpaired.second += (v); CountUnpairedPosition(i,v); }
#else
#define ScoreMultiUnpaired(i) (ScoreUnpairedPosition(i))
#define CountMultiUnpaired(i,v) { CountUnpairedPosition(i,v); }
#endif

// score for each base-pair adjacent to an external loop

#if PARAMS_EXTERNAL_LENGTH
#define ScoreExternalPaired() score_external_paired.first
#define CountExternalPaired(v) { score_external_paired.second += (v); }
#else
#define ScoreExternalPaired() RealT(0)
#define CountExternalPaired(v)
#endif

// score for each unpaired position in an external loop

#if PARAMS_EXTERNAL_LENGTH
#define ScoreExternalUnpaired(i) (score_external_unpaired.first + ScoreUnpairedPosition(i))
#define CountExternalUnpaired(i,v) { score_external_unpaired.second += (v); CountUnpairedPosition(i,v); }
#else
#define ScoreExternalUnpaired(i) (ScoreUnpairedPosition(i))
#define CountExternalUnpaired(i,v) { CountUnpairedPosition(i,v); }
#endif

// score for a helix stacking pair of the form:
//
//       |         |
//    s[i+1] == s[j-1]
//       |         |
//     s[i] ==== s[j]
//       |         |

#if PARAMS_HELIX_STACKING
#if PROFILE
#define ScoreHelixStacking(i,j) profile_score_helix_stacking[i*(L+1)+j].first
#define CountHelixStacking(i,j,v) { profile_score_helix_stacking[i*(L+1)+j].second += (v); }
#else
#define ScoreHelixStacking(i,j) score_helix_stacking[s[i]][s[j]][s[i+1]][s[j-1]].first
#define CountHelixStacking(i,j,v) { score_helix_stacking[s[i]][s[j]][s[i+1]][s[j-1]].second += (v); }
#endif
#else
#define ScoreHelixStacking(i,j) RealT(0)
#define CountHelixStacking(i,j,v)
#endif

//////////////////////////////////////////////////////////////////////
// UPDATE_MAX()
//
// Macro for updating a score/traceback pointer which does not
// evaluate t unless an update is needed.  Make sure that this is
// used as a stand-alone statement (i.e., not the "if" condition
// of an if-then-else statement.)
//////////////////////////////////////////////////////////////////////

#define UPDATE_MAX(bs,bt,s,t) { RealT work(s); if ((work)>(bs)) { (bs)=(work); (bt)=(t); } }

//////////////////////////////////////////////////////////////////////
// FillScores()
// FillCounts()
// 
// Routines for setting scores and counts quickly. 
//////////////////////////////////////////////////////////////////////

template<class RealT>
void InferenceEngine<RealT>::FillScores(typename std::vector<std::pair<RealT, RealT> >::iterator begin, typename std::vector<std::pair<RealT, RealT> >::iterator end, RealT value)
{
    while (begin != end)
    {
        begin->first = value;
        ++begin;
    }
}

template<class RealT>
void InferenceEngine<RealT>::FillCounts(typename std::vector<std::pair<RealT,RealT> >::iterator begin, typename std::vector<std::pair<RealT,RealT> >::iterator end, RealT value)
{
    while (begin != end)
    {
        begin->second = value;
        ++begin;
    }
}

//////////////////////////////////////////////////////////////////////
// ComputeRowOffset()
//
// Consider an N x N upper triangular matrix whose elements are
// stored in a one-dimensional flat array using the following
// row-major indexing scheme:
//
//     0  1  2  3     <-- row 0
//        4  5  6     <-- row 1
//           7 [8]    <-- row 2
//              9     <-- row 3
//
// Assuming 0-based indexing, this function computes offset[i]
// for the ith row such that offset[i]+j is the index of the
// (i,j)th element of the upper triangular matrix in the flat
// array.
//
// For example, offset[2] = 5, so the (2,3)th element of the
// upper triangular matrix (marked in the picture above) can be 
// found at position offset[2]+3 = 5+3 = 8 in the flat array.
//////////////////////////////////////////////////////////////////////

template<class RealT>
int InferenceEngine<RealT>::ComputeRowOffset(int i, int N) const
{
    Assert(i >= 0 && i <= N, "Index out-of-bounds.");
    
    // equivalent to:
    //   return N*(N+1)/2 - (N-i)*(N-i+1)/2 - i;
    return i*(N+N-i-1)/2;
}

//////////////////////////////////////////////////////////////////////
// InferenceEngine::IsComplementary()
//
// Determine if a pair of positions is considered "complementary."
//////////////////////////////////////////////////////////////////////

template<class RealT>
bool InferenceEngine<RealT>::IsComplementary(int i, int j) const
{
    Assert(1 <= i && i <= L, "Index out-of-bounds.");
    Assert(1 <= j && j <= L, "Index out-of-bounds.");

#if !PROFILE
    return is_complementary[s[i]][s[j]];
#else
    RealT complementary_weight = 0;
    RealT total_weight = 0;

    for (int k = 0; k < N; k++)
    {
        if (is_complementary[A[k*(L+1)+i]][A[k*(L+1)+j]]) complementary_weight += weights[k];
        total_weight += weights[k];
    }

    return complementary_weight / total_weight >= std::min(RealT(N-1) / RealT(N), RealT(0.5));
#endif
}

//////////////////////////////////////////////////////////////////////
// InferenceEngine::InferenceEngine()
//
// Constructor
//////////////////////////////////////////////////////////////////////

template<class RealT>
InferenceEngine<RealT>::InferenceEngine(bool allow_noncomplementary) :
    allow_noncomplementary(allow_noncomplementary),
    cache_initialized(false),
    parameter_manager(NULL),
    L(0),
    SIZE(0)
#if PROFILE
    , N(0)
    , SIZE2(0)
#endif

{
    // precompute mapping from characters to index representation
    std::memset(char_mapping, BYTE(alphabet.size()), 256);
    for (size_t i = 0; i < alphabet.size(); i++)
    {
        char_mapping[BYTE(tolower(alphabet[i]))] = 
            char_mapping[BYTE(toupper(alphabet[i]))] = i;
    }
    
    // precompute complementary pairings
    for (int i = 0; i <= M; i++)
        for (int j = 0; j <= M; j++)
            is_complementary[i][j] = 0;
    
    is_complementary[char_mapping[BYTE('A')]][char_mapping[BYTE('U')]] = 
        is_complementary[char_mapping[BYTE('U')]][char_mapping[BYTE('A')]] = 
        is_complementary[char_mapping[BYTE('G')]][char_mapping[BYTE('U')]] = 
        is_complementary[char_mapping[BYTE('U')]][char_mapping[BYTE('G')]] = 
        is_complementary[char_mapping[BYTE('C')]][char_mapping[BYTE('G')]] = 
        is_complementary[char_mapping[BYTE('G')]][char_mapping[BYTE('C')]] = 1;
}
