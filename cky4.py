#!/usr/bin/env python

from __future__ import division

import sys
from collections import defaultdict, Counter
import math
import time
from rna import RNA, FScore
import gflags as flags
FLAGS=flags.FLAGS
precision = 1e-6
MAXINT = 100000000

PROFILE = False

allowedpairs = set(['AU','UA','CG','GC','GU','UG'])
helixstackingcombinations = set([
    'AUAU', 'AUCG', 'AUGC', 'AUGU', 'AUUA', 'AUUG',
    'CGAU', 'CGCG', 'CGGC', 'CGGU', 'CGUG',
    'GCAU', 'GCCG', 'GCGU', 'GCUG', 'GUAU', 'GUGU', 'GUUG',
    'UAAU', 'UAGU', 'UGGU'
])

EXPLICIT_MAX_LEN = 4
SINGLE_MIN_LEN = 0
SINGLE_MAX_LEN = 30

HAIRPIN_MIN_LEN = 0
BULGE_MIN_LEN = 1
INTERNAL_MIN_LEN = 2
SYMMETRIC_MIN_LEN = 1
ASYMMETRY_MIN_LEN = 1

HAIRPIN_MAX_LEN = 30
BULGE_MAX_LEN = SINGLE_MAX_LEN
INTERNAL_MAX_LEN = SINGLE_MAX_LEN
SYMMETRIC_MAX_LEN = 15
ASYMMETRY_MAX_LEN = 28

features_atleast = [
    "hairpin_length",
    "bulge_length",
    "internal_length",
    "internal_symmetric_length",
    "internal_asymmetry"
]

bounds = [
    (HAIRPIN_MIN_LEN, HAIRPIN_MAX_LEN),
    (BULGE_MIN_LEN, BULGE_MAX_LEN),
    (INTERNAL_MIN_LEN, INTERNAL_MAX_LEN),
    (SYMMETRIC_MIN_LEN, SYMMETRIC_MAX_LEN),
    (ASYMMETRY_MIN_LEN, ASYMMETRY_MAX_LEN),
]

def mydot(feats, weightdict):
    return sum((0.0 if key not in weightdict else weightdict[key]) for key in feats)

def printfv(fv, fw, ifprintvalue=False):
    counterfv_before = Counter(fv)
    counterfv = Counter()
    for key in counterfv_before:
        inatleast = False
        for index, fea in enumerate(features_atleast):
            _min, _max = bounds[index]
            if key.startswith(fea):
                length = int(key.rsplit("_", 1)[1])
                count = counterfv_before[key]
                for i in xrange(_min, length+1):
                    counterfv[fea+"_at_least_%d" % i] += count
                inatleast = True
                break
        if not inatleast:
            counterfv[key] += counterfv_before[key]

    # transfer '_exact' back to '_at_least'
    printlist = [(x, counterfv[x]) for x in counterfv]
    if ifprintvalue:
        for (x,y) in sorted(printlist):
            print x, y, fw[x]*y
            print "sum value: %f" % sum([fw[x]*y for (x,y) in printlist])
    else:
        for (x,y) in sorted(printlist):
            print x, y
    return

class State(object):
    __slots__ = "i", "j", "score", "backptr", "fv", "_hash", "_signature"
    bases = []
    seq = []
    L = 0 ## length of seq

    def __init__(self, i, j, score=0, backptr=(None, None, None), fv=[]):
        # backptr = manner, parent1, parent2
        self.i = i
        self.j = j
        self.score = score
        self.backptr = backptr
        self.fv = fv
        self._hash = None
        self._signature = None

    def __eq__(self, other):
        return self.signature() == other.signature()

    def signature(self):
        if self._signature is None:
            self._signature = self.i, self.j
        return self._signature

    def __hash__(self):
        if self._hash is None:
            self._hash = hash(self.signature())
        return self._hash

    def pp(self):
        ## manners:
        ## hairpin, helix, single, multi,
        ## M1, M=M1+M1, M=M+M1, M=U+M
        ## C=U, C=P, C=C+U, C=C+P
        i, j = self.i, self.j
        if i > j:
            return "", self.fv
        elif i == j:
            return ".", self.fv #singleton unpaired
        manner, parent1, parent2 = self.backptr

        if manner == "hairpin":
            return "(%s)" % ("."*(j-i-1)), self.fv
        elif manner == "single":
            s1, fv1 = parent1.pp()
            l1, l2 = parent2
            return "(%s%s%s)" % ("."*l1, s1, "."*l2), self.fv + fv1
        elif manner in ["helix", "multi"]:
            s1, fv1 = parent1.pp()
            return "(%s)" % s1, self.fv + fv1
        elif manner in ["M1", "C=C+U"]:
            s1, fv1 = parent1.pp()
            p,q = parent2
            return "%s%s" % (s1, "."*(q-p+1)), self.fv + fv1
        elif manner in ["M=M1+M1", "M=M+M1", "C=C+P"]:
            s1, fv1 = parent1.pp()
            s2, fv2 = parent2.pp()
            return "%s%s" % (s1, s2), self.fv + fv1 + fv2
        elif manner == "M=U+M":
            p,q = parent1
            s2, fv2 = parent2.pp()
            return "%s%s" % ("."*(q-p+1), s2), self.fv + fv2
        elif manner == "C=U":
            return "%s" % ("."*(j-i+1)), self.fv
        elif manner == "C=P":
            s1, fv1 = parent1.pp()
            return "%s" % s1, self.fv + fv1
        else:
            assert False, "unknown manner: %s" % manner

    def __str__(self):
        return "(%d, %d) %s %.1f %s" % (self.i, self.j, self.pp()[0], self.score, self.backptr[0])

class Decoder(object):

    def __init__(self):
        pass

    def loadmodel(self, modelfilename):
        self.fw = defaultdict(float)
        for line in open(modelfilename):
            key, value = line.split()
            self.fw[key] = float(value)

        ## transfer '_at_least' to 'exact'
        for index, fea in enumerate(features_atleast):
            _min, _max = bounds[index]
            self.fw[fea+"_exact_%d" % _min] = self.fw[fea+"_at_least_%d" % _min]
            for i in xrange(_min+1, _max+1):
                self.fw[fea+"_exact_%d" % i] = self.fw[fea+"_exact_%d" % (i-1)] + self.fw[fea+"_at_least_%d" % i]
        return

    def update(self, state, dct, manner):
        if state is not None:
            i, j = state.i, state.j
            self.cnt[manner] += 1
            oldstate = dct[i,j]
            if oldstate is None:
                dct[i,j] = state
            elif state.score > oldstate.score:
                del dct[i,j]
                dct[i,j] = state

    def solve(self, bases, sequence = ""):
        self.starttime = time.time()
        self.cnt = Counter()

        State.bases = bases
        State.seq = sequence
        State.L = len(bases)

        allowedpairposition = defaultdict(lambda : False)
        allowedunpairposition = defaultdict(lambda : True) ## useful when structure is stricted
        bestP = defaultdict(lambda : None)
        bestM1 = defaultdict(lambda : None)
        bestM = defaultdict(lambda : None)
        bestC = defaultdict(lambda : None)

        if sequence:
            stack = []
            for index, item in enumerate(sequence):
                if item == ".":
                    pass
                elif item == "(":
                    stack.append(index)
                elif item == ")":
                    q = index
                    p = stack[-1]
                    stack = stack[:-1]
                    allowedpairposition[p,q] = True
            for i in xrange(State.L):
                for j in xrange(i, State.L):
                    if "(" not in sequence[i:j+1] and ")" not in sequence[i:j+1]:
                        allowedunpairposition[i,j] = True
                    else:
                        allowedunpairposition[i,j] = False
        else:
            for i in xrange(State.L):
                for j in xrange(i, State.L):
                    pair = "".join(sorted(State.bases[i]+State.bases[j]))
                    if pair in allowedpairs:
                        allowedpairposition[i,j] = True

        for length in xrange(2, State.L+1):
            for i in xrange(State.L-length+1):
                j = i+length-1

                ## update bestP
                if allowedpairposition[i,j]:
                    ## hairpin
                    fv, score = self.scorehairpin(i,j)
                    state = State(i, j, score, ("hairpin", None, None), fv)
                    self.update(state, bestP, "hairpin")

                    ## helix and single-branched loops
                    ## constraints: single length <= SINGLE_MAX_LEN
                    for p in xrange(i+1, min(j,i+1+SINGLE_MAX_LEN+1)):
                        for q in xrange(max(p+1, j-1-SINGLE_MAX_LEN+(p-i-1)), j):
                            if p == i+1 and q == j-1:
                                if bestP[p,q] != None:
                                    ostate = bestP[p,q]
                                    fv, score = self.scorehelix(i,j)
                                    state = State(i, j, score + ostate.score, ("helix", ostate, None), fv)
                                    self.update(state, bestP, "helix")
                            elif bestP[p,q] != None and allowedunpairposition[i+1,p-1] and allowedunpairposition[q+1,j-1]:
                                ostate = bestP[p,q]
                                fv, score = self.scoresingle(i,j,p,q)
                                state = State(i, j, score + ostate.score, ("single", ostate, (p-i-1, j-q-1)), fv)
                                self.update(state, bestP, "single")
                    ## multi
                    if i != 0 and j != State.L-1 and bestM[i+1,j-1] != None:
                        ostate = bestM[i+1,j-1]
                        fv, score = self.scoremulti(i,j)
                        state = State(i, j, score + ostate.score, ("multi", ostate, None), fv)
                        self.update(state, bestP, "multi")

                ## update bestM1
                if i != 0 and j != State.L-1:
                    for k in xrange(i+1, j+1):
                        if bestP[i,k] != None and allowedunpairposition[k+1,j]:
                            ostate = bestP[i,k]
                            fv, score = self.scoreM1(i,j,k)
                            state = State(i, j, score + ostate.score, ("M1", ostate, (k+1,j)), fv)
                            self.update(state, bestM1, "M1")

                ## update bestM
                if i != 0 and j != State.L-1:
                    for k in xrange(i+1, j-1):
                        # M = M1 M1
                        if bestM1[i,k] != None and bestM1[k+1,j] != None:
                            ostate1 = bestM1[i,k]
                            ostate2 = bestM1[k+1,j]
                            state = State(i, j, ostate1.score + ostate2.score, ("M=M1+M1", ostate1, ostate2), [])
                            self.update(state, bestM, "M=M1+M1")
                        # M = M M1
                        if bestM[i,k] != None and bestM1[k+1,j] != None:
                            ostate1 = bestM[i,k]
                            ostate2 = bestM1[k+1,j]
                            state = State(i, j, ostate1.score + ostate2.score, ("M=M+M1", ostate1, ostate2), [])
                            self.update(state, bestM, "M=M+M1")
                    # M = unpaired + M
                    for k in xrange(i+1, j):
                        if bestM[k,j] != None and allowedunpairposition[i,k-1]:
                            ostate = bestM[k,j]
                            fv, score = self.scoremultiunpaired(i,k-1)
                            state = State(i, j, score + ostate.score, ("M=U+M", (i, k-1), ostate), fv)
                            self.update(state, bestM, "M=U+M")

        ## update bestC
        for j in xrange(0,State.L):
            # C=U
            if allowedunpairposition[0,j]:
                fv, score = self.scoreexternalunpaired(0,j)
                state = State(0, j, score, ("C=U", None, None), fv)
                self.update(state, bestC, "C=U")
            # C=P
            if bestP[0,j] != None:
                ostate = bestP[0,j]
                fv, score = self.scoreexternalpaired(0,j)
                state = State(0, j, score + ostate.score, ("C=P", ostate, None), fv)
                self.update(state, bestC, "C=P")

            for k in xrange(0, j):
                # C=C+U
                if bestC[0,k] != None and "U" not in bestC[0,k].backptr[0] and allowedunpairposition[k+1,j]:
                    ostate = bestC[0,k]
                    fv, score = self.scoreexternalunpaired(k+1,j)
                    state = State(0, j, score + ostate.score, ("C=C+U", ostate, (k+1,j)), fv)
                    self.update(state, bestC, "C=C+U")
                # C=C+P
                if bestC[0,k] != None and bestP[k+1,j] != None:
                    ostate1 = bestC[0,k]
                    ostate2 = bestP[k+1,j]
                    fv, score = self.scoreexternalpaired(k+1,j)
                    state = State(0, j, score + ostate1.score + ostate2.score, ("C=C+P", ostate1, ostate2), fv)
                    self.update(state, bestC, "C=C+P")

        ## end loop

        # ## debug
        # for key in bestP:
        #     if bestP[key] is not None:
        #         print "bestP", bestP[key]
        # for key in bestM1:
        #     if bestM1[key] is not None:
        #         print "bestM1", bestM1[key]
        # for key in bestM:
        #     if bestM[key] is not None:
        #         print "bestM", bestM[key]
        # for key in bestC:
        #     if bestC[key] is not None:
        #         print "bestC", bestC[key]
        best_state = bestC[0, State.L-1]

        s = "Viterbi score: %8.5f" % best_state.score
        print State.L, s,
        t = time.time() - self.starttime
        allstates = sum(self.cnt.values())
        print "Time: %.5f seconds  len: %d  all: %d" % (t, State.L, allstates)

        ## missing cnt2 counts
        struct, fv = best_state.pp()
        if FLAGS.feats:
            printfv(fv, self.fw, False)

        print "".join(State.bases)
        print ">structure"
        print struct
        print
        sys.stdout.flush()
        return struct, best_state.score, allstates, t

    @staticmethod
    def getbasepair(i, j):
        pair = "".join(sorted(State.bases[i]+State.bases[j]))
        assert pair in allowedpairs
        return "base_pair_%s" % pair

    @staticmethod
    def getmultibase():
        return "multi_base"

    @staticmethod
    def getmultipaired():
        return "multi_paired"

    @staticmethod
    def getmultiunpaired():
        return "multi_unpaired"

    @staticmethod
    def getexternalpaired():
        return "external_paired"

    @staticmethod
    def getexternalunpaired():
        return "external_unpaired"

    @staticmethod
    def gethelixstacking(i, j):
        string = State.bases[i]+State.bases[j]+State.bases[i+1]+State.bases[j-1]
        if string in helixstackingcombinations:
            return "helix_stacking_%s" % string
        else:
            return "helix_stacking_%s" % string[::-1]

    @staticmethod
    def gethelixclosing(i, j):
        return "helix_closing_%s" % (State.bases[i]+State.bases[j])

    @staticmethod
    def getterminalmismatch(i, j):
        return "terminal_mismatch_%s" % (State.bases[i]+State.bases[j]+State.bases[i+1]+State.bases[j-1])

    @staticmethod
    def gethairpinlength(l):
        l = min(l, HAIRPIN_MAX_LEN)
        return "hairpin_length_exact_%d" % l

    @staticmethod
    def getinternalexplicit(l1, l2):
        l1 = min(l1, EXPLICIT_MAX_LEN)
        l2 = min(l2, EXPLICIT_MAX_LEN)
        if l1 > l2:
            l1,l2 = l2,l1
        return "internal_explicit_%d_%d" % (l1, l2)

    @staticmethod
    def getbulgelength(l):
        l = min(l, BULGE_MAX_LEN)
        return "bulge_length_exact_%d" % l

    @staticmethod
    def getinternallength(l):
        l = min(l, INTERNAL_MAX_LEN)
        return "internal_length_exact_%d" % l

    @staticmethod
    def getinternalsymmetric(l):
        l = min(l, SYMMETRIC_MAX_LEN)
        return "internal_symmetric_length_exact_%d" % l

    @staticmethod
    def getinternalasymmetry(d):
        d = min(d, ASYMMETRY_MAX_LEN)
        return "internal_asymmetry_exact_%d" % d

    @staticmethod
    def getbulgenuc(i):
        return "bulge_0x1_nucleotides_%s" % State.bases[i]

    @staticmethod
    def getinternalnuc(i, j):
        pair = "".join(sorted(State.bases[i]+State.bases[j]))
        return "internal_1x1_nucleotides_%s" % pair

    @staticmethod
    def getdangleleft(i, j):
        return "dangle_left_%s" % (State.bases[i]+State.bases[j]+State.bases[i+1])

    @staticmethod
    def getdangleright(i, j):
        return "dangle_right_%s" % (State.bases[i]+State.bases[j]+State.bases[j-1])

    ## initializecachesingle:
    ## calculate some single score before inference
    def initializecachesingle(self):
        self.cachefvsingle = defaultdict(lambda : defaultdict(list))
        self.cachesingle = defaultdict(lambda : defaultdict(float))
        for l1 in xrange(SINGLE_MIN_LEN, SINGLE_MAX_LEN+1):
            for l2 in xrange(SINGLE_MIN_LEN, SINGLE_MAX_LEN+1):
                if l1 == 0 and l2 == 0:
                    continue
                elif l1 == 0:
                    self.cachefvsingle[l1][l2].append(Decoder.getbulgelength(l2))
                elif l2 == 0:
                    self.cachefvsingle[l1][l2].append(Decoder.getbulgelength(l1))
                else:
                    self.cachefvsingle[l1][l2].append(Decoder.getinternallength(min(l1+l2, INTERNAL_MAX_LEN)))
                    if l1 <= EXPLICIT_MAX_LEN and l2 <= EXPLICIT_MAX_LEN:
                        self.cachefvsingle[l1][l2].append(Decoder.getinternalexplicit(l1, l2))
                    if l1 == l2:
                        self.cachefvsingle[l1][l2].append(Decoder.getinternalsymmetric(min(l1, SYMMETRIC_MAX_LEN)))
                    else:
                        self.cachefvsingle[l1][l2].append(Decoder.getinternalasymmetry(min(math.fabs(l1-l2), ASYMMETRY_MAX_LEN)))
                self.cachesingle[l1][l2] = mydot(self.cachefvsingle[l1][l2], self.fw)
        return

    ## scorejunctionA:
    ## returns the feature vector for an asymmetric junction at position i and j such that
    ## (i, j) are base-paired and (i+1, j-1) are free.
    ## This applies to multi-branch loops which difference from scorejunctionB
    ## that applies to hairpin and single-branch loops
    def scorejunctionA(self, i, j):
        fv = [
            Decoder.gethelixclosing(i, j),
            ]
        if i < State.L-1:
            fv.append(Decoder.getdangleleft(i, j))
        if j > 0:
            fv.append(Decoder.getdangleright(i, j))
        return fv, mydot(fv, self.fw)

    ## scorejunctionB:
    ## returns the feature vector for a symmetric junction at position i and j such that
    ## (i, j) are base-paired and (i+1, j-1) are free.
    def scorejunctionB(self, i, j):
        fv = [
            Decoder.gethelixclosing(i, j),
            Decoder.getterminalmismatch(i, j)
        ]
        return fv, mydot(fv, self.fw)

    ## scorehairpin:
    ## returns the feature vector for a hairpin spanning positions i to j.
    def scorehairpin(self, i, j):
        fvjunc, scorejunc = self.scorejunctionB(i,j)
        fv = [Decoder.gethairpinlength(j-i-1)]
        score = mydot(fv, self.fw)
        return fvjunc+fv, scorejunc+score

    ## scorehelix:
    ## returns the feature vector for a helix starting at positions i and j
    ## which all base-pairs except for x[i]-x[j] are scored.
    def scorehelix(self, i, j):
        fv = [
            Decoder.gethelixstacking(i, j),
            Decoder.getbasepair(i+1, j-1)
        ]
        return fv, mydot(fv, self.fw)

    ## scoresinglenuc:
    ## returns the feature vector for nucleotides in a single-branch loop
    ## spanning i to j and p to q.
    ## Here x[i] and x[j] are paired. Inside of them, x[p] and x[q] are paired.
    def scoresinglenuc(self, i, j, p, q):
        l1, l2 = p-i-1, j-q-1
        fv = []
        if l1 == 0 and l2 == 1:
            fv.append(Decoder.getbulgenuc(q+1))
        elif l1 == 1 and l2 == 0:
            fv.append(Decoder.getbulgenuc(p-1))
        elif l1 == 1 and l2 == 1:
            fv.append(Decoder.getinternalnuc(p-1,q+1))
        return fv, mydot(fv, self.fw)

    ## scoresingle:
    ## returns the feature vector for a single-branch loop
    ## spanning i to j and p to q.
    def scoresingle(self, i, j, p, q):
        l1, l2 = p-i-1, j-q-1
        fvcache, scorecache = self.cachefvsingle[l1][l2], self.cachesingle[l1][l2]
        fvjunc1, scorejunc1 = self.scorejunctionB(i,j)
        fvjunc2, scorejunc2 = self.scorejunctionB(q,p)
        fvnuc, scorenuc = self.scoresinglenuc(i,j,p,q)
        fv = [Decoder.getbasepair(p,q)]
        score = mydot(fv, self.fw)
        return fv+fvcache+fvjunc1+fvjunc2+fvnuc, score+scorecache+scorejunc1+scorejunc2+scorenuc

    ## scoremulti:
    ## returns the feature vector for a multi-branch loop
    def scoremulti(self, i, j):
        fvjunc, scorejunc = self.scorejunctionA(i,j)
        fv = [
            Decoder.getmultipaired(),
            Decoder.getmultibase()
        ]
        score = mydot(fv, self.fw)
        return fv+fvjunc, score+scorejunc

    ## scoremultiunpaired
    ## returns the feature vector for umpaired positions x[i]-x[j] in multi-branch loops
    def scoremultiunpaired(self, i, j):
        fv = [Decoder.getmultiunpaired()] * (j-i+1)
        return fv, mydot(fv, self.fw)

    ## scoreM1
    ## returns the feature vector for a M1 i,j,k in which
    ## x[i]-x[k] are paired, and x[k+1]..x[j] are unpaired
    def scoreM1(self, i, j, k):
        fvjunc, scorejunc = self.scorejunctionA(k,i)
        fvunpaired, scoreunpaired = self.scoremultiunpaired(k+1,j)
        fv = [
            Decoder.getbasepair(i,k),
            Decoder.getmultipaired()
        ]
        score = mydot(fv, self.fw)
        return fv+fvjunc+fvunpaired, score+scorejunc+scoreunpaired

    ## scoreexternalpaired
    def scoreexternalpaired(self, i, j):
        fvjunc, scorejunc = self.scorejunctionA(j,i)
        fv = [
            Decoder.getexternalpaired(),
            Decoder.getbasepair(i,j),
]
        score = mydot(fv, self.fw)
        return fv+fvjunc, score+scorejunc

    ## scoreexternalunpaired
    def scoreexternalunpaired(self, i, j):
        fv = [Decoder.getexternalunpaired()] * (j-i+1)
        return fv, mydot(fv, self.fw)


def main():
    decoder = Decoder()
    decoder.loadmodel(FLAGS.params)
    decoder.initializecachesingle()

    sequence = FLAGS.sequence
    if FLAGS.refs:
        reffile = open(FLAGS.refs)
    total_f = FScore()
    num = 0
    total_len = total_states = total_score = total_time = 0
    for line in sys.stdin:
        line = line.strip() # UACG....
        struct, score, states, time = decoder.solve(line, sequence)
        num += 1
        total_score += score
        total_states += states
        total_time += time
        total_len += len(line)
        if FLAGS.refs:
            refline = reffile.readline().strip()
            rna1 = RNA.loadpp(line, refline)
            rna2 = RNA.loadpp(line, struct)
            f = FScore.evaluate(rna1, rna2)
            print "eval: len=", len(rna1), f
            print
            total_f += f

    print "num_seq: %d, avg_len: %.1f" % (num, 
                                          total_len / num), \
        "beam: %d, avg_score: %.4f, avg_time: %.3f, avg_states: %.1f" % (FLAGS.b, 
                                                                         total_score / num, 
                                                                         total_time / num,
                                                                         total_states / num),
    if FLAGS.refs:
        print total_f
    else:
        print

if __name__ == "__main__":

    flags.DEFINE_boolean("profile", False, "profile")
    flags.DEFINE_boolean("debug", False, "debug (print chart)", short_name="D")
    flags.DEFINE_boolean("feats", False, "print features of 1-best parse")
    flags.DEFINE_boolean("values", False, "print feature values of 1-best parse")
    flags.DEFINE_string("params", "params.my", "parameters file")
    flags.DEFINE_boolean("realparams", False, "use contrafold.params.complementary")
    flags.DEFINE_string("sequence", "", "true sequence (to generate feature vector)", short_name="s")
    flags.DEFINE_boolean("uniq", False, "also print uniq states counts")    
    flags.DEFINE_integer("beam", 0, "beam size (default: no pruning)", short_name="b")
    flags.DEFINE_boolean("multi", True, "consider multi-loops")    
    flags.DEFINE_string("refs", "", "reference brackets file", short_name="r")

    argv = FLAGS(sys.argv)
    if FLAGS.realparams:
        FLAGS.params = "contrafold.params.complementary"
    if FLAGS.values:
        FLAGS.feats = True

    if FLAGS.profile:
        import cProfile as profile
        profile.run("main()", "cky.profile")
        import pstats
        p = pstats.Stats("cky.profile")
        p.sort_stats("time").print_stats(60)
    else:
        main()

