set term postscript eps enhanced color
set output '|ps2pdf - outputfile.pdf'
set title "VCF Speed"
set xlabel "seq len"
set ylabel "running time"
plot "< tail -149 fastcky11.speed" with lines title "fastcky", \
     "< tail -149 viterbi6.speed" with lines title "contrafold", \
     "< tail -149 vienna.speed" with lines title "vienna",\
     "< tail -149 fastcky11.speed" using ($1):(($2)/8.0) with lines title "fastcky (1/8)"  
exit()