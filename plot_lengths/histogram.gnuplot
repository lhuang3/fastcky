#!/bin/gnuplot --persist

set terminal postscript eps enhanced color 
set output '| epstopdf --filter --outfile=/dev/stdout'

set key top right
set border 3

# Each bar is half the (visual) width of its x-range.
set boxwidth 90 absolute
set style fill solid 1.0 noborder

set ytics nomirror
set y2tics

set yrange [-10:]

bin_width = 100

bin_number(x) = floor(x/bin_width)

rounded(x) = bin_width * ( bin_number(x) + 0.5 )

plot "/tmp/aaa" u (rounded($1)):(1) smooth frequency with boxes t "all",\
     ""         u 1:(1) smooth cnorm t "all" axes x1y2,\
     "/tmp/bbb" u (rounded($1)):(1) smooth frequency with boxes t "pknot free",\
     ""         u 1:(1) smooth cnorm t "pknot free" axes x1y2