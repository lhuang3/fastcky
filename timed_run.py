#!/usr/bin/env python

"""run command on each line of the inputfile, with time measured
Usage: timed_run.py command inputfile logfile timeout
e.g.
timed_run.py "RNAfold -i %s" inputseq rnafold.log
timed_run.py "contrafold predict --params src/contrafold.params.complementary --viterbi %s" inputseq rnafold
"""

import sys
import tempfile
import time
import subprocess

def main():
    command = sys.argv[1]
    inputfile = sys.argv[2]
    logfile = sys.argv[3]
    timeout = float("inf")
    if len(sys.argv) == 5:
        timeout = float(sys.argv[4])

    with open(logfile, "w") as log:
        for i, _line in enumerate(open(inputfile)):
            line = _line.strip()
            tempf = tempfile.NamedTemporaryFile()
            print >> tempf, line
            tempf.flush()
            start_time = time.time()
            output = subprocess.check_output(command % tempf.name, shell=True)
            end_time = time.time()
            elapsed_time = end_time - start_time
            if elapsed_time > timeout:
                break
            print >> log, output
            print >> log, "External Info: seq %d len %d time %f" % (i+1, len(line), elapsed_time)
            print >> log
            log.flush()



if __name__ == "__main__":
    main()
