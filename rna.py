#!/usr/bin/env python

"""
Representation of an RNA sequence with secondary structure and its evaluation.
"""

from __future__ import division

import sys

# pairs that are allowed to have links
allowed_pairs = set(['AU','UA','CG','GC','GU','UG'])


class RNA(object):

    __slots__ = "seq", "pairs", "filename"

    def __init__(
        self,
        seq=None,
        pairs=None,
        filename=""
    ):
        self.seq = seq
        self.pairs = pairs
        self.filename = filename

    def __len__(self):
        return len(self.seq)

    def has_multi(self):
        level = 0
        loops = [0]
        for i, x in enumerate(self.pairs):
            if x == -1:
                pass
            elif x > i:  # (
                loops[-1] += 1
                if level > 0 and loops[-1] >= 2:
                    return True
                level += 1
                loops.append(0)
            else:  # )
                level -= 1
                loops.pop()
        return False # no multi loop

    brackets = ['()', '[]', '{}', '<>']

    def pp(self): # ((..((..[[..)).]].))
        def contains((a,b), (c,d)):
            return a < c < d < b
        result = []
        stacks = [[]] 
        for i, x in enumerate(self.pairs):
            if x == -1:
                result.append(".")
            elif x > i:
                for j, stack in enumerate(stacks):
                    if stack == [] or contains(stack[-1], (i, x)):
                        stack.append((i, x))
                        break
                else:
                    stacks.append([(i,x)]) # new level
                    j += 1
                result.append(RNA.brackets[j][0]) # ( or [ or { or <
            else:
                for j, stack in enumerate(stacks):
                    if stack != [] and stack[-1][1] == i:
                        stack.pop()                        
                        result.append(RNA.brackets[j][1]) # ) or ] or } or >
                        break
                    
        return "".join(result)

    def __str__(self):
        if self.seq is None:
            return ""
        return "".join(self.seq) + "\n" + self.pp()

    def normalized_seq(self):
        def normalize(c):
            if c in ['A','C','G','U']:
                return c
            elif c in ['a','c','g','u']:
                return c.upper()
            else:
                return 'N' # for all non ACGU
        return "".join(map(normalize, self.seq))

    @staticmethod
    def load(filename, verbose=False): # return an RNA
        seq = []
        pairs = []
        preamble = True
        num_sentence = "number of molecules: "
        first_warn = True
        for index, line in enumerate(open(filename)):
            items = line.split()  # 1 G 71 or 1 G 0 2 71 1
            if preamble:
                x = line.find(num_sentence)
                if x >= 0:
                    num_molecules = int(line[x + len(num_sentence):].split()[0]) # first number
                    if num_molecules > 1:
                        print >> logs, "error: number of molecules: %d" % num_molecules
                        return None
                if items[0] != "1":
                    continue
                else:
                    preamble = False # start of the real part
            if len(items) == 3: # BPSEQ FORMAT
                i, char, j = items 
            elif len(items) == 6: # CT FORMAT
                i, char, _, _, j, _ = items                
            else:
                assert False, items
            char = char.upper()
            if char not in ['A','C','G','U']:
                print >> logs, "error: invalid char %s" % char
                return None
            i = int(i) - 1
            j = int(j) - 1
            seq.append(char)
            pairs.append(j)
            if j != -1 and j < i and (seq[j]+seq[i]) not in allowed_pairs:
                if first_warn:
                    print >> logs, "warning: non-canonical pair(s)",
                    first_warn = False
                print >> logs, "%s (%d-%d)" % (seq[j]+seq[i], j, i),
                #return None

        return RNA.cleanup(RNA(seq, pairs, filename), verbose)

    @staticmethod
    def cleanup(rna, verbose=False):
        """remove illegal links"""
        for i, (c, j) in enumerate(zip(rna.seq, rna.pairs)):
            if j == -1 or j > i:
                pass
            else:
                pair = "".join(sorted(rna.seq[j]+c))
                #print pair
                if not pair in allowed_pairs:
                    if verbose:
                        print >> sys.stderr, "illegal pair %s(%d) %s(%d)" % (rna.seq[j], j, c, i)
                    rna.pairs[i] = -1
                    rna.pairs[j] = -1
        return rna

    @staticmethod
    def loadseqs(path, verbose=False): # only load .ct's
        import os
        errors = 0
        for filename in sorted(os.listdir(path)):
            if filename[-3:] != ".ct" or filename.find("domain") >= 0:
                continue
            #if verbose:
            print >> sys.stderr, filename,
            rna = RNA.load(os.path.join(path, filename), verbose)
            if rna is not None:
                print >> sys.stderr, "OK"
                yield rna
            else:
                errors += 1

    @staticmethod
    def loadpp(bases, pp):
        """load from pp string"""
        bases = bases.strip()
        pp = pp.strip()
        # if len(bases) != len(pp):
        #     print len(bases), len(pp)
        assert len(bases) == len(pp)
        stack = [[] for x in range(3)]
        seq = []
        pairs = []
        for i, (c, j) in enumerate(zip(bases, pp)):
            seq.append(c)
            if j == ".":
                pairs.append(-1)
            elif j == "(":
                stack[0].append(i)
                pairs.append(-1)  # placeholder
            elif j == ")":
                link = stack[0].pop()
                pairs[link] = i
                pairs.append(link)
            elif j == "[":
                stack[1].append(i)
                pairs.append(-1)  # placeholder
            elif j == "]":
                link = stack[1].pop()
                pairs[link] = i
                pairs.append(link)
            elif j == "{":
                stack[2].append(i)
                pairs.append(-1)  # placeholder
            elif j == "}":
                link = stack[2].pop()
                pairs[link] = i
                pairs.append(link)
            else:
                assert False, "wrong symbol in pp string"

        # assert len(stack) == 0
        return RNA(seq, pairs)

    def __cmp__(self, other):
        return 1 if len(self) > len(other) else -1 #shorter to longer

class FScore(object):

    def __init__(self, correct=0, predcount=0, goldcount=0):
        self.correct = correct        # correct brackets
        self.predcount = predcount    # total predicted brackets
        self.goldcount = goldcount    # total gold brackets

    def precision(self):
        if self.predcount > 0:
            return (100.0 * self.correct) / self.predcount
        else:
            return 0.0

    def recall(self):
        if self.goldcount > 0:
            return (100.0 * self.correct) / self.goldcount
        else:
            return 0.0

    def fscore(self):
        precision = self.precision()
        recall = self.recall()
        if (precision + recall) > 0:
            return (2 * precision * recall) / (precision + recall)
        else:
            return 0.0

    def __str__(self):
        precision = self.precision()
        recall    = self.recall()
        fscore = self.fscore()
        return '(P = {:0.2f}, R = {:0.2f}, F= {:0.2f} )'.format(
            precision,
            recall,
            fscore,
        )

    def __iadd__(self, other):
        self.correct += other.correct
        self.predcount += other.predcount
        self.goldcount += other.goldcount
        return self

    def __add__(self, other):
        return Fmeasure(self.correct + other.correct, 
                        self.predcount + other.predcount, 
                        self.goldcount + other.goldcount)

    def __cmp__(self, other):
        return cmp(self.fscore(), other.fscore())

def evaluate(gold, pred):
    assert len(gold.seq) == len(pred.seq)
    if gold.seq != pred.seq:
        newseq = gold.seq
        for i, x in enumerate(newseq):
            if x not in 'AUCGaucg':
                newseq[i] = 'n'
        assert newseq == pred.seq, "\n%s\n%s\n%s" % (gold, "".join(newseq), pred) # same seq
        print >> logs, "invalid char in gold"
    correct = predcount = goldcount = 0
    for i, (g, p) in enumerate(zip(gold.pairs, pred.pairs)):
        if g != -1:
            goldcount += 1
        if p != -1:
            predcount += 1
            if p == g:
                correct += 1
    # N.B.: should divide by 2, but doesn't change result
    return FScore(correct, predcount, goldcount)


if __name__ == '__main__':

    import itertools
    import sys
    logs = sys.stderr
    GOLD = sys.argv[1] #'../data-comparna/'
    if len(sys.argv) < 3:
        for i, rna in enumerate((RNA.loadseqs(GOLD))):
            #print >> logs, i+1, len(rna), rna.filename
            print i+1, len(rna), rna.filename.split("/")[-1] # no path
            print rna
    elif len(sys.argv) == 3:
        data = zip(RNA.loadseqs(GOLD), RNA.loadseqs(TEST))
        total_len = 0
        total_f = FScore()
        for rna, rna2 in sorted(data, lambda x,y: len(x[0])-len(y[0])):
            print rna
            print rna2
            # rna2 = RNA(rna.seq[:], rna.pairs[:])
            # rna2.pairs[0] = len(rna) - 1
            f = evaluate(rna, rna2)
            print f.correct
            print f.predcount
            print f.goldcount
            print len(rna)
            print "len=", len(rna), f
            total_f += f
            total_len += len(rna)

        print "#seq %d, avg_len %.1f, %s" % (len(data), total_len / len(data), total_f)

    else: # ./rna.py small-seqs.txt small-brackets.txt test-brackets.txt
        import itertools
        total_f = FScore()
        total_len = 0
        num = 0
        for seq, bracket1, bracket2 in itertools.izip(*map(open, sys.argv[1:])):
            rna1 = RNA.loadpp(seq, bracket1)
            rna2 = RNA.loadpp(seq, bracket2)
            f = evaluate(rna1, rna2)
            print "len=", len(rna1), f
            total_f += f
            total_len += len(rna1)
            num += 1

        print "#seq %d, avg_len %.1f, %s" % (num, total_len / num, total_f)


        # TEST = sys.argv[2] #'output-comparna/'
        # #itertools.izip(RNA.loadseqs(GOLD), RNA.loadseqs(TEST)):
        # data = zip(RNA.loadseqs(GOLD), RNA.loadseqs(TEST))
        # total_len = 0
        # total_f = FScore()
        # for rna, rna2 in sorted(data, lambda x,y: len(x[0])-len(y[0])):
        #     print rna
        #     print rna2
        #     # rna2 = RNA(rna.seq[:], rna.pairs[:])
        #     # rna2.pairs[0] = len(rna) - 1
        #     f = FScore.evaluate(rna, rna2)
        #     print f.correct
        #     print f.predcount
        #     print f.goldcount
        #     print len(rna)
        #     print "len=", len(rna), f
        #     total_f += f
        #     total_len += len(rna)

        # print "#seq %d, avg_len %.1f, %s" % (len(data), total_len / len(data), total_f)
