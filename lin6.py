#!/usr/bin/env python

''' based on lin4.py, with adding internal explicit/symmetry/asymmetry features'''

from __future__ import division

import sys
from collections import defaultdict, Counter
import math
import time

from rna import allowed_pairs, RNA, FScore

shapes = ["P","B1","B2","I","M1","M2", "C"]
precision = 1e-6

EXPLICIT_MAX_LEN = 4
SINGLE_MIN_LEN = 0
SINGLE_MAX_LEN = 30

HAIRPIN_MIN_LEN = 0
BULGE_MIN_LEN = 1
INTERNAL_MIN_LEN = 2
SYMMETRIC_MIN_LEN = 1
ASYMMETRY_MIN_LEN = 1

HAIRPIN_MAX_LEN = 30
BULGE_MAX_LEN = SINGLE_MAX_LEN
INTERNAL_MAX_LEN = SINGLE_MAX_LEN
SYMMETRIC_MAX_LEN = 15
ASYMMETRY_MAX_LEN = 28

MAX_LEN = max(HAIRPIN_MAX_LEN, BULGE_MAX_LEN, INTERNAL_MAX_LEN)
MAXINT = 100000000
PROFILE = False

features_atleast = [
    "hairpin_length",
    "bulge_length",
    "internal_length",
    "internal_symmetric_length",
    "internal_asymmetry"
]

bounds = [
    (HAIRPIN_MIN_LEN, HAIRPIN_MAX_LEN),
    (BULGE_MIN_LEN, BULGE_MAX_LEN),
    (INTERNAL_MIN_LEN, INTERNAL_MAX_LEN),
    (SYMMETRIC_MIN_LEN, SYMMETRIC_MAX_LEN),
    (ASYMMETRY_MIN_LEN, ASYMMETRY_MAX_LEN),
]


import gflags as flags
FLAGS=flags.FLAGS

def myadd(x, y):
    return min(2, x+y)

def mydot(feats, weightdict):
    return sum(weightdict.get(key, 0) for key in feats)

def printfv(fv, ifvalue=False): # fv is a list
    #print "before"
    #print mydot(fv, State.fw)
    fv = Counter(fv)

    #print "fv=", sorted((x, fv[x]) for x in fv)# if x in State.fw])
    myfv = Counter()
    for key in fv:
        ifcopied = False
        for index,fea in enumerate(features_atleast):
            _min, _max = bounds[index]
            if key.startswith(fea+"_exact"):
                length = int(key.rsplit("_", 1)[1])
                count = fv[key]
                for i in xrange(_min, length+1):
                    myfv[fea+"_at_least_%d" % i] += count
                ifcopied = True
                break
        if not ifcopied:
            myfv[key] += fv[key]

    # tmp = [(x, myfv[x]) for x in myfv if x in State.fw]
    tmp = [(x, myfv[x]) for x in myfv]
    #print "FV=", sorted(tmp)
    if ifvalue:
        for (x,y) in sorted(tmp):
            print x, y, State.fw[x]*y
        print "sum value: %f" % sum([State.fw[x]*y for (x,y) in tmp])
    else:
        for (x,y) in sorted(tmp):
            print x, y
    return

class State(object):
    __slots__ = "i", "j", "l", "l1", "l2", "shape", "status", "score", "inside", "backptr", "leftptrs", "fv", "_hash", "_signature", "boundary_gaps", "heuristic"
    bases = []

    fw = defaultdict(float)
    
    heuristic_exact = [0] * (MAX_LEN+1)
    heuristic_at_least = [0] * (MAX_LEN+1)

    def __init__(self, shape, i, j, l=0, l1=0, l2=0, status=0): #, boundary_gaps=(False, False), score=0, inside=0, backptr=(None, None, None)): # backptr = manner, parent1, parent2
        self.i = i
        self.j = j
        self.l = l
        self.l1, self.l2 = l1, l2
        self.shape = shape  # External, Multi, Other, Paired
        self.status = status
        self.boundary_gaps = (False, False)
        # self.score = score
        # self.inside = inside
        # self.backptr = backptr
        # self.leftptrs = []
        self.fv = []  # delta feats list, not dict
        self._hash = None
        self._signature = None
        self.heuristic = 0        

    def __eq__(self, other): # everything except score & backptr
        return self.signature() == other.signature()

    def signature(self):
         if self._signature is None:
             if self.shape == "E":
                 self._signature = self.shape, self.j
             elif self.shape[0] in ["P", "M"]: # M1 or M2
                 self._signature = self.shape, self.i, self.j
             else: #O
                 # if self.status == 0:
                 #     self._signature = self.shape, self.i, self.j, self.l, self.status
                 # else: #status=1
                 self._signature = self.shape, self.i, self.j, self.l, self.l1, self.l2, self.status, self.boundary_gaps
         return self._signature

    def __hash__(self):
        if self._hash is None:
            self._hash = hash(self.signature())
        return self._hash

    helix_closing = lambda _,i,j: "helix_closing_%s%s" % (_.bases[i], _.bases[j])
    bulge = lambda _, l: "bulge_length_exact_%d" % min(BULGE_MAX_LEN, l)
    bulge_at_least = lambda _, l: "bulge_length_at_least_%d" % min(BULGE_MAX_LEN, l)
    internal = lambda _, l: "internal_length_exact_%d" % min(INTERNAL_MAX_LEN, l)
    internal_at_least = lambda _, l: "internal_length_at_least_%d" % l

    internal_explicit = lambda _, l1, l2: "internal_explicit_%d_%d" % (min(l1, l2), max(l1, l2))
    internal_sym = lambda _, l1: "internal_symmetric_length_exact_%d" % (min(SYMMETRIC_MAX_LEN, l1))
    internal_asym = lambda _, l1, l2: "internal_asymmetry_exact_%d" % (min(ASYMMETRY_MAX_LEN, l1-l2 if l1 > l2 else l2-l1))

    hairpin = lambda _, l: "hairpin_length_exact_%d" % min(HAIRPIN_MAX_LEN, l)
    hairpin_at_least = lambda _, l: "hairpin_length_at_least_%d" % l
    terminal_mismatch = lambda _, i, j: "terminal_mismatch_%s%s%s%s" % \
                                      (_.bases[i], _.bases[j],
                                       _.bases[i+1], _.bases[j-1])
    internal_1x1 = lambda _,i,j: "internal_1x1_nucleotides_%s" % "".join(sorted(_.bases[i]+_.bases[j]))
    bulge_0x1 = lambda _,j: "bulge_0x1_nucleotides_%s" % _.bases[j]
    dangle_left = lambda _,i,j: "dangle_left_%s%s%s" % (_.bases[j], _.bases[i], _.bases[j+1])
    dangle_right = lambda _,i,j: "dangle_right_%s%s%s" % (_.bases[j], _.bases[i], _.bases[i-1])

    external_unpaired = lambda _: "external_unpaired"
    multi_unpaired = lambda _: "multi_unpaired"

    def helix_stacking(_, i, j): # TODO: bug
        stuff = (_.bases[i], _.bases[j], _.bases[i+1], _.bases[j-1])
        a = "helix_stacking_%s%s%s%s" % stuff
        b = "helix_stacking_%s%s%s%s" % tuple(reversed(stuff))
        if a == b:
            return [a]
        return [a, b]

    def skip(self):
        ''' j will be unpaired '''
        state = State(self.shape, self.i, self.j+1, self.l+1, self.l1, self.l2, self.status)
        fv = []
        sc = 0 # avoid unnecessary mydot
        if self.shape == "E":
            fv.append(self.external_unpaired())
            sc = mydot(fv, State.fw)
        elif self.shape == "O":
            if self.status == 0:
                state.boundary_gaps = (True, self.boundary_gaps[1])
                state.l1 += 1
            else: # status == 1
                state.boundary_gaps = (self.boundary_gaps[0], True)
                state.l2 += 1
                if self.l == 0: #boundary_gaps == (False, False): 
                    # and self.l == 0: # ((...). right bulge: don't double-count
                    # (...(...). need to helix_close (right after reduce)
                    j = self.j-1
                    i = self.i + self.l + 1 # (...(
                    fv.append(self.helix_closing(j,i)) # right-to-left
                    fv.append(self.terminal_mismatch(j,i))
                    sc = mydot(fv, State.fw)
        else: #M1/M2
            fv.append("multi_unpaired")
            sc = mydot(fv, State.fw)
            
        state.fv = fv
        if self.shape == "O":
            state.heuristic = self.heuristic + State.heuristic_at_least[min(MAX_LEN, state.l)] #  prepay State.fw[self.bulge_at_least(state.l)] ##
        state.score = self.score + sc #+ state.heuristic
        state.inside = self.inside + sc
        state.backptr = ("skip", self, None)
        state.leftptrs = self.leftptrs
        return state

    def push(self): # shift
        ''' j will be (, forming "O" shape '''
        if self.shape == "O" and self.status == 1:
            return None # should become M
        state = State("O", self.j, self.j+1, l=0, l1=0, l2=0, status=0)
        fv = []
        state.fv = fv
        state.score = self.score #+ mydot(fv, fw) # shift score = 0
        state.inside = 0
        state.backptr = ("push", None, None) # shifted state has leftptr but no backptr
        state.leftptrs = [self]
        state.heuristic = self.heuristic + State.heuristic_at_least[0] # prepay hairpin
        return state

    def link(self):
        ''' i and j will be paired, forming "P" shape (from O0/O1 or M2)'''
        i, j = self.i, self.j
        bases = State.bases
        pair = "".join(sorted(bases[i]+bases[j])) # sorted pair
        if pair in allowed_pairs:
            basepairkey = "base_pair_" + pair
            state = State("P", i, j) # not advancing j
            fv = [basepairkey]
            if self.shape == "O":
                if self.status == 0: # no link b/w i--j => hairpin
                    fv += [self.hairpin(j-i-1),
                           self.helix_closing(i, j),
                           self.terminal_mismatch(i,j)]
                else: # 1 link b/w i--j: stacking, bulge, or internal
                    if self.l == 0: #boundary_gaps == (False, False): # stacking
                        fv += self.helix_stacking(i,j)
                    else: # bulge or internal
                        fv.append(self.terminal_mismatch(i,j))
                        fv.append(self.helix_closing(i,j))
                        if self.boundary_gaps == (True, True): # internal
                            fv.append(self.internal(self.l))
                            if self.l == 2: # exactly one gap on each side (.(...).)
                                fv.append(self.internal_1x1(i+1, j-1))
                            if 1<=self.l1<=4 and 1<=self.l2<=4:
                                fv.append(self.internal_explicit(self.l1, self.l2))
                            if self.l1 == self.l2:
                                fv.append(self.internal_sym(self.l1))
                            if self.l1 != self.l2:
                                fv.append(self.internal_asym(self.l1, self.l2))
                        else: # bulge
                            fv.append(self.bulge(self.l))
                            if self.l == 1:
                                fv.append(self.bulge_0x1(i+1 if self.boundary_gaps[0] else j-1))
            else: # M2
                #fv.append(self.terminal_mismatch(i,j)) #N.B. multi has no terminal_mismatch
                fv.append(self.helix_closing(i,j))
                fv.append("multi_paired") # outermost (...)
                # multiloop inside dangle (inside outermost (..))
                if j < len(State.bases)-1: 
                    fv.append(self.dangle_left(j, i)) # inside dangle is reversed!
                if i > 0:
                    fv.append(self.dangle_right(j,i))

            state.fv = fv
            sc = mydot(fv, State.fw)
            state.score = self.score + sc
            state.inside = self.inside + sc
            state.backptr = ("link", self, None)
            state.leftptrs = self.leftptrs
            return state
        else:
            return None # not allowed pair

    def reduce(self):
        ''' combine self (P) with left states (E, M, O) to yield many new states '''
        bases = State.bases
        for leftstate in self.leftptrs:
            i, j, k = self.i, self.j, leftstate.i
            shape = leftstate.shape
            state = State(shape, k, j+1)
            fv = []
            sc = 0 # avoid unnecessary mydot
            if shape == "E":
                fv.append(self.helix_closing(j,i)) # right-to-left
                fv.append("external_paired")
                if j < len(bases)-1:
                    fv.append(self.dangle_left(i, j))
                if i > 0:
                    fv.append(self.dangle_right(i,j))
                sc = mydot(fv, State.fw)            
            elif shape == "O":
                if i > 0 and j < len(bases) - 1:
                    if leftstate.boundary_gaps[0]: # (.(..) left bulge
                        fv.append(self.terminal_mismatch(j,i))
                        fv.append(self.helix_closing(j,i)) # right-to-left
                        sc = mydot(fv, State.fw)
            else: # shape[0] == "M":
                state.shape = "M2" # M1/2 + P = M2;;; O0 + P = O1 = M1
                if j < len(bases)-1:
                    fv.append(self.dangle_left(i, j))
                if i > 0:
                    fv.append(self.dangle_right(i,j))
                fv.append("multi_paired")
                #fv.append(self.terminal_mismatch(j,i))
                fv.append(self.helix_closing(j,i)) # right-to-left
                sc = mydot(fv, State.fw)
            state.fv = fv
            state.score = leftstate.score + self.inside + sc
            state.inside = leftstate.inside + self.inside + sc
            state.backptr = ("reduce", leftstate, self)
            state.leftptrs = leftstate.leftptrs
            state.boundary_gaps = (leftstate.boundary_gaps[0], False) #....(...)
            state.l1 = leftstate.l1
            state.l2 = 0
            state.l = leftstate.l
            state.status = 1 # TODO (O1 or M2)
            state.heuristic = leftstate.heuristic
            yield state
    
    def O1toM1(self): # (....(...)  collaps l, remove heuristics
        state = State("M1", self.i, self.j)
        fv = ["multi_unpaired"] * self.l + ["multi_base", "multi_paired"]
        manner, leftstate, rightstate = self.backptr
        i, j = rightstate.i, rightstate.j
        if j < len(State.bases)-1:
            fv.append(self.dangle_left(i, j))
        if i > 0:
            fv.append(self.dangle_right(i,j))
        fv.append(self.helix_closing(j,i)) # right-to-left N.B.

        sc = mydot(fv, State.fw)
        state.score = leftstate.score + rightstate.inside + sc
        state.inside = leftstate.inside + rightstate.inside + sc
        state.backptr = self.backptr
        state.leftptrs = self.leftptrs
        state.heuristic = self.leftptrs[0].heuristic # N.B. correct?
        state.fv = fv
        return state # M1        

    def __cmp__(self, other): # larger is better # TODO: BUG
        return -1 if self.score > other.score else 1 # max

    def pp(self): 
        ''' returns (struct, feats_list) pair '''

        i, j = self.i, self.j
        if i == -1 and j == 0: # initial
            return "", []
        manner, parent1, parent2 = self.backptr
        if manner == "push": # DON'T DO RECURSION FOR SHIFT!!
            return "(", self.fv
        s, fv = parent1.pp()
        newfv = self.fv + fv
        if manner == "skip":
            return "%s." % s, newfv
        elif manner == "link":
            return "%s)" % s, newfv
        elif manner == "reduce":
            s2, fv2 = parent2.pp()
            return "%s%s" % (s, s2), newfv + fv2
        else:
            assert False, "unknown manner: %s" % manner

    def myid(self):
        return format(id(self), "#04x")[-5:]

    def pp_backptr(self):
        manner, a, b = self.backptr
        if manner is None:
            return "-"
        elif manner == "reduce":
            return "(%s, %s, %s)" % (manner, a.myid(), b.myid())
        elif a is None: # push or INIT
            return "(%s)" % manner
        else:
            return "(%s, %s)" % (manner, a.myid())

    def __str__(self):
        s = "%s %s (%d, %d) tot=%.2f sc=%.2f in=%.2f %s" % (self.myid(),
                                                            self.shape, self.i, self.j, 
                                                            self.score+self.heuristic, self.score, self.inside,
                                                            self.pp_backptr())
        if self.shape == "P":
            s = s.replace("(", "[").replace(")", "]")
        if self.shape in ["O", "M"]:
            s += " st=%d" % self.status
        if self.shape == "O":
            s += " bgaps=%s l=%d l1=%d l2=%d" % (map(int, self.boundary_gaps), self.l, self.l1, self.l2)
        if self.backptr[0] == "push":
            #s += " lptrs=[%s]" % ",".join(x.myid() for x in self.leftptrs)
            s += " #lptrs=%d" % len(self.leftptrs)
        return s + " " + self.pp()[0] # + " fv:" + str(self.fv)

    def str2(self): # no recursive of pp
        return "(%d, %d, %d, %s) %.1f %s" % (self.i, self.j, self.l, self.shape, self.score, self.backptr[0])

    @staticmethod
    def init_state():
        state = State("E", -1, 0)
        state.score = state.inside = 0
        state.backptr = ("INIT", None, None)
        state.leftptrs = []
        return state

    __repr__ = __str__

def solve(bases):

    def tryadd(cands, state):
        if state is not None:
            cands.append(state)
            num_states[state.shape[0]] += 1
    
    starttime = time.time()

    State.bases = bases
    n = len(bases)
    beam = []
    init = State.init_state()
    beam.append({init: init})

    if FLAGS.debug:
        print -1
        print init
    num_states = {"O": 0, "E": 1, "P": 0, "M": 0}
    uniq_states = {"O": 0, "E": 1, "P": 0, "M": 0}
    for i in xrange(n):
        cands = []
        pairs = []
        for state in beam[-1]:
            tryadd(cands, state.skip())
            tryadd(cands, state.push())
            if state.shape == "O" or state.shape == "M2": # special pool for Ps
                tryadd(pairs, state.link())

        pairs.sort(key=lambda x: x.score+x.heuristic, reverse=True) #max
        already = set()
        for state in pairs:
            if state not in already: # throw away equivalent but worse items
                already.add(state)
                for newstate in state.reduce():
                    tryadd(cands, newstate)
                    if newstate.shape == "O": # O1(....(....) => M1
                        tryadd(cands, newstate.O1toM1())

        cands.sort(key=lambda x: x.score+x.heuristic, reverse=True) #max

        if FLAGS.uniq:
            uniq_cands = {"E": set(), "O": set(), "P": set(), "M": set()}
            uniq_states["P"] += len(already)
            for state in cands:
                shape = state.shape[0]
                if state not in uniq_cands[shape]:
                    uniq_states[shape] += 1
                    uniq_cands[shape].add(state)

        if not FLAGS.infbeam: # infinite beam
            cands = cands[:FLAGS.b]
        #beam.append(cands[:FLAGS.b]) 
        beam.append({})
        curr_beam = beam[-1]
        for state in cands:
            if state not in curr_beam:
                curr_beam[state] = state
            else:
                if state.backptr[0] == "push": # shifted (pushed) state
                    curr_beam[state].leftptrs.append(state.leftptrs[0]) # the one in beam is better; mergewith
                # else: # backptr merge; they must have exactly the same leftptrs
                #     pass #assert len(beam[-1][state].leftptrs) == len(state.leftptrs)

        if FLAGS.debug:
            print i, " -----  ****   ==================== ****-------"
            for j, x in enumerate(sorted(beam[-1],
                                cmp=lambda x,y: -1 if x.score+x.heuristic > y.score+y.heuristic else 1)):
                print i, j, ":", str(x)
            print "---"
            print "\n".join(map(str, sorted(filter(lambda x: x is not None, already),
                                            cmp=lambda x,y: -1 if x.score+x.heuristic > y.score+y.heuristic else 1)))

        if FLAGS.free:
            beam[-2] = {} # free useless states

    best_state = None
    for state in beam[-1]:
        if state.shape in ["E"]:
            if best_state is None or state.score > best_state.score:
                best_state = state
    if best_state is None:
        print "FAILED"
        print "Viteri score: -10000"
        return

    s = "Viterbi score: %8.5f" % best_state.score
    #if s[-1:] == "0":
    #    s = s[:-1]  # delete .xxx0
    print s,
    t = time.time() - starttime
    #total_beam_states = sum(len(x) for x in beam)
    total_states = sum(num_states.values())
    print "Time: %.5f seconds  len: %d  all: %d" % (t, n, total_states),
    print "(%s)" % ", ".join("%s: %d" % (shape, num_states[shape]) for shape in sorted(num_states)),
    if FLAGS.uniq:
        print "uniq: %d (%s)" % (sum(uniq_states.values()), ", ".join("%s: %d" % (shape, uniq_states[shape]) for shape in sorted(num_states)))
    else:
        print

    if FLAGS.DEBUG:
        print "".join(bases)
        print "".join(str(x%10) for x in xrange(len(bases)))
        print "".join(str(x//10) for x in xrange(len(bases)))
    if FLAGS.debug:
        print best_state        
    struct, fv = best_state.pp()
    if FLAGS.feats:
        printfv(fv, FLAGS.values)

    print "".join(bases)
    print ">structure"
    print struct
    print
    sys.stdout.flush()
    return struct, best_state.score, total_states, t

def main():
    ## move fw to global
    ## fw = {}
    fw = defaultdict(float)

    for line in open(FLAGS.params): #contrafold.params.complementary"):
        key, value = line.split()
        fw[key] = float(value)

    for index, fea in enumerate(features_atleast):
        _min, _max = bounds[index]
        fw[fea+"_exact_%d" % _min] = fw[fea+"_at_least_%d" % _min]
        for i in xrange(_min+1, _max+1):
            fw[fea+"_exact_%d" % i] = fw[fea+"_exact_%d" % (i-1)] + fw[fea+"_at_least_%d" % i]

    for i in xrange(0, MAX_LEN+1):
        State.heuristic_at_least[i] = min(fw["hairpin_length_at_least_%d" % i], 
                                          fw["bulge_length_at_least_%d" % i], 
                                          fw["internal_length_at_least_%d" % i]) # harshest heuristics

        if FLAGS.debug:
            print i, fw["hairpin_length_exact_%d" % i], fw["bulge_length_exact_%d" % i], fw["internal_length_exact_%d" % i], "\t", State.heuristic_exact[i], State.heuristic_at_least[i]

    
    State.fw = fw

    # for key, value in sorted(fw.iteritems()):
    #     print key, value

    if FLAGS.refs:
        reffile = open(FLAGS.refs)
    total_f = FScore()
    num = 0
    total_len = total_states = total_score = total_time = 0
    for line in sys.stdin:
        line = line.strip() # UACG....
        struct, score, states, time = solve(line)
        num += 1
        total_score += score
        total_states += states
        total_time += time
        total_len += len(line)
        if FLAGS.refs:
            refline = reffile.readline().strip()
            rna1 = RNA.loadpp(line, refline)
            rna2 = RNA.loadpp(line, struct)
            f = FScore.evaluate(rna1, rna2)
            print "eval: len=", len(rna1), f
            print
            total_f += f

    print "num_seq: %d, avg_len: %.1f" % (num, 
                                          0 if num == 0 else total_len / num), \
        "beam: %d, avg_score: %.4f, avg_time: %.3f, avg_states: %.1f" % (FLAGS.b, 
                                                                         0 if num == 0 else total_score / num, 
                                                                         0 if num == 0 else total_time / num,
                                                                         0 if num == 0 else total_states / num),
    if FLAGS.refs:
        print total_f
    else:
        print

if __name__ == "__main__":

    flags.DEFINE_boolean("profile", False, "profile")
    flags.DEFINE_boolean("debug", False, "debug (print chart)", short_name="D")
    flags.DEFINE_boolean("feats", False, "print features of 1-best parse")
    flags.DEFINE_boolean("values", False, "print feature values of 1-best parse")
    flags.DEFINE_string("params", "params.my", "parameters file")
    flags.DEFINE_integer("beam", 1, "beam size", short_name="b")
    flags.DEFINE_boolean("realparams", False, "use contrafold.params.complementary")
    flags.DEFINE_boolean("infbeam", False, "infinite beam (no pruning)")
    flags.DEFINE_boolean("uniq", False, "also print uniq states counts")
    flags.DEFINE_string("refs", "", "reference brackets file", short_name="r")

    flags.DEFINE_boolean("DEBUG", False, "DEBUG pp()")
    flags.DEFINE_boolean("free", False, "free extinct states in beam")

    argv = FLAGS(sys.argv)
    if FLAGS.realparams:
        FLAGS.params = "contrafold.params.complementary"
    if FLAGS.values:
        FLAGS.feats = True
    if FLAGS.b == 0:
        FLAGS.infbeam = True

    if FLAGS.profile:
        import cProfile as profile
        profile.run("main()", "cky.profile")
        import pstats
        p = pstats.Stats("cky.profile")
        p.sort_stats("time").print_stats(60)
    else:
        main()
