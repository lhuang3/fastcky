''' filter out pseudoknotted and length-mismatched sequences 
cat ../lengths.txt.sorted | python2.7 filterpseudo.py > biginput.noknots 2> biginput.wrong
'''

import sys
logs = sys.stderr
from rna import RNA
file2 = open("biginput.corrected")
for line in sys.stdin: # "../length.txt.sorted
    length, line = line.split()
    length = int(length)
    seq = open("../contrafold-code/data-comparna-paren/%s.bpseq.paren" % line).readline().strip() # ((....))
    seq2 = file2.readline().strip() #ACGUs
    s = RNA.load("../contrafold-code/data-comparna/%s.bpseq" % line)    
    if length == len(seq) == len(seq2) == len(s):
        if seq.find("[") < 0:
            print s.normalized_seq()
            print s.pp()
    else:
        print >> logs, "bad", (length, len(seq), len(seq2), len(s), line)

