#!/usr/bin/env python

import sys, os
from string import maketrans

DEBUG = False

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def testpattern(p):
    ## condon 2004, Classifying RNA pseudoknotted structures
    ## section 4.1 & 4.2
    ## three modes: RE99, RE00(RE), DP, LP

    def tryremove(i):
        if DEBUG:
            print "tryremove", i

        v,a,l,r = p[i]
        if l is not None:
            p[l] = (p[l][0],p[l][1],p[l][2],r)
        if r is not None:
            p[r] = (p[r][0],p[r][1],l,p[r][3])
        del p[i]
        if i in lefts:
            lefts.remove(i)
        elif i in rights:
            rights.remove(i)
        return

    if len(p) == 0:
        return True


    lefts = []
    rights = p.keys()[1:]
    tao = p.keys()[0]

    while tao is not None:

        if DEBUG:
            print p
            print lefts
            print tao
            print rights
            print "------"

        v,a,l,r = p[tao]
        if l is not None and r is not None and p[l][0] == p[r][0]: ## delta is directly adjacent to tao
            if mode == "RE" or mode == "RE00":
                if DEBUG:
                    print "#rule1", l, r
                tryremove(l)
                tryremove(r)
                continue

        if mode == "DP" or mode == "LP": ## delta is directly nested with respect to tao
            av, aa, al, ar = p[a]
            assert (av == v and aa == tao)
            m,m2 = None, None
            if a > tao:
                if (r is not None and al is not None and p[r][0] == p[al][0] and len(set([r,al,tao,a])) == 4):
                    m,m2 = r,al
            else:
                if (l is not None and ar is not None and p[l][0] == p[ar][0] and len(set([l,ar,tao,a])) == 4):
                    m,m2 = l,ar
            if m is not None:
                if DEBUG:
                    print "#rule1_nested", m, m2, tao, a
                tryremove(m)
                tryremove(m2)
                continue

        if a == l or a == r: ## tao is self adjacent
            if mode in ["RE", "RE00", "RE99", "DP", "LP"]: ## actually all modes
                if DEBUG:
                    print "#rule2", tao, a
                tryremove(tao)
                tryremove(a)
                if len(p) == 0:
                    break
                if len(lefts) != 0:
                    tao = lefts[-1]
                    lefts = lefts[:-1]
                else:
                    tao = rights[0]
                    rights = rights[1:]
                continue

        if mode.startswith("RE"): ## tao is directly adjacent to some delta
            av, aa, al, ar = p[a]
            assert (av == v and aa == tao)
            m,m2 = None, None 
            if (l is not None and al is not None and p[l][0] == p[al][0] and len(set([l,al,tao,a])) == 4):
                m,m2 = l,al
            elif (l is not None and ar is not None and p[l][0] == p[ar][0] and len(set([l,ar,tao,a])) == 4):
                m,m2 = l,ar
            elif (r is not None and al is not None and p[r][0] == p[al][0] and len(set([r,al,tao,a])) == 4):
                m,m2 = r,al
            elif (r is not None and ar is not None and p[r][0] == p[ar][0] and len(set([r,ar,tao,a])) == 4):
                m,m2 = r,ar

            if m is not None:
                if DEBUG:
                    print "#rule3", m, m2, tao, a
                tryremove(m)
                tryremove(m2)
                tryremove(tao)
                tryremove(a)
                if len(p) == 0:
                    break
                if len(lefts) != 0:
                    tao = lefts[-1]
                    lefts = lefts[:-1]
                else:
                    tao = rights[0]
                    rights = rights[1:]
                continue

        if len(lefts) >= 3 and a == lefts[-2] and p[lefts[-1]][0] == p[lefts[-3]][0]: ## delta-tao-delta is suffix of lefts
            if mode == "DP":
                if DEBUG:
                    print "#rule4", m, m2, tao, a
                m,m2 = lefts[-3], lefts[-1]
                tryremove(m)
                tryremove(m2)
                tryremove(a)
                tryremove(tao)
                if len(p) == 0:
                    break
                if len(lefts) != 0:
                    tao = lefts[-1]
                    lefts = lefts[:-1]
                else:
                    tao = rights[0]
                    rights = rights[1:]
                continue

        if DEBUG:
            print "#no rule: move tao"
        if rights == []:
            tao = None
        else:
            lefts.append(tao)
            tao = rights[0]
            rights = rights[1:]

    if mode == "LP":
        if len(p) == 0 or len(p) == 4:
            return True
        else:
            return False
    if len(p) == 0:
        return True
    else:
        return False


lbs = ["(","[","{"]
rbs = [")","]","}"]
def solve(myfile):
    parens = []
    links = []

    ## parse links from structure
    if filemode == "seq":
        newline = ""
        for line in open(myfile):
            tmp = line.strip().split()
            if len(tmp) < 3:
                continue
            if tmp[0] == "%":
                newline += tmp[2]
                stacks = [[],[],[]]
        for index, item in enumerate(newline):
            if item in lbs:
                stacks[lbs.index(item)].append(index)
            elif item in rbs:
                left = stacks[rbs.index(item)][-1]
                stacks[rbs.index(item)] = stacks[rbs.index(item)][:-1]
                links.append((left, index))
                parens += [(left,index),(index,left)]
    else:
        for line in open(myfile):
            tmp = line.strip().split()
            # x, y = int(tmp[0]), int(tmp[2])
            if filemode == "bpseq":
                x, y = int(tmp[0]), int(tmp[2])
            elif filemode == "ct":
                if len(tmp) < 6:
                    continue
                x, y = int(tmp[0]), int(tmp[4])
            else:
                print "input format error!"

            if x < y:
                links.append((x, y))
                parens += [(x,y),(y,x)]

    parens.sort()
    d = {}
    for i,(x,y) in enumerate(parens):
        d[x] = i
    p = {}
    l = len(parens)
    for i,(x,y) in enumerate(parens):
        p[i] = (-max(x,y), d[y], i-1 if i != 0 else None, i+1 if i < l-1 else None)

    return testpattern(p)


if __name__ == "__main__":
    mypath = sys.argv[1]
    mode = "RE" if len(sys.argv) <= 2 else sys.argv[2]
    files = [os.path.join(mypath, f) for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) and ".paren" not in f]
    a = []
    filemode = files[0].rsplit(".",1)[1]
    for f in files:
        a.append((file_len(f), f))
    a.sort()
    files = [x[1] for x in a]

    for f in files:
        print f, "Yes" if solve(f) else "No"
