import sys, re
lines = open(sys.argv[1]).readlines()

mysum = 0.0
cnt = 0
for line in lines:
    if "(" in line:
        line = re.sub('[()]', '', line)
        line = line.strip().split()[-1]
        a = float(line)
        mysum += a
        cnt += 1
print "numofseq: %d total: %.3f average: %.3f last: %.3f" % (cnt, mysum, mysum/cnt, a)
