import sys

filename = sys.argv[1]
mode = "MEA" if len(sys.argv) <= 2 else sys.argv[2]
lines = open(filename).readlines()
for index, line in enumerate(lines):
    if mode == "MEA":
        if "MEA" in line:
            print lines[index].strip().split()[0]
    elif mode == "Viterbi":
        if "(" in line and ")" in line:
            print lines[index].strip().split()[0]
    else:
        print "wrong mode!"
        sys.exit(0)

    # if "#len" in line:
    #     tmp = line.split()
    #     print tmp[1], tmp[3]
