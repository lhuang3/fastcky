//
// Created by Kai Zhao on 1/17/17.
//

#include "ckypar.h"

#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <vector>
#include <stack>
#include <tuple>
#include <cassert>

#include "utility.h"

#define CANDIDATE_LIST 1
#define DEBUG 0

#define NO_SHARP_TURN 1


using namespace std;

struct DecoderResult {
    string structure;
    double score;
    unsigned long numofstates;
    double time;
};


// taken from contrafold
// mapping the i, j index to an 1D array
//     0  1  2  3     <-- row 0
//        4  5  6     <-- row 1
//           7 [8]    <-- row 2
//              9     <-- row 3
// returns the offset of each row.
// e.g., get_row_offset(2, 4) = 5, so the (2,3) element in the matrix
// is offset[2] + 3 = 8th element in the array
vector<int> initialize_offset(int N) {
    vector<int> offset(N);
    for (int i=0; i<N; ++i) offset[i] = i * (N+N-i+1) / 2;
    return offset;
}


void get_parentheses(char* result,
                     unsigned seq_length,
                     vector<int>& offset,
                     vector<State>& bestP,
                     vector<State>& bestM1,
                     vector<State>& bestM2,
                     vector<State>& bestM,
                     vector<State>& bestC) {
    memset(result, '.', seq_length);
    result[seq_length] = 0;


    stack<tuple<int, int, State>> stk;

    stk.push(make_tuple(0, seq_length-1, bestC[seq_length-1]));

    while ( !stk.empty() ) {
        tuple<int, int, State> top = stk.top();
        int i = get<0>(top), j = get<1>(top);
        State& state = get<2>(top);
        stk.pop();

        int k, p, q;

        switch (state.manner) {
            case MANNER_HAIRPIN:
                result[i] = '(';
                result[j] = ')';
                break;
            case MANNER_SINGLE:
                result[i] = '(';
                result[j] = ')';
                p = i + state.trace.paddings.l1;
                q = j - state.trace.paddings.l2;
                stk.push(make_tuple(p, q, bestP[offset[p] + q]));
                break;
            case MANNER_HELIX:
                result[i] = '(';
                result[j] = ')';
                stk.push(make_tuple(i+1, j-1, bestP[offset[i+1]+j-1]));
                break;
            case MANNER_MULTI:
                result[i] = '(';
                result[j] = ')';
                stk.push(make_tuple(i+1, j-1, bestM2[offset[i+1]+j-1]));
                break;
            case MANNER_M1_eq_P:
                stk.push(make_tuple(i, j, bestP[offset[i]+j]));
                break;
            case MANNER_M1_eq_M1_plus_U:
                stk.push(make_tuple(i, j-1, bestM1[offset[i]+j-1]));
                break;
            case MANNER_M2_eq_M_plus_M1:
                k = state.trace.split;
                stk.push(make_tuple(i, k, bestM[offset[i]+k]));
                stk.push(make_tuple(k+1, j, bestM1[offset[k+1]+j]));
                break;
            case MANNER_M_eq_M1:
                stk.push(make_tuple(i, j, bestM1[offset[i]+j]));
                break;
            case MANNER_M_eq_M2:
                stk.push(make_tuple(i, j, bestM2[offset[i]+j]));
                break;
            case MANNER_M2_eq_U_plus_M2:
                stk.push(make_tuple(i+1, j, bestM2[offset[i+1]+j]));
                break;
            case MANNER_C_eq_U:
                // i=0,  skip from i to j
                break;
            case MANNER_C_eq_P:
                stk.push(make_tuple(i, j, bestP[offset[i]+j]));
                break;
            case MANNER_C_eq_C_plus_U:
                k = j - 1;
                stk.push(make_tuple(0, k, bestC[k]));
                break;
            case MANNER_C_eq_C_plus_P:
                k = state.trace.split;
                stk.push(make_tuple(0, k, bestC[k]));
                stk.push(make_tuple(k+1, j, bestP[offset[k+1]+j]));
                break;
            default:  // MANNER_NONE or other cases
                printf("wrong manner at %d, %d: manner %d\n", i, j, state.manner); fflush(stdout);
                assert(false);
        }
    }

    return;
}


struct DecoderResult solve(string& seq) {
    struct timeval starttime, endtime;

    unsigned seq_length = static_cast<int>(seq.length());

    // number of states
    unsigned long nos_P = 0, nos_M1 = 0, nos_M2 = 0, nos_M = 0, nos_C = 0;

    gettimeofday(&starttime, NULL);

    // initialize everything
    vector<int> offset = initialize_offset(seq_length);

#if CANDIDATE_LIST
    vector<int> candidate_list;
#endif  // CANDIDATE_LIST

    unsigned alloc_size = (seq_length+1) * (seq_length+2) / 2;

    vector<State> bestP(alloc_size),
            bestM1(alloc_size),
            bestM2(alloc_size),
            bestM(alloc_size),
            bestC(seq_length);

    int nucs[seq_length];
    for (int i = 0; i < seq_length; ++i)
        nucs[i] = GET_ACGU_NUM(seq[i]);

    // start CKY decoding
    for(int j = 1; j < seq_length; ++j) {
#if CANDIDATE_LIST
        candidate_list.clear();
#endif  // CANDIDATE_LIST
        for (int i = j-1; i >= 0; --i ) {
            int nuci = nucs[i], nucj = nucs[j];
            int nuci1 = (i + 1) < seq_length ? nucs[i + 1] : -1;
            int nucj_1 = (j - 1) > -1 ? nucs[j - 1] : -1;

            // targets to be updated
            State &candidate_P = bestP[offset[i] + j];

            // update candidate_P
#if NO_SHARP_TURN
            if (j - i >= 4) {
#endif
                if (_allowed_pairs[nuci][nucj]) {
                    // hairpin
                    {
                        double newscore = score_hairpin(i, j, nuci, nuci1, nucj_1, nucj);
                        ++nos_P;
                        if (candidate_P.score < newscore || candidate_P.manner == MANNER_NONE) {
                            candidate_P.set(newscore, MANNER_HAIRPIN);
                        }
                    }

                    // helix and single-branched loops
                    // constraints: single length <= SINGLE_MAX_LEN
                    double precomputed_single = score_junction_B(i, j, nuci, nuci1, nucj_1, nucj);
                    double precomputed_helix = score_helix(nuci, nuci1, nucj_1, nucj);
                    for (int p = i + 1; p < std::min(j, i + 1 + SINGLE_MAX_LEN + 1); ++p) {
                        for (int q = std::max(p + 1, j - 1 - SINGLE_MAX_LEN + (p - i - 1)); q < j; ++q) {
                            int nucp = nucs[p], nucq = nucs[q];
                            if (_allowed_pairs[nucp][nucq]) {
                                State &child = bestP[offset[p] + q];
                                if (child.manner != MANNER_NONE) {
                                    if (p == i + 1 && q == j - 1) {
                                        double newscore = precomputed_helix + child.score;
                                        ++nos_P;
                                        if (candidate_P.score < newscore || candidate_P.manner == MANNER_NONE) {
                                            // no need to store trace since p and q are fixed
                                            candidate_P.set(newscore, MANNER_HELIX);
                                        }
                                    } else {
                                        double newscore = precomputed_single +
                                                          score_single_without_junctionB(i, j, p, q,
                                                                                         nucs[p - 1], nucp, nucq,
                                                                                         nucs[q + 1]) +
                                                          score_junction_B(q, p, nucq, nucs[q + 1], nucs[p - 1], nucp) +
                                                          child.score;
                                        ++nos_P;
                                        if (candidate_P.score < newscore || candidate_P.manner == MANNER_NONE) {
                                            candidate_P.set(newscore, MANNER_SINGLE,
                                                            static_cast<char>(p - i),
                                                            static_cast<char>(j - q));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // multi
                    if (j - i - 1 >= 4) {
                        State &child_M2 = bestM2[offset[i + 1] + j - 1];
                        if (child_M2.manner != MANNER_NONE) {
                            double newscore = score_multi(i, j, nuci, nuci1, nucj_1, nucj, seq_length) +
                                              child_M2.score;
                            ++nos_P;
                            if (candidate_P.score < newscore || candidate_P.manner == MANNER_NONE) {
                                candidate_P.set(newscore, MANNER_MULTI);
                            }
                        }
                    }
                }  // end of updating candidate_P
#if NO_SHARP_TURN
            }
#endif

#if DEBUG
            printf("%d %d bestP %.2f\n", i, j, candidate_P.score); fflush(stdout);
#endif
            // update candidate_M1
            State &candidate_M1 = bestM1[offset[i] + j];

            if (i != 0 && j != seq_length - 1 && j - i + 1 >= 2) {
                // M1=P
                if (_allowed_pairs[nuci][nucj] && candidate_P.manner != MANNER_NONE) {
                    double newscore = score_M1(i, j, j, nucs[i - 1], nuci, nucj, nucs[j + 1], seq_length) +
                                      candidate_P.score;
                    ++nos_M1;
                    if (candidate_M1.score < newscore || candidate_M1.manner == MANNER_NONE) {
                        candidate_M1.set(newscore, MANNER_M1_eq_P);
                    }
                }
                // M1=M1+U
                State child_M1 = bestM1[offset[i] + j - 1];
                if (child_M1.manner != MANNER_NONE) {
                    double newscore = score_multi_unpaired(j, j) + child_M1.score;
                    ++nos_M1;
                    if (candidate_M1.score < newscore || candidate_M1.manner == MANNER_NONE) {
                        candidate_M1.set(newscore, MANNER_M1_eq_M1_plus_U);
                    }
                }
            }  // end of updating candidate_M1

#if DEBUG
            printf("%d %d bestM1 %.2f\n", i, j, candidate_M1.score); fflush(stdout);
#endif
            State &candidate_M2 = bestM2[offset[i] + j];
            if (i != 0 && j != seq_length - 1 && j - i + 1 >= 4) {
                // M2 = M + M1
                double best_score = std::numeric_limits<double>::lowest();
                int split = -1;
#if CANDIDATE_LIST
                for (auto k : candidate_list){
#else
                for (int k = i + 1; k < j - 1; ++k) {
#endif  // CANDIDATE_LIST
                    State &left_M = bestM[offset[i] + k];
                    State &right_M1 = bestM1[offset[k + 1] + j];
                    if (left_M.manner != MANNER_NONE && right_M1.manner != MANNER_NONE) {
                        double newscore = left_M.score + right_M1.score;
                        ++nos_M2;
                        if (best_score < newscore) {
                            best_score = newscore;
                            split = k;
                        }
                    }
                }
                if (split != -1) candidate_M2.set(best_score, MANNER_M2_eq_M_plus_M1, split);

                {
                  // M2=unpaired+M2
                  State& right_M2 = bestM2[offset[i+1]+j];
                  if(right_M2.manner!=MANNER_NONE) {
                    double newscore = score_multi_unpaired(i, i) + right_M2.score;
                    if(candidate_M2.score < newscore || candidate_M2.manner == MANNER_NONE){
                      candidate_M2.set(newscore, MANNER_M2_eq_U_plus_M2);
                    }
                  }
                }
            }

#if DEBUG
                printf("%d %d bestM2 rule1 %.2f: manner %d\n", i, j, candidate_M2.score, candidate_M2.manner); fflush(stdout);
#endif

#if CANDIDATE_LIST
            if (bestM1[offset[i]+j].score > bestM2[offset[i]+j].score) candidate_list.push_back(i-1);
#endif  // CANDIDATE_LIST

            // update candidate_M
            State &candidate_M = bestM[offset[i] + j];

            if (i != 0 && j != seq_length - 1) {

                // M=M2
                if (j - i + 1 >= 4) {
                    {
                        State &child_M2 = bestM2[offset[i] + j];
                        if (child_M2.manner != MANNER_NONE && child_M2.manner!=MANNER_M2_eq_U_plus_M2) {
                            double newscore = child_M2.score;
                            ++nos_M;
                            if (candidate_M.score < newscore || candidate_M.manner == MANNER_NONE) {
                                candidate_M.set(newscore, MANNER_M_eq_M2);
                            }
                        }
                    }

#if DEBUG
                    printf("%d %d bestM rule1 %.2f: manner %d\n", i, j, candidate_M.score, candidate_M.manner); fflush(stdout);
#endif
                }

                // M=M1
                if (j - i + 1 >= 2) {
                  State &child_M1 = bestM1[offset[i] + j];
                  if (child_M1.manner != MANNER_NONE) {
                    double newscore = child_M1.score;
                    ++nos_M;
                    if (candidate_M.score < newscore || candidate_M.manner == MANNER_NONE) {
                      candidate_M.set(newscore, MANNER_M_eq_M1);
                    }
                  }
                }

            }  // end of updating candidate_M
        }  // end of for-loop i
    }  // end of for-loop j

    // update candidate_C
    for (int j=0; j<seq_length; ++j) {
        State& candidate_C = bestC[j];
        int nucj_1 = nucs[j-1], nucj = nucs[j];

        // C=U
        {
            double newscore = score_external_unpaired(0, j);
            ++ nos_C;
            if (candidate_C.score < newscore || candidate_C.manner == MANNER_NONE) {
                candidate_C.set(newscore, MANNER_C_eq_U);
#if DEBUG
                printf("%d C=U %.2f\n", j, newscore); fflush(stdout);
#endif
            }
        }

        // C=P
        {
            State& state_P = bestP[offset[0]+j];
            if (state_P.manner != MANNER_NONE) {
                int nucj1 = (j < seq_length-1) ? nucs[j+1] : -1;
                double newscore = score_external_paired(0, j, -1, nucs[0],
                                                        nucj, nucj1, seq_length) +
                        state_P.score;
                ++ nos_C;
                if (candidate_C.score < newscore || candidate_C.manner == MANNER_NONE) {
                    candidate_C.set(newscore, MANNER_C_eq_P);
#if DEBUG
                    printf("%d C=P %.2f\n", j, newscore); fflush(stdout);
#endif
                }
            }
        }

        // C=C+U
        {
            int k = j - 1;
            if (k>-1) {
                State &prefix_C = bestC[k];
                if (prefix_C.manner != MANNER_NONE) {
                    double newscore = score_external_unpaired(k + 1, j) + prefix_C.score;
                    ++ nos_C;
                    if (candidate_C.score < newscore || candidate_C.manner == MANNER_NONE) {
                        candidate_C.set(newscore, MANNER_C_eq_C_plus_U);
#if DEBUG
                        printf("%d C=C+U %.2f\n", j, newscore); fflush(stdout);
#endif
                    }
                }
            }
        }

        // C=C+P
        for (int k=0; k<j; ++k){
            State& prefix_C = bestC[k];
            State& state_P = bestP[offset[k+1] + j];
            int nuck = nucs[k], nuck1 = nucs[k+1];
            if(prefix_C.manner!=MANNER_NONE && state_P.manner!=MANNER_NONE && _allowed_pairs[nuck1][nucj]) {
                // when j == seq_length - 1, nucj1 is not used in score_external_paired
                int nucj1 = (j < seq_length-1)? nucs[j+1] : -1;
                double newscore = score_external_paired(k+1, j, nuck, nuck1,
                                                        nucj, nucj1, seq_length) +
                                  prefix_C.score + state_P.score;
                ++ nos_C;
                if (candidate_C.score < newscore || candidate_C.manner == MANNER_NONE ) {
                    candidate_C.set(newscore, MANNER_C_eq_C_plus_P, k);
#if DEBUG
                    printf("%d C=C+P %.2f\n", j, newscore); fflush(stdout);
#endif
                }
            }
        }
#if DEBUG
        printf("%d bestC %.2f\n", j, candidate_C.score); fflush(stdout);
#endif
    }  // end of updating candidate_C

    State& viterbi = bestC[seq_length-1];

    char result[seq_length+1];
    get_parentheses(result, seq_length, offset, bestP, bestM1, bestM2, bestM, bestC);

    gettimeofday(&endtime, NULL);
    double elapsed_time = endtime.tv_sec - starttime.tv_sec + (endtime.tv_usec-starttime.tv_usec)/1000000.0;

    printf("Viterbi score: %f\n", viterbi.score);
    unsigned long nos_tot = nos_P + nos_M1 + nos_M2 + nos_M + nos_C;
    printf("Time: %f len: %d score %f #states %lu P %lu M1 %lu M2 %lu M %lu C %lu\n",
           elapsed_time, seq_length, viterbi.score, nos_tot,
           nos_P, nos_M1, nos_M2, nos_M, nos_C);
    printf(">structure\n%s\n\n", result);

    fflush(stdout);

    return {string(result), viterbi.score, nos_tot, elapsed_time};
}



int main(int argc, char** argv){
    assert(argc >= 2);

    initialize();
    initialize_cachesingle();

    char* seq_file_name = argv[1];

    ifstream f_seq(seq_file_name);

    // variables for decoding
    int num=0, total_len = 0;
    unsigned long long total_states;
    double total_score = .0;
    double total_time = .0;

    // go through the seq file to decode each seq
    for (string seq; getline(f_seq, seq);) {
        printf("seq:\n%s\n", seq.c_str()); // passed

        DecoderResult result = solve(seq);

        ++num;
        total_len += seq.length();
        total_score += result.score;
        total_states += result.numofstates;
        total_time += result.time;
    }

    f_seq.close();

    double dnum = (double)num;
    printf("num_seq: %d; avg_len: %.1f ", num, total_len/dnum);
    printf("avg_score: %.4f; avg_time: %.3f; tot_time: %f; avg_states: %.1f\n",
           total_score/dnum, total_time/dnum, total_time, total_states/dnum);
    return 0;
}
