from __future__ import division
from collections import defaultdict
import sys

fin = open("contrafold.params.complementary")
fout = open("fastcky_w.h", "w")
print >> fout, "#ifndef FASTCKY_W"
print >> fout, "#define FASTCKY_W"
fw = defaultdict(float)

NUM_OF_TYPE_OF_NUCS = 5
NOTON = NUM_OF_TYPE_OF_NUCS

EXPLICIT_MAX_LEN = 4
SINGLE_MIN_LEN = 0
SINGLE_MAX_LEN = 30

HAIRPIN_MIN_LEN = 0
BULGE_MIN_LEN = 1
INTERNAL_MIN_LEN = 2
SYMMETRIC_MIN_LEN = 1
ASYMMETRY_MIN_LEN = 1

HAIRPIN_MAX_LEN = 30
BULGE_MAX_LEN = SINGLE_MAX_LEN
INTERNAL_MAX_LEN = SINGLE_MAX_LEN
SYMMETRIC_MAX_LEN = 15
ASYMMETRY_MAX_LEN = 28

features_constant = [
    "multi_base",
    "multi_unpaired",
    "multi_paired",
    "external_unpaired",
    "external_paired"
]

features_unary_reversable = [
    "base_pair",
    "internal_1x1_nucleotides",
    "helix_stacking",
]

unary_reversable_len = [
    NOTON*NOTON,
    NOTON*NOTON,
    NOTON*NOTON*NOTON*NOTON,
]

features_unary = [
    "terminal_mismatch",
    "bulge_0x1_nucleotides",
    "helix_closing",
    "dangle_left",
    "dangle_right"
]

unarylen = [
    NOTON*NOTON*NOTON*NOTON,
    NOTON,
    NOTON*NOTON,
    NOTON*NOTON*NOTON,
    NOTON*NOTON*NOTON
]

## only binary feature: internal_explicit

features_atleast = [
    "hairpin_length",
    "bulge_length",
    "internal_length",
    "internal_symmetric_length",
    "internal_asymmetry"
]

bounds = [
    (HAIRPIN_MIN_LEN, HAIRPIN_MAX_LEN),
    (BULGE_MIN_LEN, BULGE_MAX_LEN),
    (INTERNAL_MIN_LEN, INTERNAL_MAX_LEN),
    (SYMMETRIC_MIN_LEN, SYMMETRIC_MAX_LEN),
    (ASYMMETRY_MIN_LEN, ASYMMETRY_MAX_LEN),
]

ln = {'A':0,'C':1,'G':2,"U":3}

## load feature weights and convert to exact format
for line in fin:
    key, value = line.strip().split()
    fw[key] = float(value)

    for index, fea in enumerate(features_atleast):
        _min, _max = bounds[index]
        fw[fea+"_exact_%d" % _min] = fw[fea+"_at_least_%d" % _min]
        for i in xrange(_min+1, _max+1):
            fw[fea+"_exact_%d" % i] = fw[fea+"_exact_%d" % (i-1)] + \
                                      fw[fea+"_at_least_%d" % i]
## print out features to fout
for fea in features_constant:
    print >> fout, "double %s = %f;" % (fea, fw[fea])


# lst = [0.0] * (NOTON*NOTON)
# for key in fw:
#     if key.startswith("base_pair"):
#         right = key.rsplit("_", 1)[1]
#         num = ln[right[0]] * NOTON + ln[right[1]]
#         num_r = ln[right[1]] * NOTON + ln[right[0]]
#         lst[num] = fw[key]
#         lst[num_r] = fw[key]
# print >> fout, "double base_pair[%d] = {%s};" % (NOTON*NOTON, ",".join([str(x) for x in lst]))

for index, fea in enumerate(features_unary_reversable): ## all ACGU's, no number!
    l = unary_reversable_len[index]
    lst = [0.0] * l
    for key in fw:
        if key.startswith(fea):
            right = key.rsplit("_", 1)[1]

            stringlst = []
            stringlst.append(right)
            stringlst.append(right[::-1])

            for string in stringlst:
                num = 0
                for index2, char2 in enumerate(string):
                    num *= NOTON
                    num += ln[char2]
                lst[num] = fw[key]
    # print index, fea, lst
    print >> fout, "double %s[%d] = {%s};" % (fea, l, ",".join([str(x) for x in lst]))


for index, fea in enumerate(features_unary): ## all ACGU's, no number!
    l = unarylen[index]
    lst = [0.0] * l
    for key in fw:
        if key.startswith(fea):
            right = key.rsplit("_", 1)[1]
            num = 0
            for index2, char2 in enumerate(right):
                num *= NOTON
                num += ln[char2]
            lst[num] = fw[key]
    # print index, fea, lst
    print >> fout, "double %s[%d] = {%s};" % (fea, l, ",".join([str(x) for x in lst]))

fea = "internal_explicit" 
l = 21
lst = [0.0] * l
for key in fw:
    if key.startswith(fea):
        right = key.rsplit("_", 2)[1:]
        num = int(right[0])*4+int(right[1])
        lst[num] = fw[key]
print >> fout, "double %s[%d] = {%s};" % (fea, l, ",".join([str(x) for x in lst]))

for index, fea in enumerate(features_atleast):
    fea_exact = fea+"_exact"
    _min, _max = bounds[index]
    l = _max+1
    lst = [0.0] * l
    for key in fw:
        if key.startswith(fea) and "exact" in key:
            right = key.rsplit("_", 1)[1]
            num = int(right)
            lst[num] = fw[key]
    print >> fout, "double %s[%d] = {%s};" % (fea, l, ",".join([str(x) for x in lst]))

for index, fea in enumerate(features_atleast):
    fea_atleast = fea+"_at_least"
    _min, _max = bounds[index]
    l = _max+1
    lst = [0.0] * l
    for key in fw:
        if key.startswith(fea) and "at_least" in key:
            right = key.rsplit("_", 1)[1]
            num = int(right)
            lst[num] = fw[key]
    print >> fout, "double %s[%d] = {%s};" % (fea_atleast, l, ",".join([str(x) for x in lst]))


print >> fout, "#endif"
fout.close()
