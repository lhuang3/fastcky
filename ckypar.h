//
// Created by Kai Zhao on 1/17/17.
//

#ifndef FASTCKY_CKYPAR_H
#define FASTCKY_CKYPAR_H

#include <string>
#include <limits>

enum Manner {
    MANNER_NONE = 0,          // 0: empty
    MANNER_HAIRPIN,           // 1: hairpin
    MANNER_SINGLE,            // 2: single
    MANNER_HELIX,             // 3: helix
    MANNER_MULTI,             // 4: multi
    MANNER_M1_eq_P,           // 5: M1=P
    MANNER_M1_eq_M1_plus_U,   // 6: M1=M1+U
    MANNER_C_eq_C_plus_U,     // 7: C=C+U
    MANNER_M_eq_M1,           // 8: M1=M1
    MANNER_M_eq_M2,           // 9: M=M2
    MANNER_C_eq_C_plus_P,     // 10: C=C+P
    MANNER_M2_eq_U_plus_M2,   // 11: M2=U+M2
    MANNER_M2_eq_M_plus_M1,   // 12: M2=M+M1
    MANNER_C_eq_U,            // 13: C=U
    MANNER_C_eq_P             // 14: C=P
};

struct State {
    double score;
    Manner manner;

    union TraceInfo {
        int split;
        struct {
            char l1;
            char l2;
        } paddings;
    };

    TraceInfo trace;

    State(): manner(MANNER_NONE), score(std::numeric_limits<double>::lowest()) {};

    void set(double score_, Manner manner_) {
        score = score_; manner = manner_;
    }

    void set(double score_, Manner manner_, int split_) {
        score = score_; manner = manner_; trace.split = split_;
    }

    void set(double socore_, Manner manner_, char l1_, char l2_) {
        score = socore_; manner = manner_;
        trace.paddings.l1 = l1_; trace.paddings.l2 = l2_;
    }
};


#endif //FASTCKY_CKYPAR_H
