\documentclass{article}
\usepackage[cm]{fullpage}
\addtolength{\textheight}{1cm}
%\addtolength{\textwidth}{1cm}
%\addtolength{\evensidemargin}{-1cm}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\algrenewcommand\algorithmicindent{0.5em}%

\usepackage{pifont}

\usepackage{tikz}
\usetikzlibrary{positioning, arrows}

\begin{document}


%\newpage

\twocolumn

This brief note summarizes 
the $O(n^6)$ algorithm of Rivas and Eddy (1999; 2000) for RNA pseudoknots. 
We present a simplified version of the 2000 paper
which is strictly more general than the 1999 one (the 2000 algorithm can model kissing hairpins
``$abacbc$''),
but the 1999 one can not).

\section{Shapes}
Each shape contains part of nucleotides in the sequence, and the pairing
structure between them. For the structure, each nucleotide in the shape must be
unpaired or paired with another nucleotide in the current shape (pairing with
nucleotide outside the shape is \textbf{not} allowed). 

\begin{itemize}
\item \textbf{semicircle}: contains nucs and substructure between position $i$
  and $j$.

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}

  Here nucs in position $i$ and $j$ are not necessary to be paired to each other
  . A solid arc will be used if paired.

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[black, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}
  
\item \textbf{bridge}: contains nucs and substructure between position $i$ and
  $j$, but not between $k$ and $l$.

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}

\end{itemize}

%\newpage

\section{Rules for Semicircle}

\begin{itemize}
\item \textbf{singleton} ($N^2$)
  
  \begin{tikzpicture}
    \draw[blue, very thick] (2.75,0) -- (3.25,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[dashed, blue, very thick] (2.75,0) to[bend left] (3.25,0);
  \end{tikzpicture}
  
\item \textbf{skip} ($N^2$)

  \begin{tikzpicture}
    \draw[blue, very thick] (0,0) -- (0.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{};
    \draw[very thick] (0.5,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i+1$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (0.5,0) to[bend left] (6,0);
  \end{tikzpicture}

  \begin{tikzpicture}
    \draw[blue, very thick] (5.5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[very thick] (0,0) -- (5.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j-1$}]{};
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (0,0) to[bend left] (5.5,0);
  \end{tikzpicture}
  
\item \textbf{pair} ($N^2$)
  
  \begin{tikzpicture}
    \draw[blue, very thick] (0,0) -- (0.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{};
    \draw[very thick] (0.5,0) -- (5.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i+1$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j-1$}]{};
    \draw[blue, very thick] (5.5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (0.5,0) to[bend left] (5.5,0);
  \end{tikzpicture}

\item \textbf{combineS} ($N^3)$

  \begin{tikzpicture}
    \draw[very thick] (0,0) -- (3,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{};
    \draw[red, very thick] (3,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (0,0) to[bend left] (3,0);
    \draw[dashed, red, very thick] (3,0) to[bend left] (6,0);
  \end{tikzpicture}

\item \textbf{combineB} ($N^5$)

  \begin{tikzpicture}  
    \draw[very thick] (0,0) -- (1.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{};
    \draw[red, very thick] (1.5,0) -- (3,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$p$}]{};
    \draw[very thick] (3,0) -- (4.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[red, very thick] (4.5,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$q$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (4.5,0);
    \draw[dashed, black, very thick] (1.5,0) to[bend left] (3,0);
    \draw[dashed, red, very thick] (1.5,0) to[bend right] (6,0);
    \draw[dashed, red, very thick] (3,0) to[bend right] (4.5,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}

\item \textbf{fill} ($N^4$)

  \begin{tikzpicture}  
    \draw[red, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{};
    \draw[very thick] (2,0) -- (4,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[red, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$q$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
    \draw[dashed, red, very thick] (2,0) to[bend right] (4,0);
    \draw[dashed, red, very thick] (0,0) to[bend right] (6,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}

\end{itemize}

%\newpage

\section{Rules for Bridge}

\begin{itemize}
\item \textbf{init} ($N^2$)
  
  \begin{tikzpicture}
    \draw[blue, very thick] (0,0) -- (0.25,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{};
    \draw[blue, very thick] (5.75,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, blue, very thick] (0.25,0) to[bend left] (5.75,0);
  \end{tikzpicture}

\item \textbf{combineS} ($N^4$)
  
  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[red, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, blue, very thick] (2,0) to[bend left] (4,0);
    \draw[dashed, black, very thick] (0,0) to[bend left] (2,0);
    \draw[dashed, red, very thick] (4,0) to[bend left] (6,0);
  \end{tikzpicture}

\item \textbf{pair} ($N^4$)

  \begin{tikzpicture}
    \draw[blue, very thick] (0,0) -- (0.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{};
    \draw[black, very thick] (0.5,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i+1$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (5.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j-1$}]{};
    \draw[blue, very thick] (5.5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (0.5,0) to[bend left] (5.5,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (1.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k-1$}]{};
    \draw[blue, very thick] (1.5,0) -- (2,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$k$}]{};
    \draw[blue, very thick] (4,0) -- (4.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$l$}]{};
    \draw[black, very thick] (4.5,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l+1$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (1.5,0) to[bend left] (4.5,0);
    \draw[blue, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}

\item \textbf{skip} ($N^4$)

  \begin{tikzpicture}
    \draw[blue, very thick] (0,0) -- (0.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{};
    \draw[black, very thick] (0.5,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i+1$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0.5,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}


  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (1.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k-1$}]{};
    \draw[blue, very thick] (1.5,0) -- (2,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$k$}]{};
    \draw[black, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (1.5,0) to[bend left] (4,0);
    \draw[dashed, blue, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}


  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[blue, very thick] (4,0) -- (4.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$l$}]{};
    \draw[black, very thick] (4.5,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l+1$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4.5,0);
    \draw[dashed, blue, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}
  

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (5.5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j-1$}]{};
    \draw[blue, very thick] (5.5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (5.5,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}

\item \textbf{expand} ($N^5$)

  \begin{tikzpicture}
    \draw[red, very thick] (0,0) -- (1,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$i$}]{};
    \draw[black, very thick] (1,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (1,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
    \draw[dashed, red, very thick] (0,0) to[bend left] (1,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}


  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (1,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{};
    \draw[red, very thick] (1,0) -- (2,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$k$}]{};
    \draw[black, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (1,0) to[bend left] (4,0);
    \draw[dashed, red, very thick] (1,0) to[bend left] (2,0);
    \draw[dashed, blue, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}


  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[red, very thick] (4,0) -- (5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$l$}]{};
    \draw[black, very thick] (5,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (5,0);
    \draw[dashed, red, very thick] (4,0) to[bend left] (5,0);
    \draw[dashed, blue, very thick] (2,0) to[bend left] (4,0);
  \end{tikzpicture}
  

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{};
    \draw[red, very thick] (5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{blue}{$j$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend left] (5,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
    \draw[dashed, red, very thick] (5,0) to[bend left] (6,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}

\item \textbf{combine1side} ($N^6$)

  \begin{tikzpicture}
    \draw[black, very thick] (0.67,0) -- (1.33,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$s$}]{};
    \draw[black, very thick] (4,0) -- (6,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, black, very thick] (0.67,0) to[bend right] (6,0);
    \draw[dashed, black, very thick] (1.33,0) to[bend right] (4,0);
    \draw[red, very thick] (0,0) -- (0.67,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{};
    \draw[red, very thick] (1.33,0) -- (2,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[dashed, red, very thick] (0,0) to [bend left] (2,0);
    \draw[dashed, red, very thick] (0.67,0) to [bend left] (1.33,0);
    \draw[dashed, blue, very thick] (0,0) to [bend left] (6,0);
    \draw[dashed, blue, very thick] (2,0) to [bend left] (4,0);
  \end{tikzpicture}


  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4.67,0) -- (5.33,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$s$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend right] (5.33,0);
    \draw[dashed, black, very thick] (2,0) to[bend right] (4.67,0);
    \draw[red, very thick] (4,0) -- (4.67,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{};
    \draw[red, very thick] (5.33,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, red, very thick] (4,0) to[bend left] (6,0);
    \draw[dashed, red, very thick] (4.67,0) to[bend left] (5.33,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, blue, very thick] (2,0) to [bend left] (4,0);
  \end{tikzpicture}

\item \textbf{combine2side} ($N^6$)

  \begin{tikzpicture}
    \draw[black, very thick] (0,0) -- (1,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{};
    \draw[black, very thick] (4,0) -- (5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$s$}]{};
    \draw[dashed, black, very thick] (0,0) to[bend right] (5,0);
    \draw[dashed, black, very thick] (1,0) to[bend right] (4,0);
    \draw[red, very thick] (1,0) -- (2,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[red, very thick] (5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, red, very thick] (1,0) to[bend left] (6,0);
    \draw[dashed, red, very thick] (2,0) to[bend left] (5,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
    \draw[dashed, blue, very thick] (2,0) to [bend left] (4,0);
  \end{tikzpicture}

  \begin{tikzpicture}
    \draw[black, very thick] (1,0) -- (2,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$r$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$k$}]{};
    \draw[black, very thick] (4,0) -- (5,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$l$}]{}
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$s$}]{};
    \draw[dashed, black, very thick] (1,0) to[bend left] (5,0);
    \draw[dashed, black, very thick] (2,0) to[bend left] (4,0);
    \draw[red, very thick] (0,0) -- (1,0)
    node[pos=0,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$i$}]{};
    \draw[red, very thick] (5,0) -- (6,0)
    node[pos=1,circle,fill,inner sep=1pt,label=below:\textcolor{black}{$j$}]{};
    \draw[dashed, red, very thick] (0,0) to[bend right] (6,0);
    \draw[dashed, red, very thick] (1,0) to[bend right] (5,0);
    \draw[dashed, blue, very thick] (0,0) to[bend left] (6,0);
  \end{tikzpicture}

\end{itemize}


\section{Pseudocode (maximizing the number of pairs)}

%\resizebox{0.5\textwidth}{!}{
\noindent
\hspace{-2cm}
\begin{minipage}{0.5\textwidth}
\begin{algorithmic}[0]
\footnotesize
  \Procedure{CKY\textendash Parsing}{}
  \For {$j \leftarrow 1$ to $N$ }
  \For {$i \leftarrow j$ downto $0$}

  \State $S^{i,j} \leftarrow \textbf{Shairpin}$ \Comment{$S$: semicircle}
  \State $S^{i,j} \leftarrow \textbf{Sskip}(S^{i+1,j} | S^{i,j-1})$
  \State $S^{i,j} \leftarrow \textbf{Spair}(S^{i+1,j-1})$

  % \For {$p \leftarrow i+1$ to $j-2$}
  % \For {$q \leftarrow p+1$ to $j-1$}
  % \State $S^{i,j} \leftarrow \textbf{Ssingle}(S^{p,q})$
  % \EndFor
  % \EndFor

  \For {$k \leftarrow i$ to $j$}
  \State $S^{i,j} \leftarrow \textbf{ScombineS}(S^{i,k}, S^{k+1,j})$
  \EndFor

  \For {$p \leftarrow i$ to $j$}
  \For {$k \leftarrow p$ to $j$}
  \For {$q \leftarrow k$ to $j$}
  \State $S^{i,j} \leftarrow \textbf{ScombineB}(B^{i,p,k+1,q}, B^{p+1,k,q+1,j})$
  \EndFor
  \EndFor
  \EndFor

  \For {$k,l$ inside $(i,j)$}
  \State $S^{i,j} \leftarrow \textbf{Sfill}(S^{k+1,l}, B^{i,k,l+1,j})$
  \EndFor
  
  \State $B^{i,i,j,j} \leftarrow \textbf{Binit}$ \Comment {$B$: bridge}
  
  \For {$k \leftarrow i$ to $j$}
  \For {$l \leftarrow j$ downto $1$}

  \State $B^{i,k,l,j} \leftarrow \textbf{BcombineS}(S^{i,k},S^{l,j})$ 
  
  \State $B^{i,k,l,j} \leftarrow \textbf{Bpair}(B^{i+1,k,l,j-1}|B^{i,k-1,l+1,j})$ 

  \State $B^{i,k,l,j} \leftarrow \textbf{Bskip}(B^{i+1,k,l,j}|B^{i,k-1,l,j}|B^{i,k,l+1,j}|B^{i,k,l,j-1})$ 

  \For {$r$ inside $B^{i,k,l,j}$}
  \State $B^{i,k,l,j} \leftarrow \textbf{Bexpand}(B^{r+1,k,l,j},S^{i,r}|B^{i,r,l,j},S^{r+1,k}|B^{i,k,r+1,j},S^{l,r}|B^{i,k,l,r},S^{r+1,j})$
  \EndFor

  \For {$r,s$ inside $(i,k)|(l,j)$}
  \State $B^{i,k,j,l} \leftarrow \textbf{Bcombine1side}(B^{r+1,s,l,j},B^{i,r,s+1,k}|B^{i,k,r+1,s},B^{l,r,k+1,j})$    
  \EndFor
  
  \For {$r \leftarrow i$ to $k$}
  \For {$s \leftarrow l$ to $j$}
  \State $B^{i,k,j,l} \leftarrow \textbf{Bcombine2side}(B^{i,r,s+1,j},B^{r+1,k,l,s}|B^{i,r,l,s},B^{r+1,k,s+1,j})$  
  \EndFor
  \EndFor
  
  % k, l
  \EndFor
  \EndFor
  

  % i, j
  \EndFor
  \EndFor

  \EndProcedure
\end{algorithmic}
\end{minipage}
%}
\end{document} 
